/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.dao;

import az.kitabxanaConnection.model.Login;
import az.kitabxanaConnection.util.JdbcUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class LoginDaoImpl implements LoginDao {

    @Override
    public Login login(String username, String password) throws Exception {
        Login login = new Login();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ID,USERNAME,PASSWORD,ROLE,NAME,SURNAME FROM LOGIN "
                + " WHERE ACTIVE = 1 AND USERNAME = ? AND PASSWORD = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, username);
                ps.setString(2, password);
                rs = ps.executeQuery();
                if (rs.next()) {
                    login.setId(rs.getLong("ID"));
                    login.setUsename(rs.getString("USERNAME"));
                    login.setPassword(rs.getString("PASSWORD"));
                    login.setRole(rs.getInt("ROLE"));
                    login.setName(rs.getString("NAME"));
                    login.setSurname(rs.getString("SURNAME"));
                } else {
                    login = null;
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return login;
    }

    @Override
    public List<Login> getLoginList() throws Exception {
        List<Login> loginList = new ArrayList<Login>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID,USERNAME,PASSWORD,ROLE,NAME,SURNAME FROM LOGIN "
                + " WHERE ACTIVE = 1";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Login login = new Login();
                    login.setR(rs.getInt("r"));
                    login.setId(rs.getLong("ID"));
                    login.setUsename(rs.getString("USERNAME"));
                    login.setPassword(rs.getString("PASSWORD"));
                    login.setRole(rs.getInt("ROLE"));
                    login.setName(rs.getString("NAME"));
                    login.setSurname(rs.getString("SURNAME"));
                    loginList.add(login);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return loginList;
    }

    @Override
    public Login getLoginByList(long id) throws Exception {
        Login login = new Login();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ID,USERNAME,PASSWORD,ROLE,NAME,SURNAME FROM LOGIN "
                + " WHERE ID = ? AND ACTIVE = 1";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    login.setId(rs.getLong("ID"));
                    login.setUsename(rs.getString("USERNAME"));
                    login.setPassword(rs.getString("PASSWORD"));
                    login.setRole(rs.getInt("ROLE"));
                    login.setName(rs.getString("NAME"));
                    login.setSurname(rs.getString("SURNAME"));
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return login;
    }

    @Override
    public boolean addLogin(Login login) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO LOGIN (ID,USERNAME,PASSWORD,ROLE,NAME,SURNAME) "
                + " VALUES(LOGIN_SEQ.NEXTVAL,?,?,?,?,?)";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, login.getUsename());
                ps.setString(2, login.getPassword());
                ps.setInt(3, login.getRole());
                ps.setString(4, login.getName());
                ps.setString(5, login.getSurname());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateLogin(Login login, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE LOGIN SET USERNAME = ?,PASSWORD = ?, ROLE = ?, NAME = ?, SURNAME = ? "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, login.getUsename());
                ps.setString(2, login.getPassword());
                ps.setInt(3, login.getRole());
                ps.setString(4, login.getName());
                ps.setString(5, login.getSurname());
                ps.setLong(6, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean deleteLogin(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE LOGIN SET ACTIVE = 0 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Login> searchLoginList(String search,int a) throws Exception {
        List<Login> loginList = new ArrayList<Login>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID,USERNAME,PASSWORD,ROLE,NAME,SURNAME FROM LOGIN  "
                + " WHERE ACTIVE = ? AND (LOWER (USERNAME) LIKE LOWER('%" + search + "%') OR LOWER(PASSWORD) LIKE LOWER('%" + search + "%') OR "
                + " LOWER(ROLE) LIKE LOWER('%" + search + "%') OR LOWER(NAME) LIKE LOWER('%" + search + "%') OR LOWER(SURNAME) LIKE LOWER('%" + search + "%')) ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Login login = new Login();
                    login.setR(rs.getInt("r"));
                    login.setId(rs.getLong("ID"));
                    login.setUsename(rs.getString("USERNAME"));
                    login.setPassword(rs.getString("PASSWORD"));
                    login.setRole(rs.getInt("ROLE"));
                    login.setName(rs.getString("NAME"));
                    login.setSurname(rs.getString("SURNAME"));
                    loginList.add(login);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return loginList;
    }

    @Override
    public List<Login> getDeleteLoginList() throws Exception {
        List<Login> loginList = new ArrayList<Login>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r, ID, USERNAME,PASSWORD, ROLE,NAME,SURNAME FROM LOGIN "
                + " WHERE ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Login login = new Login();
                    login.setR(rs.getInt("r"));
                    login.setId(rs.getLong("ID"));
                    login.setUsename(rs.getString("USERNAME"));
                    login.setPassword(rs.getString("PASSWORD"));
                    login.setRole(rs.getInt("ROLE"));
                    login.setName(rs.getString("NAME"));
                    login.setSurname(rs.getString("SURNAME"));
                    loginList.add(login);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return loginList;
    }

    @Override
    public boolean fullDeleteLogin(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM LOGIN "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairLogin(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE LOGIN SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

}
