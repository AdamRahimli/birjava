/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.dao;

import az.kitabxanaConnection.model.Login;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface LoginDao {

    Login login(String username, String password) throws Exception;

    List<Login> getLoginList() throws Exception;

    Login getLoginByList(long id) throws Exception;

    boolean addLogin(Login login) throws Exception;

    boolean updateLogin(Login login, long id) throws Exception;

    boolean deleteLogin(long id) throws Exception;

    List<Login> searchLoginList(String search,int a) throws Exception;
    
    List<Login> getDeleteLoginList()throws Exception;
    
    boolean fullDeleteLogin(long id) throws Exception;
    
    boolean repairLogin(long id) throws Exception;

}
