package az.kitabxanaConnection.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.KitabOxu;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Shobeler;
import az.kitabxanaConnection.util.JdbcUtility;

public class TestDaoImpl implements TestDao {

    public List<Kitabxana> getKitabxanaList() throws Exception {
        List<Kitabxana> kitabxanaList = new ArrayList<Kitabxana>();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID,AD,ILI,JANR,MUELLIF,QIYMET,SAYI FROM KITABXANA "
                + " WHERE ACTIVE = 1 ";

        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Kitabxana kitab = new Kitabxana();
                    kitab.setR(rs.getLong("r"));
                    kitab.setId(rs.getLong("ID"));
                    kitab.setAd(rs.getString("AD"));
                    kitab.setIli(rs.getDate("ILI"));
                    kitab.setJanr(rs.getString("JANR"));
                    kitab.setMuellif(rs.getString("MUELLIF"));
                    kitab.setQiymet(rs.getString("QIYMET"));
                    kitab.setSayi(rs.getShort("SAYI"));
                    kitabxanaList.add(kitab);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabxanaList;
    }

    public List<Istifadeci> getIstifadeci() throws Exception {
        List<Istifadeci> istifadecisList = new ArrayList<Istifadeci>();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r, ID,NAME ,SURNAME, DOQUMGUNU,MAIL,TEL FROM ISTIFADECI "
                + " WHERE ACTIVE = 1";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Istifadeci adam = new Istifadeci();
                    adam.setR(rs.getLong("r"));
                    adam.setId(rs.getLong("ID"));
                    adam.setName(rs.getString("NAME"));
                    adam.setSurname(rs.getString("SURNAME"));
                    adam.setDoqumGunu(rs.getDate("DOQUMGUNU"));
                    adam.setMail(rs.getString("MAIL"));
                    adam.setTel(rs.getString("TEL"));
                    istifadecisList.add(adam);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return istifadecisList;
    }

    public List<Shobeler> getShobeler() throws Exception {
        List<Shobeler> shobelersList = new ArrayList<Shobeler>();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r, ID, UNVAN, ISHVAXTI,TEL FROM SHOBELER "
                + " WHERE ACTIVE = 1";

        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Shobeler sobe = new Shobeler();
                    sobe.setR(rs.getLong("r"));
                    sobe.setId(rs.getLong("ID"));
                    sobe.setUnvan(rs.getString("UNVAN"));
                    sobe.setIshVaxti(rs.getString("ISHVAXTI"));
                    sobe.setTel(rs.getString("TEL"));
                    shobelersList.add(sobe);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return shobelersList;
    }

    public List<KitabMekan> getKitabMekan() throws Exception {
        List<KitabMekan> kitabMekanList = new ArrayList<KitabMekan>();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,M.ID M_ID, K.AD K_AD,K.MUELLIF K_MUELLIF,K.JANR K_JANR,K.SAYI K_SAYI,S.UNVAN S_UNVAN,S.TEL S_TEL,S.ISHVAXTI S_ISHVAXTI FROM KITABMEKAN M "
                + " INNER JOIN KITABXANA K ON M.K_ID = K.ID "
                + " INNER JOIN SHOBELER S ON M.S_ID = S.ID "
                + " WHERE M.ACTIVE = 1 AND K.ACTIVE =1 AND S.ACTIVE = 1";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabMekan km = new KitabMekan();
                    Kitabxana kitab = new Kitabxana();
                    Shobeler sobe = new Shobeler();
                    km.setR(rs.getLong("r"));
                    km.setId(rs.getLong("M_ID"));
                    kitab.setAd(rs.getString("K_AD"));
                    sobe.setTel(rs.getString("S_TEL"));
                    sobe.setIshVaxti(rs.getString("S_ISHVAXTI"));
                    kitab.setMuellif(rs.getString("K_MUELLIF"));
                    kitab.setJanr(rs.getString("K_JANR"));
                    kitab.setSayi(rs.getInt("K_SAYI"));
                    sobe.setUnvan(rs.getString("S_UNVAN"));
                    km.setKitabxana(kitab);
                    km.setShobeler(sobe);
                    kitabMekanList.add(km);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabMekanList;
    }

    public List<KitabOxu> getKitabOxu() throws Exception {
        List<KitabOxu> kitabOxuList = new ArrayList<KitabOxu>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,O.ID O_ID, I.NAME || ' ' || I.SURNAME ADI_SOYADI, K.AD OXUDUQU_KITABIN_ADI FROM KITABOXU O "
                + "INNER JOIN KITABXANA K ON O.ID_K = K.ID "
                + "INNER JOIN ISTIFADECI I ON O.ID_I = I.ID "
                + "WHERE O.ACTIVE = 1 AND K.ACTIVE = 1 AND I.ACTIVE = 1";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabOxu oxu = new KitabOxu();
                    Kitabxana kitab = new Kitabxana();
                    Istifadeci adam = new Istifadeci();
                    oxu.setR(rs.getLong("r"));
                    oxu.setId(rs.getLong("O_ID"));
                    adam.setName(rs.getString("ADI_SOYADI"));
                    kitab.setAd(rs.getString("OXUDUQU_KITABIN_ADI"));
                    oxu.setIstifadeci(adam);
                    oxu.setKitabxana(kitab);
                    kitabOxuList.add(oxu);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabOxuList;
    }

    public boolean addKitabxana(Kitabxana kitab) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO KITABXANA(ID,AD,MUELLIF,ILI,JANR,QIYMET,SAYI) "
                + "VALUES(KITABXANA_SEQ.NEXTVAL,?,?,?,?,?,?)";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, kitab.getAd());
                ps.setString(2, kitab.getMuellif());
                ps.setDate(3, new java.sql.Date(kitab.getIli().getTime()));
                ps.setString(4, kitab.getJanr());
                ps.setString(5, kitab.getQiymet());
                ps.setInt(6, kitab.getSayi());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean addIstifadeci(Istifadeci adam) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO ISTIFADECI (ID, NAME,SURNAME,DOQUMGUNU,TEL,MAIL) "
                + "VALUES(ISTIFADECI_SEQ.NEXTVAL,?,?,?,?,?)";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, adam.getName());
                ps.setString(2, adam.getSurname());
                ps.setDate(3, new java.sql.Date(adam.getDoqumGunu().getTime()));
                ps.setString(4, adam.getTel());
                ps.setString(5, adam.getMail());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL!!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean addShobeler(Shobeler sobe) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO SHOBELER(ID, UNVAN,TEL,ISHVAXTI) "
                + "VALUES(SHOBELER_SEQ.NEXTVAL,?,?,?)";

        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, sobe.getUnvan());
                ps.setString(2, sobe.getTel());
                ps.setString(3, sobe.getIshVaxti());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean addKitabMekan(KitabMekan mekan) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO KITABMEKAN(ID,K_ID,S_ID) "
                + "VALUES(KITABMEKAN_SEQ.NEXTVAL,?,?)";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, mekan.getK_id());
                ps.setLong(2, mekan.getS_id());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean addKitabOxu(KitabOxu oxu) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO KITABOXU(ID,ID_I,ID_K) "
                + "VALUES(KITABOXU_SEQ.NEXTVAL,?,?)";

        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, oxu.getId_i());
                ps.setLong(2, oxu.getId_k());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }

        return result;
    }

    public Kitabxana getKitabxanaByID(long kitabID) throws Exception {
        Kitabxana kitab = new Kitabxana();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM KITABXANA "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, kitabID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    kitab.setId(rs.getLong("ID"));
                    kitab.setAd(rs.getString("AD"));
                    kitab.setMuellif(rs.getString("MUELLIF"));
                    kitab.setIli(rs.getDate("ILI"));
                    kitab.setJanr(rs.getString("JANR"));
                    kitab.setQiymet(rs.getString("QIYMET"));
                    kitab.setSayi(rs.getShort("SAYI"));
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitab;
    }

    public Istifadeci getIstifadeciByID(long adamID) throws Exception {
        Istifadeci adam = new Istifadeci();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM ISTIFADECI "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, adamID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    adam.setId(rs.getLong("ID"));
                    adam.setName(rs.getString("NAME"));
                    adam.setSurname(rs.getString("SURNAME"));
                    adam.setDoqumGunu(rs.getDate("DOQUMGUNU"));
                    adam.setTel(rs.getString("TEL"));
                    adam.setMail(rs.getString("MAIL"));
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return adam;
    }

    public Shobeler getShobelerByID(long sobeID) throws Exception {
        Shobeler sobe = new Shobeler();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM SHOBELER "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, sobeID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    sobe.setId(rs.getLong("ID"));
                    sobe.setUnvan(rs.getString("UNVAN"));
                    sobe.setTel(rs.getString("TEL"));
                    sobe.setIshVaxti(rs.getString("ISHVAXTI"));
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return sobe;
    }

    public KitabMekan getKitabMekanByID(long mekanID) throws Exception {
        KitabMekan mekan = new KitabMekan();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT M.ID M_ID,M.K_ID M_K_ID ,M.S_ID M_S_ID ,K.AD K_AD,K.MUELLIF K_MUELLIF,K.JANR K_JANR,K.SAYI K_SAYI,S.UNVAN S_UNVAN,S.TEL S_TEL,S.ISHVAXTI S_ISHVAXTI FROM KITABMEKAN  M "
                + "INNER JOIN KITABXANA K ON M.K_ID = K.ID "
                + "INNER JOIN SHOBELER S ON M.S_ID = S.ID "
                + "WHERE M.ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, mekanID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Kitabxana kitab = new Kitabxana();
                    Shobeler sobe = new Shobeler();
                    kitab.setAd(rs.getString("K_AD"));
                    kitab.setMuellif(rs.getString("K_MUELLIF"));
                    kitab.setJanr(rs.getString("K_JANR"));
                    kitab.setSayi(rs.getInt("K_SAYI"));
                    sobe.setUnvan(rs.getString("S_UNVAN"));
                    sobe.setTel(rs.getString("S_TEL"));
                    sobe.setIshVaxti(rs.getString("S_ISHVAXTI"));
                    mekan.setId(rs.getLong("M_ID"));
                    mekan.setK_id(rs.getLong("M_K_ID"));
                    mekan.setS_id(rs.getLong("M_S_ID"));
                    mekan.setKitabxana(kitab);
                    mekan.setShobeler(sobe);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return mekan;
    }

    public KitabOxu getKitabOxuByID(long oxuID) throws Exception {
        KitabOxu oxu = new KitabOxu();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT O.ID O_ID, O.ID_I O_ID_I, O.ID_K O_ID_K ,I.NAME || ' ' || I.SURNAME ADI_SOYADI, K.AD OXUDUQU_KITABIN_ADI FROM KITABOXU O "
                + "INNER JOIN KITABXANA K ON O.ID_K = K.ID "
                + "INNER JOIN ISTIFADECI I ON O.ID_I = I.ID "
                + "WHERE O.ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, oxuID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Kitabxana kitab = new Kitabxana();
                    Istifadeci adam = new Istifadeci();
                    adam.setName(rs.getString("ADI_SOYADI"));
                    kitab.setAd(rs.getString("OXUDUQU_KITABIN_ADI"));
                    oxu.setId(rs.getLong("O_ID"));
                    oxu.setId_i(rs.getLong("O_ID_I"));
                    oxu.setId_k(rs.getLong("O_ID_K"));
                    oxu.setIstifadeci(adam);
                    oxu.setKitabxana(kitab);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return oxu;
    }

    public boolean updateKitabxana(Kitabxana kitab, long kitabID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABXANA SET AD = ?, MUELLIF = ?, ILI = ?, JANR = ?, QIYMET = ?, SAYI = ? "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, kitab.getAd());
                ps.setString(2, kitab.getMuellif());
                ps.setDate(3, new java.sql.Date(kitab.getIli().getTime()));
                ps.setString(4, kitab.getJanr());
                ps.setString(5, kitab.getQiymet());
                ps.setInt(6, kitab.getSayi());
                ps.setLong(7, kitabID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean updateIstifadeci(Istifadeci adam, long adamID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE ISTIFADECI SET NAME = ?, SURNAME = ?, DOQUMGUNU = ?, TEL = ?, MAIL = ? "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, adam.getName());
                ps.setString(2, adam.getSurname());
                ps.setDate(3, new java.sql.Date(adam.getDoqumGunu().getTime()));
                ps.setString(4, adam.getTel());
                ps.setString(5, adam.getMail());
                ps.setLong(6, adamID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean updateShobeler(Shobeler sobe, long sobeID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE SHOBELER SET UNVAN = ?, TEL = ?, ISHVAXTI = ? "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, sobe.getUnvan());
                ps.setString(2, sobe.getTel());
                ps.setString(3, sobe.getIshVaxti());
                ps.setLong(4, sobeID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean updateKitabMekan(KitabMekan mekan, long mekanID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABMEKAN SET K_ID = ?, S_ID = ?  "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, mekan.getK_id());
                ps.setLong(2, mekan.getS_id());
                ps.setLong(3, mekanID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean updateKitabOxu(KitabOxu oxu, long oxuID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABOXU SET ID_I = ?, ID_K = ? "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, oxu.getId_i());
                ps.setLong(2, oxu.getId_k());
                ps.setLong(3, oxuID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean deleteKitabxana(long kitabID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABXANA SET ACTIVE = 0"
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, kitabID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean deleteIstifadeci(long adamID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE ISTIFADECI SET ACTIVE = 0 "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, adamID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean deleteShobeler(long sobeID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE SHOBELER SET ACTIVE = 0 "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, sobeID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean deleteKitabMekan(long mekanID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABMEKAN SET ACTIVE = 0 "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, mekanID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    public boolean deleteKitabOxu(long oxuID) throws Exception {
        boolean result = false;

        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABOXU SET ACTIVE = 0 "
                + "WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, oxuID);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Kitabxana> SearchKitabxana(String search,int a) throws Exception {
        List<Kitabxana> kitabxanaList = new ArrayList<Kitabxana>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID,AD,ILI,JANR,MUELLIF,QIYMET,SAYI FROM KITABXANA "
                + "WHERE ACTIVE = ? AND( LOWER(AD) LIKE LOWER('%" + search + "%') OR ILI LIKE '%" + search + "%' OR LOWER(JANR) LIKE LOWER('%" + search + "%') "
                + "OR LOWER(MUELLIF) LIKE LOWER('%" + search + "%') OR LOWER(QIYMET) LIKE LOWER('%" + search + "%') OR SAYI LIKE '%" + search + "%')";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Kitabxana kitab = new Kitabxana();
                    kitab.setR(rs.getLong("r"));
                    kitab.setId(rs.getLong("ID"));
                    kitab.setAd(rs.getString("AD"));
                    kitab.setIli(rs.getDate("ILI"));
                    kitab.setJanr(rs.getString("JANR"));
                    kitab.setMuellif(rs.getString("MUELLIF"));
                    kitab.setQiymet(rs.getString("QIYMET"));
                    kitab.setSayi(rs.getShort("SAYI"));
                    kitabxanaList.add(kitab);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabxanaList;
    }

    @Override
    public List<Istifadeci> SearchIstifadeci(String search,int a) throws Exception {
        List<Istifadeci> istifadeciList = new ArrayList<Istifadeci>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID, NAME, SURNAME,DOQUMGUNU,TEL,MAIL FROM ISTIFADECI "
                + " WHERE ACTIVE = ? AND (LOWER(NAME) LIKE LOWER('%" + search + "%') OR LOWER(SURNAME) LIKE LOWER('%" + search + "%') "
                + " OR DOQUMGUNU LIKE '%" + search + "%' OR LOWER(TEL) LIKE LOWER('%" + search + "%') OR LOWER(MAIL) LIKE LOWER('%" + search + "%'))";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Istifadeci istifadeci = new Istifadeci();
                    istifadeci.setR(rs.getLong("r"));
                    istifadeci.setId((rs.getLong("ID")));
                    istifadeci.setName(rs.getString("NAME"));
                    istifadeci.setSurname(rs.getString("SURNAME"));
                    istifadeci.setDoqumGunu(rs.getDate("DOQUMGUNU"));
                    istifadeci.setTel(rs.getString("TEL"));
                    istifadeci.setMail(rs.getString("MAIL"));
                    istifadeciList.add(istifadeci);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return istifadeciList;
    }

    @Override
    public List<Shobeler> SearchShobeler(String search,int a) throws Exception {
        List<Shobeler> shobelerList = new ArrayList<Shobeler>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID, UNVAN,TEL,ISHVAXTI FROM SHOBELER "
                + " WHERE ACTIVE = ? AND (LOWER(UNVAN) LIKE LOWER('%" + search + "%') OR "
                + " LOWER(TEL) LIKE LOWER('%" + search + "%') OR LOWER(ISHVAXTI) LIKE LOWER('%" + search + "%'))";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Shobeler shobeler = new Shobeler();
                    shobeler.setR(rs.getLong("r"));
                    shobeler.setId(rs.getLong("ID"));
                    shobeler.setUnvan(rs.getString("UNVAN"));
                    shobeler.setTel(rs.getString("TEL"));
                    shobeler.setIshVaxti(rs.getString("ISHVAXTI"));
                    shobelerList.add(shobeler);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return shobelerList;
    }

    @Override
    public List<KitabMekan> SearchKitabMekan(String search,int a) throws Exception {
        List<KitabMekan> kitabMekanList = new ArrayList<KitabMekan>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,M.ID M_ID, K.AD K_AD,K.MUELLIF K_MUELLIF,K.JANR K_JANR,K.SAYI K_SAYI,S.UNVAN S_UNVAN,S.TEL S_TEL,S.ISHVAXTI S_ISHVAXTI FROM KITABMEKAN M "
                + "INNER JOIN KITABXANA K ON M.K_ID = K.ID "
                + "INNER JOIN SHOBELER S ON M.S_ID = S.ID "
                + "WHERE (M.ACTIVE = ? AND K.ACTIVE = ? AND S.ACTIVE = ?) AND (LOWER(K.AD) LIKE LOWER('%" + search + "%') OR LOWER(K.MUELLIF) LIKE LOWER('%" + search + "%') "
                + " OR LOWER(K.JANR) LIKE LOWER('%" + search + "%') OR K.SAYI LIKE '%" + search + "%' OR LOWER(S.UNVAN) LIKE LOWER('%" + search + "%') OR LOWER(S.TEL) LIKE LOWER('%" + search + "%') OR LOWER(S.ISHVAXTI) LIKE LOWER('%" + search + "%'))";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                ps.setInt(2, a);
                ps.setInt(3, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabMekan kitabMekan = new KitabMekan();
                    Kitabxana kitabxana = new Kitabxana();
                    Shobeler shobeler = new Shobeler();
                    kitabMekan.setR(rs.getLong("r"));
                    kitabMekan.setId(rs.getLong("M_ID"));
                    kitabxana.setAd(rs.getString("K_AD"));
                    kitabxana.setMuellif(rs.getString("K_MUELLIF"));
                    kitabxana.setJanr(rs.getString("K_JANR"));
                    kitabxana.setSayi(rs.getInt("K_SAYI"));
                    shobeler.setUnvan(rs.getString("S_UNVAN"));
                    shobeler.setTel(rs.getString("S_TEL"));
                    shobeler.setIshVaxti(rs.getString("S_ISHVAXTI"));
                    kitabMekan.setKitabxana(kitabxana);
                    kitabMekan.setShobeler(shobeler);
                    kitabMekanList.add(kitabMekan);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabMekanList;
    }

    @Override
    public List<KitabOxu> SearchKitabOxu(String search,int a) throws Exception {
        List<KitabOxu> kitabOxuList = new ArrayList<KitabOxu>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,O.ID O_ID, I.NAME I_NAME ,I.SURNAME I_SURNAME, K.AD K_AD  FROM KITABOXU O "
                + "INNER JOIN KITABXANA K ON O.ID_K = K.ID "
                + "INNER JOIN ISTIFADECI I ON O.ID_I = I.ID "
                + "WHERE (O.ACTIVE = ? AND K.ACTIVE = ? AND I.ACTIVE = ?) AND ( LOWER(I.NAME) LIKE LOWER('%" + search + "%') OR LOWER(I.SURNAME) LIKE LOWER('%" + search + "%') OR LOWER(K.AD) LIKE LOWER('%" + search + "%' ))";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setInt(1, a);
                ps.setInt(2, a);
                ps.setInt(3, a);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabOxu kitabOxu = new KitabOxu();
                    Kitabxana kitabxana = new Kitabxana();
                    Istifadeci istifadeci = new Istifadeci();
                    kitabOxu.setR(rs.getLong("r"));
                    kitabOxu.setId(rs.getLong("O_ID"));
                    kitabxana.setAd(rs.getString("K_AD"));
                    istifadeci.setName(rs.getString("I_NAME"));
                    istifadeci.setSurname(rs.getString("I_SURNAME"));
                    kitabOxu.setKitabxana(kitabxana);
                    kitabOxu.setIstifadeci(istifadeci);
                    kitabOxuList.add(kitabOxu);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabOxuList;
    }

    @Override
    public List<Kitabxana> getKitabxanaDelList() throws Exception {
        List<Kitabxana> kitabxanaList = new ArrayList<Kitabxana>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r, ID,AD,MUELLIF,ILI,SAYI,JANR,QIYMET FROM KITABXANA "
                + " WHERE ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Kitabxana kitabxana = new Kitabxana();
                    kitabxana.setR(rs.getInt("r"));
                    kitabxana.setId(rs.getLong("ID"));
                    kitabxana.setAd(rs.getString("AD"));
                    kitabxana.setMuellif(rs.getString("MUELLIF"));
                    kitabxana.setIli(rs.getDate("ILI"));
                    kitabxana.setSayi(rs.getInt("SAYI"));
                    kitabxana.setJanr(rs.getString("JANR"));
                    kitabxana.setQiymet(rs.getString("QIYMET"));
                    kitabxanaList.add(kitabxana);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabxanaList;
    }

    @Override
    public List<Istifadeci> getIstifadeciDelList() throws Exception {
        List<Istifadeci> istifadeciList = new ArrayList<Istifadeci>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,ID,NAME,SURNAME,DOQUMGUNU,TEL,MAIL FROM ISTIFADECI "
                + " WHERE ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Istifadeci istifadeci = new Istifadeci();
                    istifadeci.setR(rs.getInt("r"));
                    istifadeci.setId(rs.getLong("ID"));
                    istifadeci.setName(rs.getString("NAME"));
                    istifadeci.setSurname(rs.getString("SURNAME"));
                    istifadeci.setDoqumGunu(rs.getDate("DOQUMGUNU"));
                    istifadeci.setTel(rs.getString("TEL"));
                    istifadeci.setMail(rs.getString("MAIL"));
                    istifadeciList.add(istifadeci);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return istifadeciList;
    }

    @Override
    public List<Shobeler> getShobelerDelList() throws Exception {
        List<Shobeler> shobelerList = new ArrayList<Shobeler>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r, ID,UNVAN,TEL,ISHVAXTI FROM SHOBELER "
                + " WHERE ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Shobeler shobeler = new Shobeler();
                    shobeler.setR(rs.getInt("r"));
                    shobeler.setId(rs.getLong("ID"));
                    shobeler.setUnvan(rs.getString("UNVAN"));
                    shobeler.setTel(rs.getString("TEL"));
                    shobeler.setIshVaxti(rs.getString("ISHVAXTI"));
                    shobelerList.add(shobeler);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return shobelerList;
    }

    @Override
    public List<KitabMekan> getKitabMekanDelList() throws Exception {
        List<KitabMekan> kitabMekanList = new ArrayList<KitabMekan>();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,M.ID M_ID, K.AD K_AD,K.MUELLIF K_MUELLIF,K.JANR K_JANR,K.SAYI K_SAYI,S.UNVAN S_UNVAN,S.TEL S_TEL,S.ISHVAXTI S_ISHVAXTI FROM KITABMEKAN M "
                + " INNER JOIN KITABXANA K ON M.K_ID = K.ID "
                + " INNER JOIN SHOBELER S ON M.S_ID = S.ID "
                + " WHERE M.ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabMekan km = new KitabMekan();
                    Kitabxana kitab = new Kitabxana();
                    Shobeler sobe = new Shobeler();
                    km.setR(rs.getLong("r"));
                    km.setId(rs.getLong("M_ID"));
                    kitab.setAd(rs.getString("K_AD"));
                    sobe.setTel(rs.getString("S_TEL"));
                    sobe.setIshVaxti(rs.getString("S_ISHVAXTI"));
                    kitab.setMuellif(rs.getString("K_MUELLIF"));
                    kitab.setJanr(rs.getString("K_JANR"));
                    kitab.setSayi(rs.getInt("K_SAYI"));
                    sobe.setUnvan(rs.getString("S_UNVAN"));
                    km.setKitabxana(kitab);
                    km.setShobeler(sobe);
                    kitabMekanList.add(km);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabMekanList;
    }

    @Override
    public List<KitabOxu> getKitabOxuDelList() throws Exception {
        List<KitabOxu> kitabOxuList = new ArrayList<KitabOxu>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT ROWNUM r,O.ID O_ID, I.NAME || ' ' || I.SURNAME ADI_SOYADI, K.AD OXUDUQU_KITABIN_ADI FROM KITABOXU O "
                + " INNER JOIN KITABXANA K ON O.ID_K = K.ID "
                + " INNER JOIN ISTIFADECI I ON O.ID_I = I.ID "
                + " WHERE O.ACTIVE = 0";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    KitabOxu oxu = new KitabOxu();
                    Kitabxana kitab = new Kitabxana();
                    Istifadeci adam = new Istifadeci();
                    oxu.setR(rs.getLong("r"));
                    oxu.setId(rs.getLong("O_ID"));
                    adam.setName(rs.getString("ADI_SOYADI"));
                    kitab.setAd(rs.getString("OXUDUQU_KITABIN_ADI"));
                    oxu.setIstifadeci(adam);
                    oxu.setKitabxana(kitab);
                    kitabOxuList.add(oxu);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return kitabOxuList;
    }

    @Override
    public boolean fullDeleteKitabxana(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM KITABXANA "
                + " WHERE ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connectin is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean fullDeleteIstifadeci(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM ISTIFADECI "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean fullDeleteShobeler(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM SHOBELER "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean fullDeletKitabMekan(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM KITABMEKAN "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean fullDeleteKitabOxu(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "DELETE FROM KITABOXU "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairKitabxana(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABXANA SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairIstifadeci(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE ISTIFADECI SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairShobeler(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE SHOBELER SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("COnnection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairKitabMekan(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABMEKAN SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean repairKitabOxu(long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "UPDATE KITABOXU SET ACTIVE = 1 "
                + " WHERE ID = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

}
