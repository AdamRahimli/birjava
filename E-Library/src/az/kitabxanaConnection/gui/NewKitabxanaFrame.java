/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.dao.TestDao;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;


/**
 *
 * @author Orxan
 */
public class NewKitabxanaFrame extends javax.swing.JFrame {

    private TestService ts;
    
    public NewKitabxanaFrame() {
        initComponents();
    }

    NewKitabxanaFrame(TestService ts) {
        initComponents();
        this.ts = ts;
    }

   
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kitabxanaNewTxt = new javax.swing.JLabel();
        kitabxanaAddBtn = new javax.swing.JButton();
        kitabxanaCancelBtn = new javax.swing.JButton();
        kitabxanaAdTxt = new javax.swing.JLabel();
        kitabxanaMuellifTxt = new javax.swing.JLabel();
        kitabxanaIliTxt = new javax.swing.JLabel();
        kitabxanaSayiTxt = new javax.swing.JLabel();
        kitabxanaJanrTxt = new javax.swing.JLabel();
        kitabxanaQiymetTxt = new javax.swing.JLabel();
        kitabxanaAdTxtField = new javax.swing.JTextField();
        kitabxanaMuellifTxtField = new javax.swing.JTextField();
        kitabxanaJanrTxtField = new javax.swing.JTextField();
        kitabxanaQiymetTxtField = new javax.swing.JTextField();
        kitabxanaSayiTxtField = new javax.swing.JSpinner();
        kitabxanaIliTxtField = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        kitabxanaNewTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        kitabxanaNewTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kitabxanaNewTxt.setText("Kitabxana(NEW)");

        kitabxanaAddBtn.setText("Add");
        kitabxanaAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabxanaAddBtnActionPerformed(evt);
            }
        });

        kitabxanaCancelBtn.setText("Cancel");
        kitabxanaCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabxanaCancelBtnActionPerformed(evt);
            }
        });

        kitabxanaAdTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaAdTxt.setText("Ad");

        kitabxanaMuellifTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaMuellifTxt.setText("Muellif");

        kitabxanaIliTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaIliTxt.setText("Ili");

        kitabxanaSayiTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaSayiTxt.setText("Sayi");

        kitabxanaJanrTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaJanrTxt.setText("Janr");

        kitabxanaQiymetTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        kitabxanaQiymetTxt.setText("Qiymet");

        kitabxanaSayiTxtField.setModel(new javax.swing.SpinnerNumberModel(0, 0, 1000000, 1));

        try {
            kitabxanaIliTxtField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.##.####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        kitabxanaIliTxtField.setToolTipText("Date format dd.mm.yyyy");
        kitabxanaIliTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabxanaIliTxtFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(kitabxanaNewTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(kitabxanaMuellifTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kitabxanaIliTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kitabxanaSayiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kitabxanaJanrTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kitabxanaQiymetTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(kitabxanaAdTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(75, 75, 75)
                                        .addComponent(kitabxanaAddBtn)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(kitabxanaCancelBtn))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(kitabxanaMuellifTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(kitabxanaAdTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(kitabxanaJanrTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(kitabxanaQiymetTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(kitabxanaSayiTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(kitabxanaIliTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(9, 9, 9)))))
                        .addGap(0, 15, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kitabxanaNewTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaAdTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaAdTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaMuellifTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaMuellifTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaIliTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaIliTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaSayiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaSayiTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaJanrTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaJanrTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaQiymetTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabxanaQiymetTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabxanaAddBtn)
                    .addComponent(kitabxanaCancelBtn))
                .addContainerGap())
        );

        kitabxanaSayiTxtField.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kitabxanaCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabxanaCancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_kitabxanaCancelBtnActionPerformed

    private void kitabxanaAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabxanaAddBtnActionPerformed
        try{         
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            Kitabxana kitab = new Kitabxana();
            kitab.setAd(kitabxanaAdTxtField.getText());
            kitab.setMuellif(kitabxanaMuellifTxtField.getText());
            kitab.setIli(df.parse(kitabxanaIliTxtField.getText()));
            kitab.setSayi((int) kitabxanaSayiTxtField.getValue());
            kitab.setJanr(kitabxanaJanrTxtField.getText());
            kitab.setQiymet(kitabxanaQiymetTxtField.getText());
            boolean result = ts.addKitabxana(kitab);
            if(result){
                List<Kitabxana> kitabxanaList = ts.getKitabxanaList();
                Methods.showKitabxanaData(MainFrame.allDataTbl, kitabxanaList);
                this.dispose();
                JOptionPane.showConfirmDialog(null, "Elave olundu...", "Complete", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_kitabxanaAddBtnActionPerformed

    private void kitabxanaIliTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabxanaIliTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kitabxanaIliTxtFieldActionPerformed

    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel kitabxanaAdTxt;
    private javax.swing.JTextField kitabxanaAdTxtField;
    private javax.swing.JButton kitabxanaAddBtn;
    private javax.swing.JButton kitabxanaCancelBtn;
    private javax.swing.JLabel kitabxanaIliTxt;
    private javax.swing.JFormattedTextField kitabxanaIliTxtField;
    private javax.swing.JLabel kitabxanaJanrTxt;
    private javax.swing.JTextField kitabxanaJanrTxtField;
    private javax.swing.JLabel kitabxanaMuellifTxt;
    private javax.swing.JTextField kitabxanaMuellifTxtField;
    private javax.swing.JLabel kitabxanaNewTxt;
    private javax.swing.JLabel kitabxanaQiymetTxt;
    private javax.swing.JTextField kitabxanaQiymetTxtField;
    private javax.swing.JLabel kitabxanaSayiTxt;
    private javax.swing.JSpinner kitabxanaSayiTxtField;
    // End of variables declaration//GEN-END:variables
}
