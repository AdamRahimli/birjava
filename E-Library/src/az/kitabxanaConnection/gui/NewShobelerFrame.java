/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.model.Shobeler;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Orxan
 */
public class NewShobelerFrame extends javax.swing.JFrame {

    private TestService ts;
    
    public NewShobelerFrame() {
        initComponents();
    }

    NewShobelerFrame(TestService ts) {
        initComponents();
        this.ts = ts;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        shobelerTxt = new javax.swing.JLabel();
        shobelerAddBtn = new javax.swing.JButton();
        shobelerCancelBtn = new javax.swing.JButton();
        shobelerUnvanTxt = new javax.swing.JLabel();
        shobelerTelTxt = new javax.swing.JLabel();
        shobelerIshvaxtiTxt = new javax.swing.JLabel();
        shobelerTelTxtField = new javax.swing.JTextField();
        shobelerUnvanTxtField = new javax.swing.JTextField();
        shobelerIshvaxtiTxtField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        shobelerTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        shobelerTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        shobelerTxt.setText("Shobeler(NEW)");

        shobelerAddBtn.setText("Add");
        shobelerAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shobelerAddBtnActionPerformed(evt);
            }
        });

        shobelerCancelBtn.setText("Cancel");
        shobelerCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shobelerCancelBtnActionPerformed(evt);
            }
        });

        shobelerUnvanTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        shobelerUnvanTxt.setText("Unvan");

        shobelerTelTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        shobelerTelTxt.setText("Tel");

        shobelerIshvaxtiTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        shobelerIshvaxtiTxt.setText("Ish Vaxti");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(shobelerTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(shobelerAddBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(shobelerCancelBtn)
                        .addGap(0, 15, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(shobelerIshvaxtiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(shobelerIshvaxtiTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(shobelerTelTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(shobelerTelTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(shobelerUnvanTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(shobelerUnvanTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(shobelerTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shobelerUnvanTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shobelerUnvanTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shobelerTelTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shobelerTelTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shobelerIshvaxtiTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shobelerIshvaxtiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 151, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shobelerAddBtn)
                    .addComponent(shobelerCancelBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void shobelerCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shobelerCancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_shobelerCancelBtnActionPerformed

    private void shobelerAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shobelerAddBtnActionPerformed
        
        try {
            Shobeler sobe = new Shobeler();
            sobe.setUnvan(shobelerUnvanTxtField.getText());
            sobe.setTel(shobelerTelTxtField.getText());
            sobe.setIshVaxti(shobelerIshvaxtiTxtField.getText());
            boolean result = ts.addShobeler(sobe);
            if(result){
                List<Shobeler> shobelerList = ts.getShobeler();
                Methods.showShobelerData(MainFrame.allDataTbl, shobelerList);
                this.dispose();
                JOptionPane.showConfirmDialog(null, "Elave olundu...", "Complete", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_shobelerAddBtnActionPerformed

    /**
     * @param args the command line arguments
     */
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton shobelerAddBtn;
    private javax.swing.JButton shobelerCancelBtn;
    private javax.swing.JLabel shobelerIshvaxtiTxt;
    private javax.swing.JTextField shobelerIshvaxtiTxtField;
    private javax.swing.JLabel shobelerTelTxt;
    private javax.swing.JTextField shobelerTelTxtField;
    private javax.swing.JLabel shobelerTxt;
    private javax.swing.JLabel shobelerUnvanTxt;
    private javax.swing.JTextField shobelerUnvanTxtField;
    // End of variables declaration//GEN-END:variables
}
