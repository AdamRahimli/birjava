/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Orxan
 */
public class NewIstifadeciFrame extends javax.swing.JFrame {

    private TestService ts;

    public NewIstifadeciFrame() {
        initComponents();
    }

    NewIstifadeciFrame(TestService ts) {
        initComponents();
        this.ts = ts;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        istifadeciNewTxt = new javax.swing.JLabel();
        istifadeciAddBtn = new javax.swing.JButton();
        istifadeciCancelBtn = new javax.swing.JButton();
        istifadeciNameTxtField = new javax.swing.JTextField();
        istifadeciSurnameTxtField = new javax.swing.JTextField();
        istifadeciTelTxtField = new javax.swing.JTextField();
        istifadeciMailTxtField = new javax.swing.JTextField();
        istifadeciNameTxt = new javax.swing.JLabel();
        istifadeciSurnameTxt = new javax.swing.JLabel();
        istifadeciDoqumgunuTxt = new javax.swing.JLabel();
        istifadeciTelTxt = new javax.swing.JLabel();
        istifadeciMailTxt = new javax.swing.JLabel();
        istifadeciDoqumgunuTxtField = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        istifadeciNewTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        istifadeciNewTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        istifadeciNewTxt.setText("Istifadeci(NEW)");

        istifadeciAddBtn.setText("Add");
        istifadeciAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                istifadeciAddBtnActionPerformed(evt);
            }
        });

        istifadeciCancelBtn.setText("Cancel");
        istifadeciCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                istifadeciCancelBtnActionPerformed(evt);
            }
        });

        istifadeciNameTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        istifadeciNameTxt.setText("Name");

        istifadeciSurnameTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        istifadeciSurnameTxt.setText("Surname");

        istifadeciDoqumgunuTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        istifadeciDoqumgunuTxt.setText("Doqumgunu");

        istifadeciTelTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        istifadeciTelTxt.setText("Tel");

        istifadeciMailTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        istifadeciMailTxt.setText("eMail");

        try {
            istifadeciDoqumgunuTxtField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.##.####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        istifadeciDoqumgunuTxtField.setToolTipText("Date format dd.mm.yyyy");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(istifadeciNewTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(istifadeciSurnameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(istifadeciSurnameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(istifadeciNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(istifadeciNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(istifadeciMailTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(istifadeciTelTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(istifadeciDoqumgunuTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(istifadeciMailTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                            .addComponent(istifadeciTelTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                            .addComponent(istifadeciDoqumgunuTxtField))))
                                .addGap(0, 31, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(istifadeciAddBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(istifadeciCancelBtn)
                        .addGap(18, 18, 18))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(istifadeciNewTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(istifadeciNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(istifadeciNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(istifadeciSurnameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(istifadeciSurnameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(istifadeciDoqumgunuTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(istifadeciDoqumgunuTxtField))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(istifadeciTelTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(istifadeciTelTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(istifadeciMailTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(istifadeciMailTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(istifadeciAddBtn)
                    .addComponent(istifadeciCancelBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void istifadeciCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_istifadeciCancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_istifadeciCancelBtnActionPerformed

    private void istifadeciAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_istifadeciAddBtnActionPerformed
        try {
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            Istifadeci adam = new Istifadeci();
            adam.setName(istifadeciNameTxtField.getText());
            adam.setSurname(istifadeciSurnameTxtField.getText());
            adam.setDoqumGunu(df.parse(istifadeciDoqumgunuTxtField.getText()));
            adam.setTel(istifadeciTelTxtField.getText());
            adam.setMail(istifadeciMailTxtField.getText());
            boolean result = ts.addIstifadeci(adam);                                
            if(result){
                List<Istifadeci> istifadeciList = ts.getIstifadeci();
                Methods.showIstifadeciData(MainFrame.allDataTbl, istifadeciList);
                this.dispose();
                JOptionPane.showConfirmDialog(null, "Elave olundu...", "Complete", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_istifadeciAddBtnActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton istifadeciAddBtn;
    private javax.swing.JButton istifadeciCancelBtn;
    private javax.swing.JLabel istifadeciDoqumgunuTxt;
    private javax.swing.JFormattedTextField istifadeciDoqumgunuTxtField;
    private javax.swing.JLabel istifadeciMailTxt;
    private javax.swing.JTextField istifadeciMailTxtField;
    private javax.swing.JLabel istifadeciNameTxt;
    private javax.swing.JTextField istifadeciNameTxtField;
    private javax.swing.JLabel istifadeciNewTxt;
    private javax.swing.JLabel istifadeciSurnameTxt;
    private javax.swing.JTextField istifadeciSurnameTxtField;
    private javax.swing.JLabel istifadeciTelTxt;
    private javax.swing.JTextField istifadeciTelTxtField;
    // End of variables declaration//GEN-END:variables
}
