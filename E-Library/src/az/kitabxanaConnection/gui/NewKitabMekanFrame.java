/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.model.Item;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Shobeler;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Orxan
 */
public class NewKitabMekanFrame extends javax.swing.JFrame {

    private TestService ts;
    private List<Kitabxana> kitablar;
    private List<Shobeler> shobeler;
    
    public NewKitabMekanFrame() {
        initComponents();
    }

    NewKitabMekanFrame(TestService ts) {
        initComponents();
        this.ts = ts;
        readInitialData();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kitabMekanAddBtn = new javax.swing.JButton();
        kitabMekanCancelBtn = new javax.swing.JButton();
        kitabMekanNewTxt = new javax.swing.JLabel();
        kitabMekanKitabCmb = new javax.swing.JComboBox<>();
        kitabMekanSobeCmb = new javax.swing.JComboBox<>();
        kitabMekanKitabTxt = new javax.swing.JLabel();
        kitabMekanSobeTxt = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        kitabMekanAddBtn.setText("Add");
        kitabMekanAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabMekanAddBtnActionPerformed(evt);
            }
        });

        kitabMekanCancelBtn.setText("Cancel");
        kitabMekanCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabMekanCancelBtnActionPerformed(evt);
            }
        });

        kitabMekanNewTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        kitabMekanNewTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kitabMekanNewTxt.setLabelFor(this);
        kitabMekanNewTxt.setText("KitabMekan(NEW)");

        kitabMekanKitabTxt.setText("Kitab");

        kitabMekanSobeTxt.setText("Shobe");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(153, 153, 153)
                .addComponent(kitabMekanAddBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kitabMekanCancelBtn)
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kitabMekanNewTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(kitabMekanKitabTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kitabMekanSobeTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(kitabMekanKitabCmb, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kitabMekanSobeCmb, 0, 192, Short.MAX_VALUE))
                    .addGap(14, 14, 14)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kitabMekanNewTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 309, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabMekanAddBtn)
                    .addComponent(kitabMekanCancelBtn))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(177, 177, 177)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(kitabMekanKitabCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(kitabMekanKitabTxt))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(kitabMekanSobeTxt)
                        .addComponent(kitabMekanSobeCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(177, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kitabMekanCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabMekanCancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_kitabMekanCancelBtnActionPerformed

    private void kitabMekanAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabMekanAddBtnActionPerformed
        try {
            KitabMekan kitabMekan = new KitabMekan();
            Item itemKitab = (Item) kitabMekanKitabCmb.getSelectedItem();
            Item itemSobe = (Item) kitabMekanSobeCmb.getSelectedItem();
            kitabMekan.setK_id(itemKitab.getId());
            kitabMekan.setS_id(itemSobe.getId());
            boolean result = ts.addKitabMekan(kitabMekan);
            if(result){
                List<KitabMekan> kitabMekanList = ts.getKitabMekan();
                Methods.showKitabMekanData(MainFrame.allDataTbl, kitabMekanList);
                this.dispose();
                JOptionPane.showConfirmDialog(null, "Elave olundu...", "Complete", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_kitabMekanAddBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton kitabMekanAddBtn;
    private javax.swing.JButton kitabMekanCancelBtn;
    private javax.swing.JComboBox<String> kitabMekanKitabCmb;
    private javax.swing.JLabel kitabMekanKitabTxt;
    private javax.swing.JLabel kitabMekanNewTxt;
    private javax.swing.JComboBox<String> kitabMekanSobeCmb;
    private javax.swing.JLabel kitabMekanSobeTxt;
    // End of variables declaration//GEN-END:variables

    private void readInitialData() {
        try {
            DefaultComboBoxModel bookCmb = (DefaultComboBoxModel) kitabMekanKitabCmb.getModel();
            kitablar = ts.getKitabxanaList();
            for(int i = 0; i<kitablar.size();i++){
                bookCmb.addElement(new Item(kitablar.get(i).getId(), kitablar.get(i).getAd()));
            }
            DefaultComboBoxModel placeCmb = (DefaultComboBoxModel) kitabMekanSobeCmb.getModel();
            shobeler = ts.getShobeler();
            for(int i = 0; i<shobeler.size();i++){
                placeCmb.addElement(new Item(shobeler.get(i).getId(), shobeler.get(i).getUnvan()));
            }    
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
    }
}
