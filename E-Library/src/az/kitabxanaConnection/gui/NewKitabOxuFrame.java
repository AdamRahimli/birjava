/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.Item;
import az.kitabxanaConnection.model.KitabOxu;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Orxan
 */
public class NewKitabOxuFrame extends javax.swing.JFrame {

    private TestService ts;
    private List<Kitabxana> kitablar;
    private List<Istifadeci> istifadechiler;


    public NewKitabOxuFrame() {
        initComponents();
    }

    NewKitabOxuFrame(TestService ts) {
        initComponents();
        this.ts = ts;
        readInitialData();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kitabOxuNewTxt = new javax.swing.JLabel();
        KitabOxuAddBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        kitabOxuKitabCmb = new javax.swing.JComboBox<>();
        kitabOxuKitabTxt = new javax.swing.JLabel();
        kitabOxuOxuyanTxt = new javax.swing.JLabel();
        kitabOxuOxuyanCmb = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        kitabOxuNewTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        kitabOxuNewTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kitabOxuNewTxt.setText("KitabOxu(NEW)");

        KitabOxuAddBtn.setText("Add");
        KitabOxuAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KitabOxuAddBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Cancel");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        kitabOxuKitabTxt.setText("Kitab");

        kitabOxuOxuyanTxt.setText("Oxuyan");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(153, 153, 153)
                .addComponent(KitabOxuAddBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelBtn)
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(kitabOxuNewTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(kitabOxuOxuyanTxt)
                            .addComponent(kitabOxuKitabTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(kitabOxuKitabCmb, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(kitabOxuOxuyanCmb, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kitabOxuNewTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(108, 108, 108)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabOxuKitabCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kitabOxuKitabTxt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabOxuOxuyanTxt)
                    .addComponent(kitabOxuOxuyanCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 161, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(KitabOxuAddBtn)
                    .addComponent(cancelBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void KitabOxuAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KitabOxuAddBtnActionPerformed
        try {
            KitabOxu kitabOxu = new KitabOxu();
            Item itemKitab = (Item) kitabOxuKitabCmb.getSelectedItem();
            Item itemAdam = (Item) kitabOxuOxuyanCmb.getSelectedItem();
            kitabOxu.setId_k(itemKitab.getId());
            kitabOxu.setId_i(itemAdam.getId());
            boolean result = ts.addKitabOxu(kitabOxu);
            if (result) {
                List<KitabOxu> kitabOxuList = ts.getKitabOxu();
                Methods.showKitabOxuData(MainFrame.allDataTbl, kitabOxuList);
                this.dispose();
                JOptionPane.showConfirmDialog(null, "Elave olundu...", "Complete", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_KitabOxuAddBtnActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton KitabOxuAddBtn;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JComboBox<String> kitabOxuKitabCmb;
    private javax.swing.JLabel kitabOxuKitabTxt;
    private javax.swing.JLabel kitabOxuNewTxt;
    private javax.swing.JComboBox<String> kitabOxuOxuyanCmb;
    private javax.swing.JLabel kitabOxuOxuyanTxt;
    // End of variables declaration//GEN-END:variables

    private void readInitialData() {
        try {
            DefaultComboBoxModel readerCmb = (DefaultComboBoxModel) kitabOxuOxuyanCmb.getModel();
            kitablar = ts.getKitabxanaList();
            istifadechiler = ts.getIstifadeci();
            for (int i = 0; i < istifadechiler.size(); i++) {
                readerCmb.addElement(new Item(istifadechiler.get(i).getId(), istifadechiler.get(i).getName() + "-" + istifadechiler.get(i).getSurname()));
            }
            DefaultComboBoxModel bookCmb = (DefaultComboBoxModel) kitabOxuKitabCmb.getModel();
            for (int i = 0; i < kitablar.size(); i++) {
                bookCmb.addElement(new Item(kitablar.get(i).getId(), kitablar.get(i).getAd()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
