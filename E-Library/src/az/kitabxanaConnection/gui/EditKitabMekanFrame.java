/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.gui;

import az.kitabxanaConnection.model.Item;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Shobeler;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.util.Methods;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Orxan
 */
public class EditKitabMekanFrame extends javax.swing.JFrame {

    private TestService ts;
    private long id;

    private List<Kitabxana> kitablar;
    private List<Shobeler> sobeler;

    public EditKitabMekanFrame() {
        initComponents();
    }

    EditKitabMekanFrame(TestService ts, long id) {
        initComponents();
        this.ts = ts;
        this.id = id;
        showKitabMekanInfo();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kitabMekanSobeTxt = new javax.swing.JLabel();
        kitabMekanKitabCmb = new javax.swing.JComboBox<>();
        kitabMekanUpdateBtn = new javax.swing.JButton();
        kitabMekanSobeCmb = new javax.swing.JComboBox<>();
        kitabMekanEditTxt = new javax.swing.JLabel();
        kitabMekanKitabTxt = new javax.swing.JLabel();
        kitabMekanCancelBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        kitabMekanSobeTxt.setText("Shobe");

        kitabMekanUpdateBtn.setText("Update");
        kitabMekanUpdateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabMekanUpdateBtnActionPerformed(evt);
            }
        });

        kitabMekanEditTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        kitabMekanEditTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kitabMekanEditTxt.setText("KitabMekan(EDIT)");

        kitabMekanKitabTxt.setText("Kitab");

        kitabMekanCancelBtn.setText("Cancel");
        kitabMekanCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kitabMekanCancelBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(kitabMekanKitabTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kitabMekanKitabCmb, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(kitabMekanSobeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kitabMekanSobeCmb, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(kitabMekanUpdateBtn)
                                .addGap(18, 18, 18)
                                .addComponent(kitabMekanCancelBtn))
                            .addComponent(kitabMekanEditTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kitabMekanEditTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(111, 111, 111)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabMekanKitabTxt)
                    .addComponent(kitabMekanKitabCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabMekanSobeTxt)
                    .addComponent(kitabMekanSobeCmb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 146, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitabMekanCancelBtn)
                    .addComponent(kitabMekanUpdateBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kitabMekanUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabMekanUpdateBtnActionPerformed
        try {
            KitabMekan kitabMekan = new KitabMekan();
            Item itemKitab = (Item) kitabMekanKitabCmb.getSelectedItem();
            Item itemSobe = (Item) kitabMekanSobeCmb.getSelectedItem();
            kitabMekan.setK_id(itemKitab.getId());
            kitabMekan.setS_id(itemSobe.getId());
            boolean result = ts.updateKitabMekan(kitabMekan, id);
            if(result){
                List<KitabMekan> kitabMekanList = ts.getKitabMekan();
                Methods.showKitabMekanData(MainFrame.allDataTbl, kitabMekanList);
                JOptionPane.showConfirmDialog(null, "Deyisiklik oldu...", "Update", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }else{
                JOptionPane.showConfirmDialog(null, "Sehv bash verdi!!!", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showConfirmDialog(null, "Elave olunmadi...", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_kitabMekanUpdateBtnActionPerformed

    private void kitabMekanCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kitabMekanCancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_kitabMekanCancelBtnActionPerformed

    private void showKitabMekanInfo() {
        try {
            KitabMekan kitabMekan = ts.getKitabMekanByID(id);
            Kitabxana kitabxana = ts.getKitabxanaByID(kitabMekan.getK_id());
            Shobeler shobeler = ts.getShobelerByID(kitabMekan.getS_id());
            DefaultComboBoxModel bookCmb = (DefaultComboBoxModel) kitabMekanKitabCmb.getModel();
            DefaultComboBoxModel placeCmb = (DefaultComboBoxModel) kitabMekanSobeCmb.getModel();
            bookCmb.addElement(new Item(kitabxana.getId(),kitabxana.getAd()));
            placeCmb.addElement(new Item(shobeler.getId(),shobeler.getUnvan()));
            kitablar = ts.getKitabxanaList();
            sobeler = ts.getShobeler();
            for (int i = 0; i < kitablar.size(); i++) {
                if(kitablar.get(i).getId() == kitabxana.getId())
                    continue;
                bookCmb.addElement(new Item(kitablar.get(i).getId(), kitablar.get(i).getAd()));
            }       
            for (int i = 0; i < sobeler.size(); i++) {
                if(sobeler.get(i).getId() == shobeler.getId())
                    continue;
                placeCmb.addElement(new Item(sobeler.get(i).getId(), sobeler.get(i).getUnvan()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton kitabMekanCancelBtn;
    private javax.swing.JLabel kitabMekanEditTxt;
    private javax.swing.JComboBox<String> kitabMekanKitabCmb;
    private javax.swing.JLabel kitabMekanKitabTxt;
    private javax.swing.JComboBox<String> kitabMekanSobeCmb;
    private javax.swing.JLabel kitabMekanSobeTxt;
    private javax.swing.JButton kitabMekanUpdateBtn;
    // End of variables declaration//GEN-END:variables
}
