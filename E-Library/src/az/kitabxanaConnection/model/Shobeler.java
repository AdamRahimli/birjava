package az.kitabxanaConnection.model;

public class Shobeler extends Model{

	private String unvan;
	private String tel;
	private String ishVaxti;
	
	public String getUnvan() {
		return unvan;
	}
	public void setUnvan(String unvan) {
		this.unvan = unvan;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getIshVaxti() {
		return ishVaxti;
	}
	public void setIshVaxti(String ishVaxti) {
		this.ishVaxti = ishVaxti;
	}
    
}
