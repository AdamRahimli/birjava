package az.kitabxanaConnection.model;

import java.util.Date;

public class Kitabxana extends Model{

	private String ad;
	private String muellif;
	private Date ili;
	private int    sayi;
	private String janr;
	private String qiymet;
	
	public String getAd() {
		return ad;
	}
	public void setAd(String ad) {
		this.ad = ad;
	}
	public String getMuellif() {
		return muellif;
	}
	public void setMuellif(String muellif) {
		this.muellif = muellif;
	}
	public java.util.Date getIli() {
		return ili;
	}
	public void setIli(java.util.Date date) {
		this.ili = date;
	}
	public int getSayi() {
		return sayi;
	}
	public void setSayi(int sayi) {
		this.sayi = sayi;
	}
	public String getJanr() {
		return janr;
	}
	public void setJanr(String janr) {
		this.janr = janr;
	}
	public String getQiymet() {
		return qiymet;
	}
	public void setQiymet(String qiymet) {
		this.qiymet = qiymet;
	}

    
}
