package az.kitabxanaConnection.model;
 
import java.util.Date;

public class Istifadeci extends Model{
    private String name;
	private String surname;
	private Date doqumGunu;
	private String tel;
	private String mail;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public java.util.Date getDoqumGunu() {
		return doqumGunu;
	}
	public void setDoqumGunu(java.util.Date date) {
		this.doqumGunu =  date;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
}
