package az.kitabxanaConnection.model;

public class KitabMekan extends Model{
    
        
        private long k_id;
	private long s_id;
	private Kitabxana kitabxana;
	private Shobeler shobeler;
	
	public long getK_id() {
		return k_id;
	}
	public void setK_id(long k_id) {
		this.k_id = k_id;
	}
	public long getS_id() {
		return s_id;
	}
	public void setS_id(long s_id) {
		this.s_id = s_id;
	}
	public Kitabxana getKitabxana() {
		return kitabxana;
	}
	public void setKitabxana(Kitabxana kitabxana) {
		this.kitabxana = kitabxana;
	}
	public Shobeler getShobeler() {
		return shobeler;
	}
	public void setShobeler(Shobeler shobeler) {
		this.shobeler = shobeler;
	}    
}
