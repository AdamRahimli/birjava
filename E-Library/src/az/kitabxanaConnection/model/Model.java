package az.kitabxanaConnection.model;

public class Model {

	private long id;
	private int  active;
        private long r;
        
        
	public long getR(){
            return r;
        }
        public void setR(long r){
            this.r = r;
        }
        
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
    
}
