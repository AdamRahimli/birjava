package az.kitabxanaConnection.model;

public class KitabOxu extends Model{
    
    	private long id_i;
	private long id_k;
	private Istifadeci istifadeci;
	private Kitabxana kitabxana;
	
	public long getId_i() {
		return id_i;
	}
	public void setId_i(long id_i) {
		this.id_i = id_i;
	}
	public long getId_k() {
		return id_k;
	}
	public void setId_k(long id_k) {
		this.id_k = id_k;
	}
	public Istifadeci getIstifadeci() {
		return istifadeci;
	}
	public void setIstifadeci(Istifadeci istifadeci) {
		this.istifadeci = istifadeci;
	}
	public Kitabxana getKitabxana() {
		return kitabxana;
	}
	public void setKitabxana(Kitabxana kitabxana) {
		this.kitabxana = kitabxana;
	}
        
}
