package az.kitabxanaConnection.servic;

import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.KitabOxu;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Shobeler;
import java.util.List;

public interface TestService {

	public List<Kitabxana> getKitabxanaList() throws Exception;
	public List<Istifadeci> getIstifadeci() throws Exception;
	public List<Shobeler> getShobeler() throws Exception;
	public List<KitabMekan> getKitabMekan() throws Exception;
	public List<KitabOxu> getKitabOxu() throws Exception;

	public boolean addKitabxana(Kitabxana kitab) throws Exception;
	public boolean addIstifadeci(Istifadeci adam ) throws Exception;
	public boolean addShobeler(Shobeler sobe) throws Exception;
	public boolean addKitabMekan(KitabMekan mekan) throws Exception;
	public boolean addKitabOxu(KitabOxu oxu) throws Exception;

	public Kitabxana getKitabxanaByID(long kitabID) throws Exception;
	public Istifadeci getIstifadeciByID(long adamID) throws Exception;
	public Shobeler getShobelerByID(long sobeID) throws Exception;
	public KitabMekan getKitabMekanByID(long mekanID) throws Exception;
	public KitabOxu getKitabOxuByID(long oxuID) throws Exception;
	
	public boolean updateKitabxana(Kitabxana kitab,long kitabID) throws Exception;
	public boolean updateIstifadeci(Istifadeci adam, long adamID) throws Exception;
	public boolean updateShobeler(Shobeler sobe, long sobeID) throws Exception;
	public boolean updateKitabMekan(KitabMekan mekan, long mekanID) throws Exception;
	public boolean updateKitabOxu(KitabOxu oxu, long oxuID) throws Exception;
	
	public boolean deleteKitabxana(long kitabID) throws Exception;
	public boolean deleteIstifadeci(long adamID) throws Exception;
	public boolean deleteShobeler(long sobeID) throws Exception;
	public boolean deleteKitabMekan(long mekanID) throws Exception;
	public boolean deleteKitabOxu(long oxuID) throws Exception;
        
        public List<Kitabxana> SearchKitabxana(String search,int a) throws Exception;
        public List<Istifadeci> SearchIstifadeci(String search,int a) throws Exception;
        public List<Shobeler> SearchShobeler(String search,int a) throws Exception;
        public List<KitabMekan> SearchKitabMekan(String search,int a) throws Exception;
        public List<KitabOxu> SearchKitabOxu(String search,int a) throws Exception;
        
        public List<Kitabxana> getKitabxanaDelList()throws Exception;
        public List<Istifadeci> getIstifadeciDelList() throws Exception;
        public List<Shobeler> getShobelerDelList() throws Exception;
        public List<KitabMekan> getKitabMekanDelList() throws Exception;
        public List<KitabOxu> getKitabOxuDelList() throws Exception;
        
        public boolean fullDeleteKitabxana(long id)throws Exception;
        public boolean fullDeleteIstifadeci(long id)throws Exception;
        public boolean fullDeleteShobeler(long id)throws Exception;
        public boolean fullDeletKitabMekan(long id)throws Exception;
        public boolean fullDeleteKitabOxu(long id) throws Exception;
        
        public boolean repairKitabxana(long id) throws Exception;
        public boolean repairIstifadeci(long id) throws Exception;
        public boolean repairShobeler(long id) throws Exception;
        public boolean repairKitabMekan(long id) throws Exception;
        public boolean repairKitabOxu(long id) throws Exception;

    
}
