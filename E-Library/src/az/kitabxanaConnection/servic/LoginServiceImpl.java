/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.kitabxanaConnection.servic;

import az.kitabxanaConnection.dao.LoginDao;
import az.kitabxanaConnection.model.Login;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class LoginServiceImpl implements LoginService {

    private LoginDao loginDao;

    public LoginServiceImpl(LoginDao loginDao) {
        this.loginDao = loginDao;
    }

    public Login login(String username, String password) throws Exception {
        return loginDao.login(username, password);
    }

    @Override
    public List<Login> getLoginList() throws Exception {
        return loginDao.getLoginList();
    }

    @Override
    public Login getLoginByList(long id) throws Exception {
        return loginDao.getLoginByList(id);
    }

    @Override
    public boolean addLogin(Login login) throws Exception {
        return loginDao.addLogin(login);
    }

    @Override
    public boolean updateLogin(Login login, long id) throws Exception {
        return loginDao.updateLogin(login, id);
    }

    @Override
    public boolean deleteLogin(long id) throws Exception {
        return loginDao.deleteLogin(id);
    }

    @Override
    public List<Login> searchLoginList(String search,int a) throws Exception {
        return loginDao.searchLoginList(search,a);
    }

    @Override
    public List<Login> getDeleteLoginList() throws Exception {
        return loginDao.getDeleteLoginList();
    }

    @Override
    public boolean fullDeleteLogin(long id) throws Exception {
        return loginDao.fullDeleteLogin(id);
    }

    @Override
    public boolean repairLogin(long id) throws Exception {
        return loginDao.repairLogin(id);
    }

}
