package az.kitabxanaConnection.servic;

import az.kitabxanaConnection.dao.TestDao;
import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.KitabOxu;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Shobeler;
import java.util.List;

public class TestSeviceImpl implements TestService {

    private TestDao testDao;

    public TestSeviceImpl(TestDao td) {
        testDao = td;
    }

    @Override
    public List<Kitabxana> getKitabxanaList() throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabxanaList();
    }

    @Override
    public List<Istifadeci> getIstifadeci() throws Exception {
        // TODO Auto-generated method stub
        return testDao.getIstifadeci();
    }

    @Override
    public List<Shobeler> getShobeler() throws Exception {
        // TODO Auto-generated method stub
        return testDao.getShobeler();
    }

    @Override
    public List<KitabMekan> getKitabMekan() throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabMekan();
    }

    @Override
    public List<KitabOxu> getKitabOxu() throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabOxu();
    }

    @Override
    public boolean addKitabxana(Kitabxana kitab) throws Exception {
        // TODO Auto-generated method stub
        return testDao.addKitabxana(kitab);
    }

    @Override
    public boolean addIstifadeci(Istifadeci adam) throws Exception {
        // TODO Auto-generated method stub
        return testDao.addIstifadeci(adam);
    }

    @Override
    public boolean addShobeler(Shobeler sobe) throws Exception {
        // TODO Auto-generated method stub
        return testDao.addShobeler(sobe);
    }

    @Override
    public boolean addKitabMekan(KitabMekan mekan) throws Exception {
        // TODO Auto-generated method stub
        return testDao.addKitabMekan(mekan);
    }

    @Override
    public boolean addKitabOxu(KitabOxu oxu) throws Exception {
        // TODO Auto-generated method stub
        return testDao.addKitabOxu(oxu);
    }

    @Override
    public Kitabxana getKitabxanaByID(long kitabID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabxanaByID(kitabID);
    }

    @Override
    public Istifadeci getIstifadeciByID(long adamID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.getIstifadeciByID(adamID);
    }

    @Override
    public Shobeler getShobelerByID(long sobeID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.getShobelerByID(sobeID);
    }

    @Override
    public KitabMekan getKitabMekanByID(long mekanID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabMekanByID(mekanID);
    }

    @Override
    public KitabOxu getKitabOxuByID(long oxuID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.getKitabOxuByID(oxuID);
    }

    @Override
    public boolean updateKitabxana(Kitabxana kitab, long kitabID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.updateKitabxana(kitab, kitabID);
    }

    @Override
    public boolean updateIstifadeci(Istifadeci adam, long adamID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.updateIstifadeci(adam, adamID);
    }

    @Override
    public boolean updateShobeler(Shobeler sobe, long sobeID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.updateShobeler(sobe, sobeID);
    }

    @Override
    public boolean updateKitabMekan(KitabMekan mekan, long mekanID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.updateKitabMekan(mekan, mekanID);
    }

    @Override
    public boolean updateKitabOxu(KitabOxu oxu, long oxuID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.updateKitabOxu(oxu, oxuID);
    }

    @Override
    public boolean deleteKitabxana(long kitabID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.deleteKitabxana(kitabID);
    }

    @Override
    public boolean deleteIstifadeci(long adamID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.deleteIstifadeci(adamID);
    }

    @Override
    public boolean deleteShobeler(long sobeID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.deleteShobeler(sobeID);
    }

    @Override
    public boolean deleteKitabMekan(long mekanID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.deleteKitabMekan(mekanID);
    }

    @Override
    public boolean deleteKitabOxu(long oxuID) throws Exception {
        // TODO Auto-generated method stub
        return testDao.deleteKitabOxu(oxuID);
    }

    @Override
    public List<Kitabxana> SearchKitabxana(String search,int a) throws Exception {
        return testDao.SearchKitabxana(search,a);
    }

    @Override
    public List<Istifadeci> SearchIstifadeci(String search,int a) throws Exception {
        return testDao.SearchIstifadeci(search,a);
    }

    @Override
    public List<Shobeler> SearchShobeler(String search,int a) throws Exception {
        return testDao.SearchShobeler(search,a);
    }

    @Override
    public List<KitabMekan> SearchKitabMekan(String search,int a) throws Exception {
        return testDao.SearchKitabMekan(search,a);
    }

    @Override
    public List<KitabOxu> SearchKitabOxu(String search,int a) throws Exception {
        return testDao.SearchKitabOxu(search,a);
    }

    @Override
    public List<Kitabxana> getKitabxanaDelList() throws Exception {
        return testDao.getKitabxanaDelList();
    }

    @Override
    public List<Istifadeci> getIstifadeciDelList() throws Exception {
        return testDao.getIstifadeciDelList();
    }

    @Override
    public List<Shobeler> getShobelerDelList() throws Exception {
        return testDao.getShobelerDelList();
    }

    @Override
    public List<KitabMekan> getKitabMekanDelList() throws Exception {
        return testDao.getKitabMekanDelList();
    }

    @Override
    public List<KitabOxu> getKitabOxuDelList() throws Exception {
        return testDao.getKitabOxuDelList();
    }

    @Override
    public boolean fullDeleteKitabxana(long id) throws Exception {
        return testDao.fullDeleteKitabxana(id);
    }

    @Override
    public boolean fullDeleteIstifadeci(long id) throws Exception {
        return testDao.fullDeleteIstifadeci(id);
    }

    @Override
    public boolean fullDeleteShobeler(long id) throws Exception {
        return testDao.fullDeleteShobeler(id);
    }

    @Override
    public boolean fullDeletKitabMekan(long id) throws Exception {
        return testDao.fullDeletKitabMekan(id);
    }

    @Override
    public boolean fullDeleteKitabOxu(long id) throws Exception {
        return testDao.fullDeleteKitabOxu(id);
    }

    @Override
    public boolean repairKitabxana(long id) throws Exception {
        return testDao.repairKitabxana(id);
    }

    @Override
    public boolean repairIstifadeci(long id) throws Exception {
        return testDao.repairIstifadeci(id);
    }

    @Override
    public boolean repairShobeler(long id) throws Exception {
        return testDao.repairShobeler(id);
    }

    @Override
    public boolean repairKitabMekan(long id) throws Exception {
        return testDao.repairKitabMekan(id);
    }

    @Override
    public boolean repairKitabOxu(long id) throws Exception {
        return testDao.repairKitabOxu(id);
    }

}
