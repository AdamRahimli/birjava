package az.kitabxanaConnection.util;

import az.kitabxanaConnection.dao.TestDao;
import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.KitabMekan;
import az.kitabxanaConnection.model.KitabOxu;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.model.Login;
import az.kitabxanaConnection.model.Shobeler;
import az.kitabxanaConnection.servic.LoginService;
import az.kitabxanaConnection.servic.TestService;
import java.util.List;
import javax.swing.DefaultRowSorter;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Methods {

    public static void getKitabxanaList(List<Kitabxana> kitabxanaList) {
        for (Kitabxana kitab : kitabxanaList) {
            System.out.println(kitab.getId() + "--" + kitab.getAd() + "--" + kitab.getMuellif() + "--" + kitab.getJanr() + "--" + kitab.getQiymet() + "--" + kitab.getSayi() + "--" + kitab.getIli());
        }
    }

    public static void getIstifadeciList(List<Istifadeci> istifadecisList) {
        for (Istifadeci adam : istifadecisList) {
            System.out.println(adam.getId() + "--" + adam.getName() + " " + adam.getSurname() + "--" + adam.getTel() + "--" + adam.getMail() + "--" + adam.getDoqumGunu());
        }
    }

    public static void getShobelerList(List<Shobeler> shobelersList) {
        for (Shobeler sobe : shobelersList) {
            System.out.println(sobe.getId() + "--" + sobe.getIshVaxti() + "--" + sobe.getTel() + "--" + sobe.getUnvan());
        }
    }

    public static void getKitabMekanList(List<KitabMekan> kitabMekanList) {
        for (KitabMekan km : kitabMekanList) {
            System.out.println(km.getKitabxana().getAd() + "--" + km.getKitabxana().getMuellif() + "--" + km.getKitabxana().getJanr() + "--" + km.getKitabxana().getSayi() + "--" + km.getShobeler().getUnvan() + "--" + km.getShobeler().getTel() + "--" + km.getShobeler().getIshVaxti());
        }
    }

    public static void getKitabOxuList(List<KitabOxu> kitabOxuList) {
        for (KitabOxu oxu : kitabOxuList) {
            System.out.println(oxu.getIstifadeci().getName() + "--" + oxu.getKitabxana().getAd());
        }
    }

    public static void showIstifadeciData(JTable alldataTable, List<Istifadeci> istifadeciList) {
        try {
            DefaultTableModel model = new DefaultTableModel() {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Name");
            model.addColumn("Surname");
            model.addColumn("Doqumgunu");
            model.addColumn("Tel");
            model.addColumn("Mail");

            for (Istifadeci adam : istifadeciList) {
                Object[] data = new Object[]{
                    adam.getId(), adam.getR(), adam.getName(), adam.getSurname(), adam.getDoqumGunu(), adam.getTel(), adam.getMail()
                };
                model.addRow(data);
            }
            
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showKitabxanaData(JTable alldataTable,  List<Kitabxana> kitabxanaList) {
        try {
            DefaultTableModel model = new DefaultTableModel() {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Ad");
            model.addColumn("Muellif");
            model.addColumn("Ili");
            model.addColumn("Sayi");
            model.addColumn("Janr");
            model.addColumn("Qiymet");

            for (Kitabxana kitab : kitabxanaList) {
                Object[] data = new Object[]{
                    kitab.getId(), kitab.getR(), kitab.getAd(), kitab.getMuellif(), kitab.getIli(), kitab.getSayi(), kitab.getJanr(), kitab.getQiymet()
                };
                model.addRow(data);
            }
            
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
                   
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showShobelerData(JTable alldataTable, List<Shobeler> shobelerList) {
        try {
            DefaultTableModel model = new DefaultTableModel() {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Unvan");
            model.addColumn("Tel");
            model.addColumn("Ish Vaxti");

            for (Shobeler sobe : shobelerList) {
                Object[] data = new Object[]{
                    sobe.getId(), sobe.getR(), sobe.getUnvan(), sobe.getTel(), sobe.getIshVaxti()
                };
                model.addRow(data);
            }
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showKitabMekanData(JTable alldataTable, List<KitabMekan> kitabMekanList) {
        try {
            DefaultTableModel model = new DefaultTableModel() {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Kitabin Adi");
            model.addColumn("Kitabin Muellifi");
            model.addColumn("Kitabin Janri");
            model.addColumn("Kitabin Sayi");
            model.addColumn("Shobenin Unvani");
            model.addColumn("Shobenin Teli");
            model.addColumn("Shobenin Ish Vaxti");

            for (KitabMekan mekan : kitabMekanList) {
                Object[] data = new Object[]{
                    mekan.getId(), mekan.getR(), mekan.getKitabxana().getAd(), mekan.getKitabxana().getMuellif(), mekan.getKitabxana().getJanr(), mekan.getKitabxana().getSayi(), mekan.getShobeler().getUnvan(), mekan.getShobeler().getTel(), mekan.getShobeler().getIshVaxti()
                };
                model.addRow(data);
            }
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showKitabOxuData(JTable alldataTable,List<KitabOxu> kitabOxuList) {
        try {
            DefaultTableModel model = new DefaultTableModel() {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Istifadecinin Adi");
            model.addColumn("Kitabin Adi");

            for (KitabOxu oxu : kitabOxuList) {
                Object[] data = new Object[]{
                    oxu.getId(), oxu.getR(), oxu.getIstifadeci().getName(), oxu.getKitabxana().getAd()
                };
                model.addRow(data);
            }
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
            
           
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showLoginData(JTable alldataTable,List<Login> loginList) {
        try{
            DefaultTableModel model = new DefaultTableModel(){
                public boolean isCellEditable(int row, int column) {
                    return false;
                }                
            };
            alldataTable.setModel(model);
            model.addColumn("ID");
            model.addColumn("№");
            model.addColumn("Username");
            model.addColumn("Password");
            model.addColumn("Role");
            model.addColumn("Name");
            model.addColumn("Surname");
            for(Login login: loginList){
                Object[] data = new Object[]{
                  login.getId(),login.getR(),login.getUsename(),login.getPassword(),login.getRole(),login.getName(),login.getSurname()
                };
                model.addRow(data);
            }
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMaxWidth(0);
            alldataTable.getColumnModel().getColumn(0).setMinWidth(0);
            model.fireTableDataChanged();
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
