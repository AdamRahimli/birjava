package az.kitabxanaConnection.main;

import az.kitabxanaConnection.dao.TestDao;
import az.kitabxanaConnection.dao.TestDaoImpl;
import az.kitabxanaConnection.gui.LoginFrame;
import az.kitabxanaConnection.gui.MainFrame;
import az.kitabxanaConnection.model.Istifadeci;
import az.kitabxanaConnection.model.Kitabxana;
import az.kitabxanaConnection.servic.TestService;
import az.kitabxanaConnection.servic.TestSeviceImpl;
import az.kitabxanaConnection.util.Methods;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainClass {

    public static void main(String[] args) {
        
        TestDao td = new TestDaoImpl();
        TestService ts = new TestSeviceImpl(td);
        
        try{
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            
            LoginFrame loginFrame = new LoginFrame(ts);
            loginFrame.setVisible(true);
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
        
        
        
        
        
    }
    
}
