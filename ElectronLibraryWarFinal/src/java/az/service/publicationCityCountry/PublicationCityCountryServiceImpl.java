/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.publicationCityCountry;

import az.dao.publicationCityCountry.PublicationCityCountryDao;
import az.model.PublicationCityCountry;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class PublicationCityCountryServiceImpl implements PublicationCityCountryService{

    private PublicationCityCountryDao publicationCityCountryDao;

    public PublicationCityCountryServiceImpl(PublicationCityCountryDao publicationCityCountryDao) {
        this.publicationCityCountryDao = publicationCityCountryDao;
    }
    
    
    @Override
    public List<PublicationCityCountry> getPublicationCityCountryList(long active) throws Exception {
        return publicationCityCountryDao.getPublicationCityCountryList(active);
    }

    @Override
    public boolean addPublicationCityCountry(PublicationCityCountry publicationCityCountry) throws Exception {
        return publicationCityCountryDao.addPublicationCityCountry(publicationCityCountry);
    }

    @Override
    public PublicationCityCountry getPublicationCityCountryByID(long id) throws Exception {
        return publicationCityCountryDao.getPublicationCityCountryByID(id);
    }

    @Override
    public boolean changeActivePublicationCityCountry(long id, long active) throws Exception {
        return publicationCityCountryDao.changeActivePublicationCityCountry(id, active);
    }

    @Override
    public boolean updatePublicationCityCountry(PublicationCityCountry publicationCityCountry, long id) throws Exception {
        return publicationCityCountryDao.updatePublicationCityCountry(publicationCityCountry, id);
    }

    @Override
    public List<PublicationCityCountry> searchPublicationCityCountry(String keyWord, long active) throws Exception {
        return publicationCityCountryDao.searchPublicationCityCountry(keyWord,active);
    }
    
}
