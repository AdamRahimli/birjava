/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.publicationCityCountry;

import az.model.PublicationCityCountry;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface PublicationCityCountryService {
    
    public List<PublicationCityCountry> getPublicationCityCountryList(long active) throws Exception;
    public boolean addPublicationCityCountry(PublicationCityCountry publicationCityCountry) throws Exception;
    public PublicationCityCountry getPublicationCityCountryByID(long id) throws Exception;
    public boolean changeActivePublicationCityCountry(long id,long active)throws Exception;
    public boolean updatePublicationCityCountry(PublicationCityCountry publicationCityCountry,long id)  throws Exception;
    public List<PublicationCityCountry> searchPublicationCityCountry(String keyWord, long active) throws Exception;
    
}
