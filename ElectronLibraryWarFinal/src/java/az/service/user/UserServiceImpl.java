/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.user;

import az.dao.user.UserDao;
import az.model.User;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public class UserServiceImpl implements UserService{

    UserDao userDao;
    
    public UserServiceImpl(UserDao ud) {
        userDao = ud;
    }

    
    
    @Override
    public List<User> getUserList(long active) throws Exception {
        return userDao.getUserList(active);
    }

    @Override
    public boolean addUser(User user) throws Exception {
        return userDao.addUser(user);
    }

    @Override
    public User getUserByID(long id) throws Exception {
        return userDao.getUserByID(id);
    }

    @Override
    public boolean changeActiveUser(long id, long active) throws Exception {
        return userDao.changeActiveUser(id, active);
    }

    @Override
    public boolean updateUser(User user, long id) throws Exception {
        return userDao.updateUser(user, id);
    }

    @Override
    public List<User> searchUser(String keyWord, long active) throws Exception {
        return userDao.searchUser(keyWord, active);
    }

    @Override
    public User checkUser(String username, String password) throws Exception {
        return userDao.checkUser(username, password);
    }
    
}
