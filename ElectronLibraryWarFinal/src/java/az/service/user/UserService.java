/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.user;

import az.model.User;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public interface UserService {
    
    public List<User> getUserList(long active) throws Exception;
    public boolean addUser(User user) throws Exception;
    public User getUserByID(long id) throws Exception;
    public boolean changeActiveUser(long id,long active) throws Exception;
    public boolean updateUser(User user,long id) throws Exception;
    public List<User> searchUser(String keyWord,long active) throws Exception;
    public User checkUser(String username, String password) throws Exception;
    
}
