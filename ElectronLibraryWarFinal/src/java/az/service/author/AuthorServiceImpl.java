/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.author;

import az.dao.author.AuthorDao;
import az.model.Author;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class AuthorServiceImpl implements AuthorService{

    private AuthorDao authorDao;

    public AuthorServiceImpl(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }
      
    
    @Override
    public List<Author> getAuthorList(long active) throws Exception {
        return authorDao.getAuthorList(active);
    }

    @Override
    public boolean addAuthor(Author author) throws Exception {
        return authorDao.addAuthor(author);
    }

    @Override
    public Author getAuthorByID(long id) throws Exception {
        return authorDao.getAuthorByID(id);
    }

    @Override
    public boolean changeActiveAuthor(long id, long active) throws Exception {
        return authorDao.changeActiveAuthor(id, active);
    }

    @Override
    public boolean updateAuthor(Author author, long id) throws Exception {
        return authorDao.updateAuthor(author, id);
    }

    @Override
    public List<Author> searchAuthor(String keyWord, long active) throws Exception {
        return authorDao.searchAuthor(keyWord,active);
    }
    
}
