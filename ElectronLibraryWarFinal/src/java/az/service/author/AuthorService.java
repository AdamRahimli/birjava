/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.author;

import az.model.Author;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface AuthorService {
    
    public List<Author> getAuthorList(long active) throws Exception;
    public boolean addAuthor(Author author) throws Exception;
    public Author getAuthorByID(long id) throws Exception;
    public boolean changeActiveAuthor(long id, long active) throws Exception;
    public boolean updateAuthor(Author author, long id) throws Exception;
    public List<Author> searchAuthor(String keyWord, long active) throws Exception;
    
}
