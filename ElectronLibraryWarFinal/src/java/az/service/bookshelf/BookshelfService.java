/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.bookshelf;

import az.model.AdvancedSearchBookshelf;
import az.model.Bookshelf;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public interface BookshelfService {
    
    public List<Bookshelf> getBookshelfList() throws Exception;
    public boolean addBookshelf(Bookshelf bookshelf) throws Exception;
    public Bookshelf getBookshelfByID(long id) throws Exception;
    public boolean changeActiveBookshelf(long id,long active) throws Exception;
    public boolean updateBookshelf(Bookshelf bookshelf,long id) throws Exception;
    public List<Bookshelf> searchBookshelf(String keyWord) throws Exception;
    public List<Bookshelf> advanceSearchBookshelf(AdvancedSearchBookshelf advancedSearchBookshelf) throws Exception;
    
}
