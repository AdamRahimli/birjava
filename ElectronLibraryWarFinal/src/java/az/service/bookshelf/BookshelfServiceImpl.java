/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.bookshelf;


import az.dao.bookshelf.BookshelfDao;
import az.model.AdvancedSearchBookshelf;
import az.model.Bookshelf;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public class BookshelfServiceImpl implements BookshelfService {

    private BookshelfDao bookshelfDao;

    public BookshelfServiceImpl(BookshelfDao bookshelfDao) {
        this.bookshelfDao = bookshelfDao;
    }
    
    
    @Override
    public List<Bookshelf> getBookshelfList() throws Exception {
        return bookshelfDao.getBookshelfList();
    }

    @Override
    public boolean addBookshelf(Bookshelf bookshelf) throws Exception {
        return bookshelfDao.addBookshelf(bookshelf);
    }

    @Override
    public Bookshelf getBookshelfByID(long id) throws Exception {
        return bookshelfDao.getBookshelfByID(id);
    }

    @Override
    public boolean changeActiveBookshelf(long id, long active) throws Exception {
        return bookshelfDao.changeActiveBookshelf(id, active);
    }

    @Override
    public boolean updateBookshelf(Bookshelf bookshelf, long id) throws Exception {
        return bookshelfDao.updateBookshelf(bookshelf, id);
    }

    @Override
    public List<Bookshelf> searchBookshelf(String keyWord) throws Exception {
        return bookshelfDao.searchBookshelf(keyWord);
    }

    @Override
    public List<Bookshelf> advanceSearchBookshelf(AdvancedSearchBookshelf advancedSearchBookshelf) throws Exception {
        return bookshelfDao.advanceSearchBookshelf(advancedSearchBookshelf);
    }

}
