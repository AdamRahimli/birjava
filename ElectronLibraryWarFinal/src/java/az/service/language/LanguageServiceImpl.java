/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.language;

import az.dao.language.LanguageDao;
import az.model.Language;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class LanguageServiceImpl implements LanguageService{

    private LanguageDao languageDao;

    public LanguageServiceImpl(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }
    
    
    @Override
    public List<Language> getLanguageList(long active) throws Exception {
        return languageDao.getLanguageList(active);
    }

    @Override
    public boolean addLanguage(Language language) throws Exception {
        return languageDao.addLanguage(language);
    }

    @Override
    public Language getLanguageByID(long id) throws Exception {
        return languageDao.getLanguageByID(id);
    }

    @Override
    public boolean changeActiveLanguage(long id, long active) throws Exception {
        return languageDao.changeActiveLanguage(id, active);
    }

    @Override
    public boolean updateLanguage(Language language, long id) throws Exception {
        return languageDao.updateLanguage(language, id);
    }

    @Override
    public List<Language> searchLanguage(String keyWord, long active) throws Exception {
        return languageDao.searchLanguage(keyWord,active);
    }
    
    
    
}
