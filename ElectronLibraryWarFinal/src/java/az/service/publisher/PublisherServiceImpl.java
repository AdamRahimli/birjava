/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.publisher;

import az.dao.publisher.PublisherDao;
import az.model.Publisher;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class PublisherServiceImpl implements PublisherService{

    private PublisherDao publisherDao;

    public PublisherServiceImpl(PublisherDao publisherDao) {
        this.publisherDao = publisherDao;
    }
    
    
    
    @Override
    public List<Publisher> getPublisherList(long active) throws Exception {
        return publisherDao.getPublisherList(active);
    }

    @Override
    public boolean addPublisher(Publisher publisher) throws Exception {
        return publisherDao.addPublisher(publisher);
    }

    @Override
    public Publisher getPublisherByID(long id) throws Exception {
        return publisherDao.getPublisherByID(id);
    }

    @Override
    public boolean changeActivePublisher(long id, long active) throws Exception {
        return publisherDao.changeActivePublisher(id, active);
    }

    @Override
    public boolean updatePublisher(Publisher publisher, long id) throws Exception {
        return publisherDao.updatePublisher(publisher, id);
    }

    @Override
    public List<Publisher> searchPublisher(String keyWord, long active) throws Exception {
        return publisherDao.searchPublisher(keyWord,active);
    }
    
}
