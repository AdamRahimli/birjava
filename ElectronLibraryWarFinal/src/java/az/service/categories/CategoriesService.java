/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.categories;

import az.model.Categories;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface CategoriesService {
    
    public List<Categories> getCategoriesList(long active) throws Exception;
    public boolean addCategories(Categories categories) throws Exception;
    public Categories getCategoriesByID(long id) throws Exception;
    public boolean changeActiveCategories(long id,long active) throws Exception;
    public boolean updateCategories(Categories categories,long id) throws Exception;
    public List<Categories> searchCategories(String keyWord, long active) throws Exception;
    
}
