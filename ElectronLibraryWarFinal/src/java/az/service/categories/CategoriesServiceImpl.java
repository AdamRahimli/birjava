/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.service.categories;

import az.dao.categories.CategoriesDao;
import az.model.Categories;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class CategoriesServiceImpl implements CategoriesService{

    private CategoriesDao categoriesDao;

    public CategoriesServiceImpl(CategoriesDao categoriesDao) {
        this.categoriesDao = categoriesDao;
    }
    
    
    @Override
    public List<Categories> getCategoriesList(long active) throws Exception {
        return categoriesDao.getCategoriesList(active);
    }

    @Override
    public boolean addCategories(Categories categories) throws Exception {
        return categoriesDao.addCategories(categories);
    }

    @Override
    public Categories getCategoriesByID(long id) throws Exception {
        return categoriesDao.getCategoriesByID(id);
    }

    @Override
    public boolean changeActiveCategories(long id, long active) throws Exception {
        return categoriesDao.changeActiveCategories(id, active);
    }

    @Override
    public boolean updateCategories(Categories categories, long id) throws Exception {
        return categoriesDao.updateCategories(categories, id);
    }

    @Override
    public List<Categories> searchCategories(String keyWord, long active) throws Exception {
        return categoriesDao.searchCategories(keyWord,active);
    }
    
}
