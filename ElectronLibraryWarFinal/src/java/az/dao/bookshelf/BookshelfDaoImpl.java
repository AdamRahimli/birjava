/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.bookshelf;

import az.dao.DBHelper;
import az.model.AdvancedSearchBookshelf;
import az.model.Author;
import az.model.Bookshelf;
import az.model.Categories;
import az.model.Language;
import az.model.PublicationCityCountry;
import az.model.Publisher;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public class BookshelfDaoImpl implements BookshelfDao {

    @Override
    public List<Bookshelf> getBookshelfList() throws Exception {
        List<Bookshelf> bookshelfList = new ArrayList<Bookshelf>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_BOOKSHELF_LIST + " B.ACTIVE = 1  AND P.ACTIVE = 1 AND L.ACTIVE = 1 AND C.ACTIVE = 1 AND S.ACTIVE = 1 AND A.ACTIVE = 1 "; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Bookshelf bookshelf = new Bookshelf();

                    Author author = new Author();
                    Categories categories = new Categories();
                    Language language = new Language();
                    PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                    Publisher publisher = new Publisher();

                    bookshelf.setR(rs.getLong("r"));
                    bookshelf.setId(rs.getLong("B_ID"));
                    bookshelf.setName(rs.getString("B_NAME"));
                    bookshelf.setHardcover(rs.getString("B_HARDCOVER"));
                    bookshelf.setPublisher_id(rs.getLong("P_ID"));
                    bookshelf.setLanguage_id(rs.getLong("L_ID"));
                    bookshelf.setCategories_id(rs.getLong("C_ID"));
                    bookshelf.setDimensions(rs.getString("B_DIMENSIONS"));
                    bookshelf.setPublication_date(rs.getDate("B_PUBLICATION_DATE"));
                    bookshelf.setPublication_city_country_id(rs.getLong("S_ID"));
                    bookshelf.setAuthor_id(rs.getLong("A_ID"));
                    bookshelf.setIsbn_10(rs.getString("B_ISBN_10"));
                    bookshelf.setIsbn_13(rs.getString("B_ISBN_13"));
                    bookshelf.setActive(rs.getLong("B_ACTIVE"));
                    bookshelf.setData_date(rs.getDate("B_DATA_DATE"));
                    bookshelf.setPhoto_path(rs.getString("B_PHOTO_PATH"));
                    bookshelf.setPdf_path(rs.getString("B_PDF_PATH"));

                    publisher.setName(rs.getString("P_NAME"));
                    bookshelf.setPublisher(publisher);

                    language.setName(rs.getString("L_NAME"));
                    bookshelf.setLanguage(language);

                    categories.setName(rs.getString("C_NAME"));
                    bookshelf.setCategories(categories);

                    publicationCityCountry.setName(rs.getString("S_NAME"));
                    bookshelf.setPublicationCityCountry(publicationCityCountry);

                    author.setName(rs.getString("A_NAME"));
                    author.setSurname(rs.getString("A_SURNAME"));
                    bookshelf.setAuthor(author);

                    bookshelfList.add(bookshelf);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return bookshelfList;
    }

    @Override
    public boolean addBookshelf(Bookshelf bookshelf) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_BOOKSHELF; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, bookshelf.getName());
                ps.setString(2, bookshelf.getHardcover());
                ps.setLong(3, bookshelf.getPublisher_id());
                ps.setLong(4, bookshelf.getLanguage_id());
                ps.setLong(5, bookshelf.getCategories_id());
                ps.setString(6, bookshelf.getDimensions());
                ps.setDate(7, new java.sql.Date(bookshelf.getPublication_date().getTime()));
                ps.setLong(8, bookshelf.getPublication_city_country_id());
                ps.setLong(9, bookshelf.getAuthor_id());
                ps.setString(10, bookshelf.getIsbn_10());
                ps.setString(11, bookshelf.getIsbn_13());
                ps.setString(12, bookshelf.getPhoto_path());
                ps.setString(13, bookshelf.getPdf_path());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public Bookshelf getBookshelfByID(long id) throws Exception {
        Bookshelf bookshelf = new Bookshelf();

        Author author = new Author();
        Categories categories = new Categories();
        Language language = new Language();
        PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
        Publisher publisher = new Publisher();

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_BOOKSHELF_LIST + " B.ID = ?"; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    bookshelf.setR(rs.getLong("r"));
                    bookshelf.setId(rs.getLong("B_ID"));
                    bookshelf.setName(rs.getString("B_NAME"));
                    bookshelf.setHardcover(rs.getString("B_HARDCOVER"));
                    bookshelf.setPublisher_id(rs.getLong("P_ID"));
                    bookshelf.setLanguage_id(rs.getLong("L_ID"));
                    bookshelf.setCategories_id(rs.getLong("C_ID"));
                    bookshelf.setDimensions(rs.getString("B_DIMENSIONS"));
                    bookshelf.setPublication_date(rs.getDate("B_PUBLICATION_DATE"));
                    bookshelf.setPublication_city_country_id(rs.getLong("S_ID"));
                    bookshelf.setAuthor_id(rs.getLong("A_ID"));
                    bookshelf.setIsbn_10(rs.getString("B_ISBN_10"));
                    bookshelf.setIsbn_13(rs.getString("B_ISBN_13"));
                    bookshelf.setActive(rs.getLong("B_ACTIVE"));
                    bookshelf.setData_date(rs.getDate("B_DATA_DATE"));
                    bookshelf.setPhoto_path(rs.getString("B_PHOTO_PATH"));
                    bookshelf.setPdf_path(rs.getString("B_PDF_PATH"));

                    publisher.setName(rs.getString("P_NAME"));
                    bookshelf.setPublisher(publisher);

                    language.setName(rs.getString("L_NAME"));
                    bookshelf.setLanguage(language);

                    categories.setName(rs.getString("C_NAME"));
                    bookshelf.setCategories(categories);

                    publicationCityCountry.setName(rs.getString("S_NAME"));
                    bookshelf.setPublicationCityCountry(publicationCityCountry);

                    author.setName(rs.getString("A_NAME"));
                    author.setSurname(rs.getString("A_SURNAME"));
                    bookshelf.setAuthor(author);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            bookshelf = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return bookshelf;

    }

    @Override
    public boolean changeActiveBookshelf(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_BOOKSHELF; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateBookshelf(Bookshelf bookshelf, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_BOOKSHELF; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, bookshelf.getName());
                ps.setString(2, bookshelf.getHardcover());
                ps.setLong(3, bookshelf.getPublisher_id());
                ps.setLong(4, bookshelf.getLanguage_id());
                ps.setLong(5, bookshelf.getCategories_id());
                ps.setString(6, bookshelf.getDimensions());
                ps.setDate(7, new java.sql.Date(bookshelf.getPublication_date().getTime()));
                ps.setLong(8, bookshelf.getPublication_city_country_id());
                ps.setLong(9, bookshelf.getAuthor_id());
                ps.setString(10, bookshelf.getIsbn_10());
                ps.setString(11, bookshelf.getIsbn_13());
                ps.setString(12, bookshelf.getPhoto_path());
                ps.setString(13, bookshelf.getPdf_path());
                ps.setLong(14, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Bookshelf> searchBookshelf(String keyWord) throws Exception {
        List<Bookshelf> bookshelfList = new ArrayList<Bookshelf>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_BOOKSHELF_LIST + " B.ACTIVE = 1  AND P.ACTIVE = 1 AND L.ACTIVE = 1 AND C.ACTIVE = 1 AND S.ACTIVE = 1 AND A.ACTIVE = 1 AND "
                + " ( LOWER(B.NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(B.HARDCOVER) LIKE LOWER('%" + keyWord + "%') OR LOWER(P.NAME) LIKE LOWER('%" + keyWord + "%') OR "
                + " LOWER(L.NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(C.NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(B.DIMENSIONS) LIKE LOWER('%" + keyWord + "%') OR "
                + " B.PUBLICATION_DATE LIKE '%" + keyWord + "%' OR LOWER(S.NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(A.NAME) LIKE LOWER('%" + keyWord + "%') OR "
                + " LOWER (A.SURNAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(B.ISBN_10) LIKE LOWER('%" + keyWord + "%') OR LOWER(B.ISBN_10) LIKE LOWER('%" + keyWord + "%') ) "; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Bookshelf bookshelf = new Bookshelf();

                    Author author = new Author();
                    Categories categories = new Categories();
                    Language language = new Language();
                    PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                    Publisher publisher = new Publisher();

                    bookshelf.setR(rs.getLong("r"));
                    bookshelf.setId(rs.getLong("B_ID"));
                    bookshelf.setName(rs.getString("B_NAME"));
                    bookshelf.setHardcover(rs.getString("B_HARDCOVER"));
                    bookshelf.setPublisher_id(rs.getLong("P_ID"));
                    bookshelf.setLanguage_id(rs.getLong("L_ID"));
                    bookshelf.setCategories_id(rs.getLong("C_ID"));
                    bookshelf.setDimensions(rs.getString("B_DIMENSIONS"));
                    bookshelf.setPublication_date(rs.getDate("B_PUBLICATION_DATE"));
                    bookshelf.setPublication_city_country_id(rs.getLong("S_ID"));
                    bookshelf.setAuthor_id(rs.getLong("A_ID"));
                    bookshelf.setIsbn_10(rs.getString("B_ISBN_10"));
                    bookshelf.setIsbn_13(rs.getString("B_ISBN_13"));
                    bookshelf.setActive(rs.getLong("B_ACTIVE"));
                    bookshelf.setData_date(rs.getDate("B_DATA_DATE"));
                    bookshelf.setPhoto_path(rs.getString("B_PHOTO_PATH"));
                    bookshelf.setPdf_path(rs.getString("B_PDF_PATH"));

                    publisher.setName(rs.getString("P_NAME"));
                    bookshelf.setPublisher(publisher);

                    language.setName(rs.getString("L_NAME"));
                    bookshelf.setLanguage(language);

                    categories.setName(rs.getString("C_NAME"));
                    bookshelf.setCategories(categories);

                    publicationCityCountry.setName(rs.getString("S_NAME"));
                    bookshelf.setPublicationCityCountry(publicationCityCountry);

                    author.setName(rs.getString("A_NAME"));
                    author.setSurname(rs.getString("A_SURNAME"));
                    bookshelf.setAuthor(author);

                    bookshelfList.add(bookshelf);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return bookshelfList;
    }

    @Override
    public List<Bookshelf> advanceSearchBookshelf(AdvancedSearchBookshelf advancedSearchBookshelf) throws Exception {
        List<Bookshelf> bookshelfList = new ArrayList<Bookshelf>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_BOOKSHELF_LIST + " B.ACTIVE = 1  AND P.ACTIVE = 1 AND L.ACTIVE = 1 AND C.ACTIVE = 1 AND S.ACTIVE = 1 AND A.ACTIVE = 1 ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                if (advancedSearchBookshelf.getAuthorId() != 0) {
                    sql += " AND A.ID = " + advancedSearchBookshelf.getAuthorId();
                }
                if (advancedSearchBookshelf.getCategoriesId() != 0) {
                    sql += " AND C.ID = " + advancedSearchBookshelf.getCategoriesId();
                }
                if (advancedSearchBookshelf.getLanguageId() != 0) {
                    sql += " AND L.ID = " + advancedSearchBookshelf.getLanguageId();
                }
                if (advancedSearchBookshelf.getPublisherId() != 0) {
                    sql += " AND P.ID = " + advancedSearchBookshelf.getPublisherId();
                }
                if (advancedSearchBookshelf.getPublication_city_country_id() != 0) {
                    sql += " AND S.ID = " + advancedSearchBookshelf.getPublication_city_country_id();
                }
                if (advancedSearchBookshelf.getBeginDate() != null && !advancedSearchBookshelf.getBeginDate().isEmpty()){
                    sql += " AND B.PUBLICATION_DATE >= TO_DATE('"+ new java.sql.Date(df.parse(advancedSearchBookshelf.getBeginDate()).getTime())+"','YYYY-MM-DD')";
                }
                if (advancedSearchBookshelf.getEndDate() != null && !advancedSearchBookshelf.getEndDate().isEmpty()){
                    sql += " AND B.PUBLICATION_DATE < TO_DATE('"+new java.sql.Date(df.parse(advancedSearchBookshelf.getEndDate()).getTime())+"','YYYY-MM-DD')";
                }
                ps = c.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Bookshelf bookshelf = new Bookshelf();

                    Author author = new Author();
                    Categories categories = new Categories();
                    Language language = new Language();
                    PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                    Publisher publisher = new Publisher();

                    bookshelf.setR(rs.getLong("r"));
                    bookshelf.setId(rs.getLong("B_ID"));
                    bookshelf.setName(rs.getString("B_NAME"));
                    bookshelf.setHardcover(rs.getString("B_HARDCOVER"));
                    bookshelf.setPublisher_id(rs.getLong("P_ID"));
                    bookshelf.setLanguage_id(rs.getLong("L_ID"));
                    bookshelf.setCategories_id(rs.getLong("C_ID"));
                    bookshelf.setDimensions(rs.getString("B_DIMENSIONS"));
                    bookshelf.setPublication_date(rs.getDate("B_PUBLICATION_DATE"));
                    bookshelf.setPublication_city_country_id(rs.getLong("S_ID"));
                    bookshelf.setAuthor_id(rs.getLong("A_ID"));
                    bookshelf.setIsbn_10(rs.getString("B_ISBN_10"));
                    bookshelf.setIsbn_13(rs.getString("B_ISBN_13"));
                    bookshelf.setActive(rs.getLong("B_ACTIVE"));
                    bookshelf.setData_date(rs.getDate("B_DATA_DATE"));
                    bookshelf.setPhoto_path(rs.getString("B_PHOTO_PATH"));
                    bookshelf.setPdf_path(rs.getString("B_PDF_PATH"));

                    publisher.setName(rs.getString("P_NAME"));
                    bookshelf.setPublisher(publisher);

                    language.setName(rs.getString("L_NAME"));
                    bookshelf.setLanguage(language);

                    categories.setName(rs.getString("C_NAME"));
                    bookshelf.setCategories(categories);

                    publicationCityCountry.setName(rs.getString("S_NAME"));
                    bookshelf.setPublicationCityCountry(publicationCityCountry);

                    author.setName(rs.getString("A_NAME"));
                    author.setSurname(rs.getString("A_SURNAME"));
                    bookshelf.setAuthor(author);

                    bookshelfList.add(bookshelf);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return bookshelfList;
    }

}
