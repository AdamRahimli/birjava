/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.publicationCityCountry;

import az.dao.DBHelper;
import az.model.PublicationCityCountry;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class PublicationCityCountryDaoImpl implements PublicationCityCountryDao{

    @Override
    public List<PublicationCityCountry> getPublicationCityCountryList(long active) throws Exception {
        List<PublicationCityCountry> publicationCityCountryList = new ArrayList<PublicationCityCountry>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLICATION_CITY_COUNTRY_LIST + " ACTIVE = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                    publicationCityCountry.setR(rs.getLong("r"));
                    publicationCityCountry.setId(rs.getLong("ID"));
                    publicationCityCountry.setName(rs.getString("NAME"));
                    publicationCityCountry.setActive(rs.getLong("ACTIVE"));
                    publicationCityCountry.setData_date(rs.getDate("DATA_DATE"));
                    publicationCityCountryList.add(publicationCityCountry);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publicationCityCountryList;
    }

    @Override
    public boolean addPublicationCityCountry(PublicationCityCountry publicationCityCountry) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_PUBLICATION_CITY_COUNTRY;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, publicationCityCountry.getName());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public PublicationCityCountry getPublicationCityCountryByID(long id) throws Exception {
        PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLICATION_CITY_COUNTRY_LIST + " ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    publicationCityCountry.setR(rs.getLong("r"));
                    publicationCityCountry.setId(rs.getLong("ID"));
                    publicationCityCountry.setName(rs.getString("NAME"));
                    publicationCityCountry.setActive(rs.getLong("ACTIVE"));
                    publicationCityCountry.setData_date(rs.getDate("DATA_DATE"));
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            publicationCityCountry = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publicationCityCountry;
    }

    @Override
    public boolean changeActivePublicationCityCountry(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_PUBLICATION_CITY_COUNTRY;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updatePublicationCityCountry(PublicationCityCountry publicationCityCountry, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_PUBLICATION_CITY_COUNTRY;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, publicationCityCountry.getName());
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<PublicationCityCountry> searchPublicationCityCountry(String keyWord, long active) throws Exception {
        List<PublicationCityCountry> publicationCityCountryList = new ArrayList<PublicationCityCountry>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLICATION_CITY_COUNTRY_LIST + " ACTIVE = ? AND (ID LIKE '%"+keyWord+"%' OR  LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR DATA_DATE LIKE '%" + keyWord + "%')";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                    publicationCityCountry.setR(rs.getLong("r"));
                    publicationCityCountry.setId(rs.getLong("ID"));
                    publicationCityCountry.setName(rs.getString("NAME"));
                    publicationCityCountry.setActive(rs.getLong("ACTIVE"));
                    publicationCityCountry.setData_date(rs.getDate("DATA_DATE"));
                    publicationCityCountryList.add(publicationCityCountry);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publicationCityCountryList;
    }
    
}
