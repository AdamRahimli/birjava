/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.categories;

import az.dao.DBHelper;
import az.model.Categories;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class CategoriesDaoImpl implements CategoriesDao {

    @Override
    public List<Categories> getCategoriesList(long active) throws Exception {
        List<Categories> categorieseList = new ArrayList<Categories>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_CATEGORIES_LIST + " ACTIVE = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Categories categories = new Categories();
                    categories.setR(rs.getLong("r"));
                    categories.setId(rs.getLong("ID"));
                    categories.setName(rs.getString("NAME"));
                    categories.setActive(rs.getLong("ACTIVE"));
                    categories.setData_date(rs.getDate("DATA_DATE"));
                    categorieseList.add(categories);
                }
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return categorieseList;
    }

    @Override
    public boolean addCategories(Categories categories) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_CATEGORIES;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, categories.getName());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public Categories getCategoriesByID(long id) throws Exception {
        Categories categories = new Categories();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_CATEGORIES_LIST + " ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    categories.setR(rs.getLong("r"));
                    categories.setId(rs.getLong("ID"));
                    categories.setName(rs.getString("NAME"));
                    categories.setActive(rs.getLong("ACTIVE"));
                    categories.setData_date(rs.getDate("DATA_DATE"));
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            categories = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return categories;
    }

    @Override
    public boolean changeActiveCategories(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_CATEGORIES;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateCategories(Categories categories, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_CATEGORIES;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, categories.getName());
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Categories> searchCategories(String keyWord, long active) throws Exception {
        List<Categories> categorieseList = new ArrayList<Categories>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_CATEGORIES_LIST + " ACTIVE = ? AND (ID LIKE '%" + keyWord + "%' OR  LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR DATA_DATE LIKE '%" + keyWord + "%')";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Categories categories = new Categories();
                    categories.setR(rs.getLong("r"));
                    categories.setId(rs.getLong("ID"));
                    categories.setName(rs.getString("NAME"));
                    categories.setActive(rs.getLong("ACTIVE"));
                    categories.setData_date(rs.getDate("DATA_DATE"));
                    categorieseList.add(categories);
                }
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return categorieseList;
    }

}
