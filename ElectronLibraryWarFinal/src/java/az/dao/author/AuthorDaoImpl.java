/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.author;

import az.dao.DBHelper;
import az.model.Author;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class AuthorDaoImpl implements AuthorDao {

    @Override
    public List<Author> getAuthorList(long active) throws Exception {
        List<Author> authorList = new ArrayList<Author>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_AUTHOR_LIST + " ACTIVE = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Author author = new Author();
                    author.setR(rs.getLong("r"));
                    author.setId(rs.getLong("ID"));
                    author.setName(rs.getString("NAME"));
                    author.setSurname(rs.getString("SURNAME"));
                    author.setDob(rs.getDate("DOB"));
                    author.setActive(rs.getLong("ACTIVE"));
                    author.setData_date(rs.getDate("DATA_DATE"));
                    authorList.add(author);
                }
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return authorList;
    }

    @Override
    public boolean addAuthor(Author author) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_AUTHOR;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, author.getName());
                ps.setString(2, author.getSurname());
                ps.setDate(3, new java.sql.Date(author.getDob().getTime()));
                ps.execute();
                result = true;
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public Author getAuthorByID(long id) throws Exception {
        Author author = new Author();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_AUTHOR_LIST + " ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    author.setR(rs.getLong("r"));
                    author.setId(rs.getLong("ID"));
                    author.setName(rs.getString("NAME"));
                    author.setSurname(rs.getString("SURNAME"));
                    author.setDob(rs.getDate("DOB"));
                    author.setActive(rs.getLong("ACTIVE"));
                    author.setData_date(rs.getDate("DATA_DATE"));
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            author = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return author;
    }

    @Override
    public boolean changeActiveAuthor(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_AUTHOR;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateAuthor(Author author, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_AUTHOR;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, author.getName());
                ps.setString(2, author.getSurname());
                ps.setDate(3, new java.sql.Date(author.getDob().getTime()));
                ps.setLong(4, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Author> searchAuthor(String keyWord, long active) throws Exception {
        List<Author> authorList = new ArrayList<Author>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_AUTHOR_LIST + " ACTIVE = ? AND ( ID LIKE '%" + keyWord + "%' OR LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(SURNAME) LIKE LOWER('%" + keyWord + "%') OR "
                + " DOB LIKE '%" + keyWord + "%' OR DATA_DATE LIKE '%" + keyWord + "%' ) ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Author author = new Author();
                    author.setR(rs.getLong("r"));
                    author.setId(rs.getLong("ID"));
                    author.setName(rs.getString("NAME"));
                    author.setSurname(rs.getString("SURNAME"));
                    author.setDob(rs.getDate("DOB"));
                    author.setActive(rs.getLong("ACTIVE"));
                    author.setData_date(rs.getDate("DATA_DATE"));
                    authorList.add(author);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return authorList;
    }

}
