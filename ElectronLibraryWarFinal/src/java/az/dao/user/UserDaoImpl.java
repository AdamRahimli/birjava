/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.user;

import az.dao.DBHelper;
import az.model.User;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BirAdam
 */
public class UserDaoImpl implements UserDao {

    @Override
    public List<User> getUserList(long active) throws Exception {
        List<User> userList = new ArrayList<User>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_USER_LIST + " ACTIVE = ? "; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    User user = new User();
                    user.setR(rs.getLong(1));
                    user.setId(rs.getLong(2));
                    user.setName(rs.getString(3));
                    user.setSurname(rs.getString(4));
                    user.setGender(rs.getString(5));
                    user.setDob(rs.getDate(6));
                    user.setEmail(rs.getString(7));
                    user.setPhone_number(rs.getString(8));
                    user.setUsername(rs.getString(9));
                    user.setPassword(rs.getString(10));
                    user.setData_date(rs.getDate(11));
                    user.setActive(rs.getLong(12));
                    userList.add(user);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return userList;
    }

    @Override
    public boolean addUser(User user) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_USER; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, user.getName());
                ps.setString(2, user.getSurname());
                ps.setString(3, user.getGender());
                ps.setDate(4, new java.sql.Date(user.getDob().getTime()));
                ps.setString(5, user.getEmail());
                ps.setString(6, user.getPhone_number());
                ps.setString(7, user.getUsername());
                ps.setString(8, user.getPassword());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public User getUserByID(long id) throws Exception {
        User user = new User();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_USER_LIST + " ID = ? "; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    user.setR(rs.getLong(1));
                    user.setId(rs.getLong(2));
                    user.setName(rs.getString(3));
                    user.setSurname(rs.getString(4));
                    user.setGender(rs.getString(5));
                    user.setDob(rs.getDate(6));
                    user.setEmail(rs.getString(7));
                    user.setPhone_number(rs.getString(8));
                    user.setUsername(rs.getString(9));
                    user.setPassword(rs.getString(10));
                    user.setData_date(rs.getDate(11));
                    user.setActive(rs.getLong(12));
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            user = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return user;
    }

    @Override
    public boolean changeActiveUser(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_USER; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateUser(User user, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_USER; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, user.getName());
                ps.setString(2, user.getSurname());
                ps.setString(3, user.getGender());
                ps.setDate(4, new java.sql.Date(user.getDob().getTime()));
                ps.setString(5, user.getEmail());
                ps.setString(6, user.getPhone_number());
                ps.setString(7, user.getUsername());
                ps.setString(8, user.getPassword());
                ps.setLong(9, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<User> searchUser(String keyWord, long active) throws Exception {
        List<User> userList = new ArrayList<User>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_USER_LIST + " ACTIVE = ? AND ( LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR LOWER(SURNAME) LIKE LOWER('%" + keyWord + "%') OR "
                + " LOWER(GENDER) LIKE LOWER('%" + keyWord + "%') OR DOB LIKE '%" + keyWord + "%' OR LOWER(EMAIL) LIKE LOWER('%" + keyWord + "%') OR "
                + " LOWER(PHONE_NUMBER) LIKE LOWER('%" + keyWord + "%') OR USERNAME LIKE '%" + keyWord + "%' OR PASSWORD LIKE '%" + keyWord + "%' )"; //bura
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    User user = new User();
                    user.setR(rs.getLong(1));
                    user.setId(rs.getLong(2));
                    user.setName(rs.getString(3));
                    user.setSurname(rs.getString(4));
                    user.setGender(rs.getString(5));
                    user.setDob(rs.getDate(6));
                    user.setEmail(rs.getString(7));
                    user.setPhone_number(rs.getString(8));
                    user.setUsername(rs.getString(9));
                    user.setPassword(rs.getString(10));
                    user.setData_date(rs.getDate(11));
                    user.setActive(rs.getLong(12));
                    userList.add(user);
                }
            } else {
                System.out.println("Connection is NULL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return userList;
    }

    @Override
    public User checkUser(String username, String password) throws Exception {
        User user = new User();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_USER_LIST + " ACTIVE = 1 AND USERNAME = ? AND PASSWORD = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, username);
                ps.setString(2, password);
                rs = ps.executeQuery();
                if (rs.next()) {
                    user.setR(rs.getLong(1));
                    user.setId(rs.getLong(2));
                    user.setName(rs.getString(3));
                    user.setSurname(rs.getString(4));
                    user.setGender(rs.getString(5));
                    user.setDob(rs.getDate(6));
                    user.setEmail(rs.getString(7));
                    user.setPhone_number(rs.getString(8));
                    user.setUsername(rs.getString(9));
                    user.setPassword(rs.getString(10));
                    user.setData_date(rs.getDate(11));
                    user.setActive(rs.getLong(12));
                } else {
                    user = null;
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return user;
    }

}
