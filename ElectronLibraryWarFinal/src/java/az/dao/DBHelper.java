/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao;

import java.sql.Connection;
import java.util.Locale;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author BirAdam
 */
public class DBHelper {

    public static Connection getConnection() throws Exception {
        Locale.setDefault(Locale.ENGLISH);
        Context context = new InitialContext();
        DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/oracle");
        Connection c = dataSource.getConnection();      
        return c;
    }
}
