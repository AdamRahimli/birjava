/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.language;

import az.model.Language;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface LanguageDao {
    
    public List<Language> getLanguageList(long active) throws Exception;
    public boolean addLanguage(Language language) throws Exception;
    public Language getLanguageByID(long id) throws Exception;
    public boolean changeActiveLanguage(long id,long active)throws Exception;
    public boolean updateLanguage(Language language,long id)  throws Exception;
    public List<Language> searchLanguage(String keyWord, long active) throws Exception;
    
}
