/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.language;

import az.dao.DBHelper;
import az.model.Language;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class LanguageDaoImpl implements LanguageDao {

    @Override
    public List<Language> getLanguageList(long active) throws Exception {
        List<Language> languageList = new ArrayList<Language>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_LANGUAGE_LIST + " ACTIVE = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Language language = new Language();
                    language.setR(rs.getLong("r"));
                    language.setId(rs.getLong("ID"));
                    language.setName(rs.getString("NAME"));
                    language.setActive(rs.getLong("ACTIVE"));
                    language.setData_date(rs.getDate("DATA_DATE"));
                    languageList.add(language);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return languageList;
    }

    @Override
    public boolean addLanguage(Language language) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_LANGUAGE;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, language.getName());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public Language getLanguageByID(long id) throws Exception {
        Language language = new Language();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_LANGUAGE_LIST + " ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    language.setR(rs.getLong("r"));
                    language.setId(rs.getLong("ID"));
                    language.setName(rs.getString("NAME"));
                    language.setActive(rs.getLong("ACTIVE"));
                    language.setData_date(rs.getDate("DATA_DATE"));
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            language = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return language;
    }

    @Override
    public boolean changeActiveLanguage(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_LANGUAGE;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updateLanguage(Language language, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_LANGUAGE;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, language.getName());
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Language> searchLanguage(String keyWord, long active) throws Exception {
        List<Language> languageList = new ArrayList<Language>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_LANGUAGE_LIST + " ACTIVE = ? AND (ID LIKE '%"+keyWord+"%' OR  LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR DATA_DATE LIKE '%" + keyWord + "%')";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Language language = new Language();
                    language.setR(rs.getLong("r"));
                    language.setId(rs.getLong("ID"));
                    language.setName(rs.getString("NAME"));
                    language.setActive(rs.getLong("ACTIVE"));
                    language.setData_date(rs.getDate("DATA_DATE"));
                    languageList.add(language);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return languageList;
    }

}
