/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.publisher;

import az.dao.DBHelper;
import az.model.Publisher;
import az.util.JdbcUtility;
import az.util.SqlConstant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orxan
 */
public class PublisherDaoImpl implements PublisherDao{

    @Override
    public List<Publisher> getPublisherList(long active) throws Exception {
        List<Publisher> publisherList = new ArrayList<Publisher>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLISHER_LIST + " ACTIVE = ?";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Publisher publisher = new Publisher();
                    publisher.setR(rs.getLong("r"));
                    publisher.setId(rs.getLong("ID"));
                    publisher.setName(rs.getString("NAME"));
                    publisher.setActive(rs.getLong("ACTIVE"));
                    publisher.setData_date(rs.getDate("DATA_DATE"));
                    publisherList.add(publisher);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publisherList;
    }

    @Override
    public boolean addPublisher(Publisher publisher) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.INSERT_PUBLISHER;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, publisher.getName());
                ps.execute();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public Publisher getPublisherByID(long id) throws Exception {
        Publisher publisher = new Publisher();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLISHER_LIST + " ID = ? ";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    publisher.setR(rs.getLong("r"));
                    publisher.setId(rs.getLong("ID"));
                    publisher.setName(rs.getString("NAME"));
                    publisher.setActive(rs.getLong("ACTIVE"));
                    publisher.setData_date(rs.getDate("DATA_DATE"));
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            publisher = null;
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publisher;
    }

    @Override
    public boolean changeActivePublisher(long id, long active) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.CHANGE_ACTIVE_PUBLISHER;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public boolean updatePublisher(Publisher publisher, long id) throws Exception {
        boolean result = false;
        Connection c = null;
        PreparedStatement ps = null;
        String sql = SqlConstant.UPDATE_PUBLISHER;
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setString(1, publisher.getName());
                ps.setLong(2, id);
                ps.executeUpdate();
                result = true;
            } else {
                System.out.println("Connectin is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.commit();
            JdbcUtility.close(c, ps, null);
        }
        return result;
    }

    @Override
    public List<Publisher> searchPublisher(String keyWord, long active) throws Exception {
        List<Publisher> publisherList = new ArrayList<Publisher>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = SqlConstant.GET_PUBLISHER_LIST + " ACTIVE = ? AND (ID LIKE '%"+keyWord+"%' OR  LOWER(NAME) LIKE LOWER('%" + keyWord + "%') OR DATA_DATE LIKE '%" + keyWord + "%')";
        try {
            c = DBHelper.getConnection();
            if (c != null) {
                ps = c.prepareStatement(sql);
                ps.setLong(1, active);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Publisher publisher = new Publisher();
                    publisher.setR(rs.getLong("r"));
                    publisher.setId(rs.getLong("ID"));
                    publisher.setName(rs.getString("NAME"));
                    publisher.setActive(rs.getLong("ACTIVE"));
                    publisher.setData_date(rs.getDate("DATA_DATE"));
                    publisherList.add(publisher);
                }
            } else {
                System.out.println("Connection is NULL!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JdbcUtility.close(c, ps, rs);
        }
        return publisherList;
    }
    
}
