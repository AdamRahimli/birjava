/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.dao.publisher;

import az.model.Publisher;
import java.util.List;

/**
 *
 * @author Orxan
 */
public interface PublisherDao {
    
    public List<Publisher> getPublisherList(long active) throws Exception;
    public boolean addPublisher(Publisher publisher) throws Exception;
    public Publisher getPublisherByID(long id) throws Exception;
    public boolean changeActivePublisher(long id,long active)throws Exception;
    public boolean updatePublisher(Publisher publisher,long id)  throws Exception;
    public List<Publisher> searchPublisher(String keyWord, long active) throws Exception;
    
}
