/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.util;

/**
 *
 * @author Orxan
 */
public class SqlConstant {

    /* SELECT */
    public static final String GET_BOOKSHELF_LIST = " SELECT "
            + " ROWNUM r,B.ID B_ID,B.NAME B_NAME,B.HARDCOVER B_HARDCOVER,P.NAME P_NAME,L.NAME L_NAME,C.NAME C_NAME,B.DIMENSIONS B_DIMENSIONS,B.PUBLICATION_DATE B_PUBLICATION_DATE, "
            + " S.NAME S_NAME,A.NAME A_NAME,A.SURNAME A_SURNAME,B.ISBN_10 B_ISBN_10 ,B.ISBN_13 B_ISBN_13,B.ACTIVE B_ACTIVE,B.DATA_DATE B_DATA_DATE, P.ID P_ID,L.ID L_ID,C.ID C_ID,S.ID S_ID,A.ID A_ID, "
            + " B.PHOTO_PATH B_PHOTO_PATH,B.PDF_PATH B_PDF_PATH "
            + " FROM BOOKSHELF B "
            + " INNER JOIN PUBLISHER P ON B.PUBLISHER_ID = P.ID "
            + " INNER JOIN LANGUAGE L ON B.LANGUAGE_ID = L.ID "
            + " INNER JOIN CATEGORIES C ON B.CATEGORIES_ID = C.ID "
            + " INNER JOIN PUBLICATION_CITY_COUNTRY S ON B.PUBLICATION_CITY_COUNTRY_ID = S.ID "
            + " INNER JOIN AUTHOR A ON B.AUTHOR_ID = A.ID "
            + " WHERE ";

    public static final String GET_USER_LIST = "SELECT "
            + " ROWNUM r,ID,NAME,SURNAME,GENDER,DOB,EMAIL,PHONE_NUMBER,USERNAME,PASSWORD,DATA_DATE,ACTIVE "
            + " FROM LOGIN "
            + " WHERE ";

    public static final String GET_AUTHOR_LIST = " SELECT ROWNUM r,ID,NAME,SURNAME,DOB,ACTIVE,DATA_DATE FROM AUTHOR "
            + " WHERE  ";

    public static final String GET_CATEGORIES_LIST = " SELECT ROWNUM r,ID,NAME, ACTIVE,DATA_DATE FROM CATEGORIES "
            + " WHERE  ";

    public static final String GET_LANGUAGE_LIST = " SELECT ROWNUM r,ID,NAME,ACTIVE,DATA_DATE FROM LANGUAGE "
            + " WHERE ";

    public static final String GET_PUBLISHER_LIST = " SELECT ROWNUM r,ID,NAME,ACTIVE,DATA_DATE FROM PUBLISHER "
            + " WHERE ";

    public static final String GET_PUBLICATION_CITY_COUNTRY_LIST = " SELECT ROWNUM r,ID,NAME,ACTIVE,DATA_DATE FROM PUBLICATION_CITY_COUNTRY "
            + " WHERE ";

    /* INSERT */
    public static final String INSERT_BOOKSHELF = "INSERT INTO BOOKSHELF "
            + " (ID,NAME,HARDCOVER,PUBLISHER_ID,LANGUAGE_ID,CATEGORIES_ID,DIMENSIONS,PUBLICATION_DATE,PUBLICATION_CITY_COUNTRY_ID,AUTHOR_ID,ISBN_10,ISBN_13,PHOTO_PATH,PDF_PATH) "
            + " VALUES(BOOKSHELF_SEQ.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String INSERT_USER = " INSERT INTO LOGIN(ID,NAME,SURNAME,GENDER,DOB,EMAIL,PHONE_NUMBER,USERNAME,PASSWORD) "
            + " VALUES(LOGIN_SEQ.NEXTVAL,?,?,?,?,?,?,?,?) ";

    public static final String INSERT_AUTHOR = " INSERT INTO AUTHOR(ID,NAME,SURNAME,DOB) "
            + " VALUES(AUTHOR_SEQ.NEXTVAL,?,?,?) ";

    public static final String INSERT_CATEGORIES = " INSERT INTO CATEGORIES(ID,NAME) "
            + " VALUES(CATEGORIES_SEQ.NEXTVAL,?) ";

    public static final String INSERT_LANGUAGE = " INSERT INTO LANGUAGE(ID,NAME) "
            + " VALUES(LANGUAGE_SEQ.NEXTVAL,?) ";

    public static final String INSERT_PUBLISHER = " INSERT INTO PUBLISHER(ID,NAME) "
            + " VALUES(PUBLISHER_SEQ.NEXTVAL,?) ";

    public static final String INSERT_PUBLICATION_CITY_COUNTRY = " INSERT INTO PUBLICATION_CITY_COUNTRY(ID,NAME) "
            + " VALUES(PUBLICATION_CITY_COUNTRY_SEQ.NEXTVAL,?) ";

    /* UPDATE */
    public static final String CHANGE_ACTIVE_BOOKSHELF = "UPDATE BOOKSHELF SET ACTIVE = ? "
            + " WHERE ID = ?";

    public static final String UPDATE_BOOKSHELF = "UPDATE BOOKSHELF SET NAME=?,HARDCOVER=?,PUBLISHER_ID=?,LANGUAGE_ID=?,CATEGORIES_ID=?,DIMENSIONS=?,PUBLICATION_DATE=?, "
            + " PUBLICATION_CITY_COUNTRY_ID=?,AUTHOR_ID=?,ISBN_10=?,ISBN_13=?,PDF_PATH=?,PHOTO_PATH=? "
            + " WHERE ID = ?";

    public static final String CHANGE_ACTIVE_USER = "UPDATE LOGIN SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_USER = "UPDATE LOGIN SET NAME = ? "
            + " WHERE ID = ? ";

    public static final String CHANGE_ACTIVE_AUTHOR = " UPDATE AUTHOR SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_AUTHOR = " UPDATE AUTHOR SET NAME = ?,SURNAME = ?,DOB = ? "
            + " WHERE ID = ? ";

    public static final String CHANGE_ACTIVE_CATEGORIES = " UPDATE CATEGORIES SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_CATEGORIES = " UPDATE CATEGORIES SET NAME = ? "
            + " WHERE ID = ? ";

    public static final String CHANGE_ACTIVE_LANGUAGE = " UPDATE LANGUAGE SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_LANGUAGE = " UPDATE LANGUAGE SET NAME = ? "
            + " WHERE ID = ? ";

    public static final String CHANGE_ACTIVE_PUBLISHER = " UPDATE PUBLISHER SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_PUBLISHER = " UPDATE PUBLISHER SET NAME = ? "
            + " WHERE ID = ? ";

    public static final String CHANGE_ACTIVE_PUBLICATION_CITY_COUNTRY = " UPDATE PUBLICATION_CITY_COUNTRY SET ACTIVE = ? "
            + " WHERE ID = ? ";

    public static final String UPDATE_PUBLICATION_CITY_COUNTRY = " UPDATE PUBLICATION_CITY_COUNTRY SET NAME = ? "
            + " WHERE ID = ? ";

}
