/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.main;

import az.dao.author.AuthorDao;
import az.dao.author.AuthorDaoImpl;
import az.dao.bookshelf.BookshelfDao;
import az.dao.bookshelf.BookshelfDaoImpl;
import az.dao.categories.CategoriesDao;
import az.dao.categories.CategoriesDaoImpl;
import az.dao.language.LanguageDao;
import az.dao.language.LanguageDaoImpl;
import az.dao.publicationCityCountry.PublicationCityCountryDao;
import az.dao.publicationCityCountry.PublicationCityCountryDaoImpl;
import az.dao.publisher.PublisherDao;
import az.dao.publisher.PublisherDaoImpl;
import az.model.AdvancedSearchBookshelf;
import az.model.Author;
import az.model.Bookshelf;
import az.model.Categories;
import az.model.Language;
import az.model.PublicationCityCountry;
import az.model.Publisher;
import az.service.author.AuthorService;
import az.service.author.AuthorServiceImpl;
import az.service.bookshelf.BookshelfService;
import az.service.bookshelf.BookshelfServiceImpl;
import az.service.categories.CategoriesService;
import az.service.categories.CategoriesServiceImpl;
import az.service.language.LanguageService;
import az.service.language.LanguageServiceImpl;
import az.service.publicationCityCountry.PublicationCityCountryService;
import az.service.publicationCityCountry.PublicationCityCountryServiceImpl;
import az.service.publisher.PublisherService;
import az.service.publisher.PublisherServiceImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Orxan
 */
@WebServlet(name = "MainDesignServlet", urlPatterns = {"/mds"})
public class MainDesignServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            BookshelfDao bookshelfDao = new BookshelfDaoImpl();
            BookshelfService bookshelfService = new BookshelfServiceImpl(bookshelfDao);

            AuthorDao authorDao = new AuthorDaoImpl();
            AuthorService authorService = new AuthorServiceImpl(authorDao);

            CategoriesDao categoriesDao = new CategoriesDaoImpl();
            CategoriesService categoriesService = new CategoriesServiceImpl(categoriesDao);

            LanguageDao languageDao = new LanguageDaoImpl();
            LanguageService languageService = new LanguageServiceImpl(languageDao);

            PublicationCityCountryDao publicationCityCountryDao = new PublicationCityCountryDaoImpl();
            PublicationCityCountryService publicationCityCountryService = new PublicationCityCountryServiceImpl(publicationCityCountryDao);

            PublisherDao publisherDao = new PublisherDaoImpl();
            PublisherService publisherService = new PublisherServiceImpl(publisherDao);

            String action = null;

            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            }
            if (action.equalsIgnoreCase("getBookshelfList")) {
                List<Bookshelf> bookshelfList = bookshelfService.getBookshelfList();
                for (Bookshelf book : bookshelfList) {
                    String path = book.getPhoto_path().substring(76);
                    book.setPhoto_path(path);
                }
                request.setAttribute("bookshelfList", bookshelfList);
                request.getRequestDispatcher("WEB-INF/pages/mainBookshelfList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getAdvancedSearch")) {
                List<Author> authorList = authorService.getAuthorList(1);
                List<Categories> categoriesList = categoriesService.getCategoriesList(1);
                List<Language> languageList = languageService.getLanguageList(1);
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.getPublicationCityCountryList(1);
                List<Publisher> publisherList = publisherService.getPublisherList(1);

                request.setAttribute("authorList", authorList);
                request.setAttribute("categoriesList", categoriesList);
                request.setAttribute("languageList", languageList);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.setAttribute("publisherList", publisherList);

                request.getRequestDispatcher("WEB-INF/pages/mainAdvancedSearch.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("advancedSearchBookshelf")) {
                Long authorId = Long.parseLong(request.getParameter("authorId"));
                Long categoriesId = Long.parseLong(request.getParameter("categoriesId"));
                Long languageId = Long.parseLong(request.getParameter("languageId"));
                Long publisherId = Long.parseLong(request.getParameter("publisherId"));
                Long publication_city_country_id = Long.parseLong(request.getParameter("publication_city_country_id"));
                String beginDate = request.getParameter("beginDate");
                String endDate = request.getParameter("endDate");

                AdvancedSearchBookshelf advancedSearchBookshelf = new AdvancedSearchBookshelf();
                advancedSearchBookshelf.setAuthorId(authorId);
                advancedSearchBookshelf.setCategoriesId(categoriesId);
                advancedSearchBookshelf.setLanguageId(languageId);
                advancedSearchBookshelf.setPublisherId(publisherId);
                advancedSearchBookshelf.setPublication_city_country_id(publication_city_country_id);
                advancedSearchBookshelf.setBeginDate(beginDate);
                advancedSearchBookshelf.setEndDate(endDate);

                List<Bookshelf> bookshelfList = bookshelfService.advanceSearchBookshelf(advancedSearchBookshelf);
                request.setAttribute("bookshelfList", bookshelfList);
                request.getRequestDispatcher("WEB-INF/pages/mainBookshelfList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchBookshelfData")) {
                String keyWord = request.getParameter("keyWord");
                List<Bookshelf> bookshelfList = bookshelfService.searchBookshelf(keyWord);
                request.setAttribute("bookshelfList", bookshelfList);
                request.getRequestDispatcher("WEB-INF/pages/mainBookshelfList.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
