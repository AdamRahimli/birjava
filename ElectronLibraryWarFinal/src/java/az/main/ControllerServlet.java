/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.main;

import az.dao.DBHelper;
import az.dao.author.AuthorDao;
import az.dao.author.AuthorDaoImpl;
import az.dao.bookshelf.BookshelfDao;
import az.dao.bookshelf.BookshelfDaoImpl;
import az.dao.categories.CategoriesDao;
import az.dao.categories.CategoriesDaoImpl;
import az.dao.language.LanguageDao;
import az.dao.language.LanguageDaoImpl;
import az.dao.publicationCityCountry.PublicationCityCountryDao;
import az.dao.publicationCityCountry.PublicationCityCountryDaoImpl;
import az.dao.publisher.PublisherDao;
import az.dao.publisher.PublisherDaoImpl;
import az.model.AdvancedSearchBookshelf;
import az.model.Author;
import az.model.Bookshelf;
import az.model.Categories;
import az.model.Language;
import az.model.PublicationCityCountry;
import az.model.Publisher;
import az.service.author.AuthorService;
import az.service.author.AuthorServiceImpl;
import az.service.bookshelf.BookshelfService;
import az.service.bookshelf.BookshelfServiceImpl;
import az.service.categories.CategoriesService;
import az.service.categories.CategoriesServiceImpl;
import az.service.language.LanguageService;
import az.service.language.LanguageServiceImpl;
import az.service.publicationCityCountry.PublicationCityCountryService;
import az.service.publicationCityCountry.PublicationCityCountryServiceImpl;
import az.service.publisher.PublisherService;
import az.service.publisher.PublisherServiceImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author BirAdam
 */
@WebServlet(name = "ControllerServlet", urlPatterns = {"/cs"})
public class ControllerServlet extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "upload";
    private static final int THRESHOLD_SIZE = 1024 * 1024 * 3; //3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 10; //10MB
    private static final int REQUEST_SIZE = 1024 * 1024 * 50; //50MB

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            BookshelfDao bookshelfDao = new BookshelfDaoImpl();
            BookshelfService bookshelfService = new BookshelfServiceImpl(bookshelfDao);

            AuthorDao authorDao = new AuthorDaoImpl();
            AuthorService authorService = new AuthorServiceImpl(authorDao);

            CategoriesDao categoriesDao = new CategoriesDaoImpl();
            CategoriesService categoriesService = new CategoriesServiceImpl(categoriesDao);

            LanguageDao languageDao = new LanguageDaoImpl();
            LanguageService languageService = new LanguageServiceImpl(languageDao);

            PublicationCityCountryDao publicationCityCountryDao = new PublicationCityCountryDaoImpl();
            PublicationCityCountryService publicationCityCountryService = new PublicationCityCountryServiceImpl(publicationCityCountryDao);

            PublisherDao publisherDao = new PublisherDaoImpl();
            PublisherService publisherService = new PublisherServiceImpl(publisherDao);

            String action = null;

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            }
            if (action.equalsIgnoreCase("getBookshelfList")) {
                List<Bookshelf> bookshelfList = bookshelfService.getBookshelfList();
                List<Author> authorList = authorService.getAuthorList(1);
                List<Categories> categoriesList = categoriesService.getCategoriesList(1);
                List<Language> languageList = languageService.getLanguageList(1);
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.getPublicationCityCountryList(1);
                List<Publisher> publisherList = publisherService.getPublisherList(1);

                request.setAttribute("authorList", authorList);
                request.setAttribute("categoriesList", categoriesList);
                request.setAttribute("languageList", languageList);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.setAttribute("publisherList", publisherList);
                request.setAttribute("bookshelfList", bookshelfList);

                request.getRequestDispatcher("WEB-INF/pages/bookshelfList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getAuthorList")) {
                List<Author> authorList = authorService.getAuthorList(1);
                request.setAttribute("authorList", authorList);
                request.getRequestDispatcher("WEB-INF/pages/authorList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getCategoriesList")) {
                List<Categories> categorieseList = categoriesService.getCategoriesList(1);
                request.setAttribute("categoriesList", categorieseList);
                request.getRequestDispatcher("WEB-INF/pages/categoriesList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getLanguageList")) {
                List<Language> languageList = languageService.getLanguageList(1);
                request.setAttribute("languageList", languageList);
                request.getRequestDispatcher("WEB-INF/pages/languageList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getPublicationCityCountryList")) {
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.getPublicationCityCountryList(1);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.getRequestDispatcher("WEB-INF/pages/publicationCityCountry.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("getPublisherList")) {
                List<Publisher> publisherList = publisherService.getPublisherList(1);
                request.setAttribute("publisherList", publisherList);
                request.getRequestDispatcher("WEB-INF/pages/publisherList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("newBookshelf")) {
                List<Author> authorList = authorService.getAuthorList(1);
                List<Categories> categoriesList = categoriesService.getCategoriesList(1);
                List<Language> languageList = languageService.getLanguageList(1);
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.getPublicationCityCountryList(1);
                List<Publisher> publisherList = publisherService.getPublisherList(1);

                request.setAttribute("authorList", authorList);
                request.setAttribute("categoriesList", categoriesList);
                request.setAttribute("languageList", languageList);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.setAttribute("publisherList", publisherList);

                request.getRequestDispatcher("views/newBookshelf.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("addAuthor")) {
                String name = request.getParameter("name");
                String surname = request.getParameter("surname");
                String dob = request.getParameter("dob");

                Author author = new Author();
                author.setName(name);
                author.setSurname(surname);
                author.setDob(df.parse(dob));

                response.setContentType("text/html");
                boolean isAdded = authorService.addAuthor(author);
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("addCategories")) {
                String name = request.getParameter("name");

                Categories categories = new Categories();
                categories.setName(name);

                response.setContentType("text/html");
                boolean isAdded = categoriesService.addCategories(categories);
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("addLanguage")) {
                String name = request.getParameter("name");

                Language language = new Language();
                language.setName(name);

                response.setContentType("text/html");
                boolean isAdded = languageService.addLanguage(language);
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("addPublicationCityCountry")) {
                String name = request.getParameter("name");

                PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                publicationCityCountry.setName(name);

                response.setContentType("text/html");
                boolean isAdded = publicationCityCountryService.addPublicationCityCountry(publicationCityCountry);
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("addPublisher")) {
                String name = request.getParameter("name");

                Publisher publisher = new Publisher();
                publisher.setName(name);

                response.setContentType("text/html");
                boolean isAdded = publisherService.addPublisher(publisher);
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("addBookshelf")) {
                String name = null;
                String hardcover = null;
                Long publisher = null;
                Long language = null;
                Long categories = null;
                String dimensions = null;
                String publication_date = null;
                Long publication_city_county = null;
                Long author = null;
                String isbn_10 = null;
                String isbn_13 = null;
                Long bookshelfId = null;
                String photo_path = null;
                String pdf_path = null;

                String filePath = "";
                String fileName = "";
                String newFileName = "";

                /*  if(!ServletFileUpload.isMultipartContent(request)){
                        response.getWriter().println("Does not support!");
                        return;
                    }  */
                //configures some settings
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(THRESHOLD_SIZE);
                factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

                ServletFileUpload upload = new ServletFileUpload(factory);
                upload.setFileSizeMax(MAX_FILE_SIZE);
                upload.setSizeMax(REQUEST_SIZE);
                //System.out.println(getServletContext().getRealPath(""));
                //constructs the directory path to store upload file
                String uploadPath = getServletContext().getRealPath("") + UPLOAD_DIRECTORY;
                //creates the directory if it does not exist
                File uploadDir = new File(uploadPath);
                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }

                try {
                    //parses the request's content to extract file data
                    List formItems = upload.parseRequest(request);
                    Iterator iter = formItems.iterator();
                    UUID uuid = UUID.randomUUID();

                    //iterates over form's fields
                    while (iter.hasNext()) {
                        FileItem item = (FileItem) iter.next();
                        //processes only fields that are not form fields
                        if (!item.isFormField()) {
                            fileName = new File(item.getName()).getName();
                            newFileName = fileName.replace(fileName.substring(0, fileName.lastIndexOf(".")), uuid.toString());
                            if (item.getFieldName().equalsIgnoreCase("photoB")) {
                                filePath = uploadPath + File.separator + "photo" + File.separator + newFileName;
                                photo_path = filePath;
                                File storeFile = new File(filePath);

                                //saves the file on disk
                                item.write(storeFile);
                            }
                            if (item.getFieldName().equalsIgnoreCase("pdfB")) {
                                filePath = uploadPath + File.separator + "pdf" + File.separator + newFileName;
                                pdf_path = filePath;
                                File storeFile = new File(filePath);

                                //saves the file on disk
                                item.write(storeFile);
                            }

                        } else {
                            if (item.getFieldName().equalsIgnoreCase("nameB")) {
                                name = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("hardcoverB")) {
                                hardcover = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("publisherB")) {
                                publisher = Long.parseLong(item.getString());
                            }
                            if (item.getFieldName().equalsIgnoreCase("languageB")) {
                                language = Long.parseLong(item.getString());
                            }
                            if (item.getFieldName().equalsIgnoreCase("isbn_10B")) {
                                isbn_10 = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("isbn_13B")) {
                                isbn_13 = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("categoriesB")) {
                                categories = Long.parseLong(item.getString());
                            }
                            if (item.getFieldName().equalsIgnoreCase("dimensionsB")) {
                                dimensions = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("publication_dateB")) {
                                publication_date = item.getString();
                            }
                            if (item.getFieldName().equalsIgnoreCase("publication_city_countryB")) {
                                publication_city_county = Long.parseLong(item.getString());
                            }
                            if (item.getFieldName().equalsIgnoreCase("authorB")) {
                                author = Long.parseLong(item.getString());
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                Bookshelf bookshelf = new Bookshelf();
                bookshelf.setName(name);
                bookshelf.setHardcover(hardcover);
                bookshelf.setPublisher_id(publisher);
                bookshelf.setLanguage_id(language);
                bookshelf.setCategories_id(categories);
                bookshelf.setDimensions(dimensions);
                bookshelf.setPublication_date(df.parse(publication_date));
                bookshelf.setPublication_city_country_id(publication_city_county);
                bookshelf.setAuthor_id(author);
                bookshelf.setIsbn_10(isbn_10);
                bookshelf.setIsbn_13(isbn_13);
                bookshelf.setPhoto_path(photo_path);
                bookshelf.setPdf_path(pdf_path);
                boolean isAdded = bookshelfService.addBookshelf(bookshelf);
                response.setContentType("text/html");
                if (isAdded) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
                response.sendRedirect("main.jsp");
            } else if (action.equalsIgnoreCase("downloadPdf")) {
                //    CourseDao courseDao = new CourseDaoImpl();
                //    CourseService courseService = new CourseServiceImpl(courseDao);

                try {

                    /*long studentId = 1021;
            Student student =  courseService.getStudentById(studentId);
            System.out.println(student.getImageFullPath());*/
                    String filePath = request.getParameter("path");
                    //  String filePath = student.getImageFullPath();

                    File downloadFile = new File(filePath);
                    FileInputStream inStream = new FileInputStream(downloadFile);

                    ServletContext context = getServletContext();

                    // gets MIME type of the file
                    String mimeType = context.getMimeType(filePath);
                    if (mimeType == null) {
                        // set to binary type if MIME mapping not found
                        mimeType = "application/octet-stream";
                    }
                    System.out.println("MIME type: " + mimeType);

                    // modifies response
                    response.setContentType(mimeType);
                    response.setContentLength((int) downloadFile.length());

                    // forces download
                    String headerKey = "Content-Disposition";
                    String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
                    response.setHeader(headerKey, headerValue);

                    // obtains response's output stream
                    OutputStream outStream = response.getOutputStream();

                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;

                    while ((bytesRead = inStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }

                    inStream.close();
                    outStream.close();
                    response.setContentType("text/html");
                    out.write("success");
                } catch (Exception e) {
                    e.printStackTrace();
                    response.setContentType("text/html");
                    out.write("error");
                }

            } else if (action.equalsIgnoreCase("editBookshelfData")) {
                Long bookshelfId = Long.parseLong(request.getParameter("bookshelfId"));
                Bookshelf bookshelf = bookshelfService.getBookshelfByID(bookshelfId);

                List<Author> authorList = authorService.getAuthorList(1);
                List<Categories> categoriesList = categoriesService.getCategoriesList(1);
                List<Language> languageList = languageService.getLanguageList(1);
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.getPublicationCityCountryList(1);
                List<Publisher> publisherList = publisherService.getPublisherList(1);
                
                request.setAttribute("authorList", authorList);
                request.setAttribute("categoriesList", categoriesList);
                request.setAttribute("languageList", languageList);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.setAttribute("publisherList", publisherList);

                request.setAttribute("bookshelf", bookshelf);
                request.getRequestDispatcher("WEB-INF/pages/editBookshelf.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("editAuthorData")) {
                Long authorId = Long.parseLong(request.getParameter("authorId"));
                Author author = authorService.getAuthorByID(authorId);
                request.setAttribute("author", author);
                request.getRequestDispatcher("WEB-INF/pages/editAuthor.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("editCategoriesData")) {
                Long categoriesId = Long.parseLong(request.getParameter("categoriesId"));
                Categories categories = categoriesService.getCategoriesByID(categoriesId);
                request.setAttribute("categories", categories);
                request.getRequestDispatcher("WEB-INF/pages/editCategories.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("editLanguageData")) {
                Long languageId = Long.parseLong(request.getParameter("languageId"));
                Language language = languageService.getLanguageByID(languageId);
                request.setAttribute("language", language);
                request.getRequestDispatcher("WEB-INF/pages/editLanguage.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("editPublicationCityCoountryData")) {
                Long publicationCityCountryId = Long.parseLong(request.getParameter("publicationCityCountryId"));
                PublicationCityCountry publicationCityCountry = publicationCityCountryService.getPublicationCityCountryByID(publicationCityCountryId);
                request.setAttribute("publicationCityCountry", publicationCityCountry);
                request.getRequestDispatcher("WEB-INF/pages/editPublicationCityCountry.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("editPublisherData")) {
                Long publisherId = Long.parseLong(request.getParameter("publisherId"));
                Publisher publisher = publisherService.getPublisherByID(publisherId);
                request.setAttribute("publisher", publisher);
                request.getRequestDispatcher("WEB-INF/pages/editPublisher.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("updateBookshelf")) {
                String name = request.getParameter("name");
                String hardcover = request.getParameter("hardcover");
                Long publisher = Long.parseLong(request.getParameter("publisher"));
                Long language = Long.parseLong(request.getParameter("language"));
                Long categories = Long.parseLong(request.getParameter("categories"));
                String dimensions = request.getParameter("dimensions");
                String publication_date = request.getParameter("publication_date");
                Long publication_city_county = Long.parseLong(request.getParameter("publication_city_county"));
                Long author = Long.parseLong(request.getParameter("author"));
                String isbn_10 = request.getParameter("isbn_10");
                String isbn_13 = request.getParameter("isbn_13");
                Long bookshelfId = Long.parseLong(request.getParameter("bookshelfId"));
                String photo_path = request.getParameter("photo_path");
                String pdf_path = request.getParameter("pdf_path");

                Bookshelf bookshelf = new Bookshelf();
                bookshelf.setName(name);
                bookshelf.setHardcover(hardcover);
                bookshelf.setPublisher_id(publisher);
                bookshelf.setLanguage_id(language);
                bookshelf.setCategories_id(categories);
                bookshelf.setDimensions(dimensions);
                bookshelf.setPublication_date(df.parse(publication_date));
                bookshelf.setPublication_city_country_id(publication_city_county);
                bookshelf.setAuthor_id(author);
                bookshelf.setIsbn_10(isbn_10);
                bookshelf.setIsbn_13(isbn_13);
                bookshelf.setPhoto_path(photo_path);
                bookshelf.setPdf_path(pdf_path);
                boolean isUpdated = bookshelfService.updateBookshelf(bookshelf, bookshelfId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("updateAuthor")) {
                String name = request.getParameter("name");
                String surname = request.getParameter("surname");
                String dob = request.getParameter("dob");
                Long authorId = Long.parseLong(request.getParameter("authorId"));

                Author author = new Author();
                author.setName(name);
                author.setSurname(surname);
                author.setDob(df.parse(dob));

                boolean isUpdated = authorService.updateAuthor(author, authorId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("updateCategories")) {
                String name = request.getParameter("name");
                Long categoriesId = Long.parseLong(request.getParameter("categoriesId"));

                Categories categories = new Categories();
                categories.setName(name);

                boolean isUpdated = categoriesService.updateCategories(categories, categoriesId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("updateLanguage")) {
                String name = request.getParameter("name");
                Long languageId = Long.parseLong(request.getParameter("languageId"));

                Language language = new Language();
                language.setName(name);

                boolean isUpdated = languageService.updateLanguage(language, languageId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("updatePublicationCityCountry")) {
                String name = request.getParameter("name");
                Long publicationCityCountryId = Long.parseLong(request.getParameter("publicationCityCountryId"));

                PublicationCityCountry publicationCityCountry = new PublicationCityCountry();
                publicationCityCountry.setName(name);

                boolean isUpdated = publicationCityCountryService.updatePublicationCityCountry(publicationCityCountry, publicationCityCountryId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("updatePublisher")) {
                String name = request.getParameter("name");
                Long publisherId = Long.parseLong(request.getParameter("publisherId"));

                Publisher publisher = new Publisher();
                publisher.setName(name);

                boolean isUpdated = publisherService.updatePublisher(publisher, publisherId);
                response.setContentType("text/html");
                if (isUpdated) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deleteBookshelf")) {
                Long bookshelfId = Long.parseLong(request.getParameter("bookshelfId"));
                boolean isDeleted = bookshelfService.changeActiveBookshelf(bookshelfId, 0);
                response.setContentType("text/html");
                if (isDeleted) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deleteAuthor")) {
                Long authorId = Long.parseLong(request.getParameter("authorId"));
                boolean isDeleted = authorService.changeActiveAuthor(authorId, 0);
                response.setContentType("text/html");
                if (isDeleted) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deleteLanguage")) {
                Long languageId = Long.parseLong(request.getParameter("languageId"));
                boolean isDeleted = languageService.changeActiveLanguage(languageId, 0);
                response.setContentType("text/html");
                if (isDeleted) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deleteCategories")) {
                Long categoriesId = Long.parseLong(request.getParameter("categoriesId"));
                boolean isDeleted = categoriesService.changeActiveCategories(categoriesId, 0);
                response.setContentType("text/html");
                if (isDeleted) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deletePublicationCityCountryTable")) {
                Long publicationCityCountryTableId = Long.parseLong(request.getParameter("publicationCityCountryTableId"));
                boolean isDeleted = publicationCityCountryService.changeActivePublicationCityCountry(publicationCityCountryTableId, 0);
                response.setContentType("text/html");
                if (isDeleted) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("deletePublisher")) {
                Long publisherId = Long.parseLong(request.getParameter("publisherId"));
                boolean isDelete = publisherService.changeActivePublisher(publisherId, 0);
                response.setContentType("text/html");
                if (isDelete) {
                    out.write("success");
                } else {
                    out.write("failed");
                }
            } else if (action.equalsIgnoreCase("searchBookshelfData")) {
                String keyWord = request.getParameter("keyWord");
                List<Bookshelf> bookshelfList = bookshelfService.searchBookshelf(keyWord);
                request.setAttribute("bookshelfList", bookshelfList);
                request.getRequestDispatcher("WEB-INF/pages/bookshelfList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchAuthorData")) {
                String keyWord = request.getParameter("keyWord");
                List<Author> authorList = authorService.searchAuthor(keyWord, 1);
                request.setAttribute("authorList", authorList);
                request.getRequestDispatcher("WEB-INF/pages/authorList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchCategoriesData")) {
                String keyWord = request.getParameter("keyWord");
                List<Categories> categoriesList = categoriesService.searchCategories(keyWord, 1);
                request.setAttribute("categoriesList", categoriesList);
                request.getRequestDispatcher("WEB-INF/pages/categoriesList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchLanguageData")) {
                String keyWord = request.getParameter("keyWord");
                List<Language> languageList = languageService.searchLanguage(keyWord, 1);
                request.setAttribute("languageList", languageList);
                request.getRequestDispatcher("WEB-INF/pages/languageList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchPublicationCityCountry")) {
                String keyWord = request.getParameter("keyWord");
                List<PublicationCityCountry> publicationCityCountryList = publicationCityCountryService.searchPublicationCityCountry(keyWord, 1);
                request.setAttribute("publicationCityCountryList", publicationCityCountryList);
                request.getRequestDispatcher("WEB-INF/pages/publicationCityCountryList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("searchPublisher")) {
                String keyWord = request.getParameter("keyWord");
                List<Publisher> publisherList = publisherService.searchPublisher(keyWord, 1);
                request.setAttribute("publisherList", publisherList);
                request.getRequestDispatcher("WEB-INF/pages/publisherList.jsp").forward(request, response);
            } else if (action.equalsIgnoreCase("advancedSearchBookshelf")) {
                Long authorId = Long.parseLong(request.getParameter("authorId"));
                Long categoriesId = Long.parseLong(request.getParameter("categoriesId"));
                Long languageId = Long.parseLong(request.getParameter("languageId"));
                Long publisherId = Long.parseLong(request.getParameter("publisherId"));
                Long publication_city_country_id = Long.parseLong(request.getParameter("publication_city_country_id"));
                String beginDate = request.getParameter("beginDate");
                String endDate = request.getParameter("endDate");
                
                AdvancedSearchBookshelf advancedSearchBookshelf = new AdvancedSearchBookshelf();
                advancedSearchBookshelf.setAuthorId(authorId);
                advancedSearchBookshelf.setCategoriesId(categoriesId);
                advancedSearchBookshelf.setLanguageId(languageId);
                advancedSearchBookshelf.setPublisherId(publisherId);
                advancedSearchBookshelf.setPublication_city_country_id(publication_city_country_id);
                advancedSearchBookshelf.setBeginDate(beginDate);
                advancedSearchBookshelf.setEndDate(endDate);
                
                List<Bookshelf> bookshelfList = bookshelfService.advanceSearchBookshelf(advancedSearchBookshelf);
                request.setAttribute("bookshelfList", bookshelfList);
                request.getRequestDispatcher("WEB-INF/pages/bookshelfData.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
