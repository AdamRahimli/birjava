/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.model;

import java.util.Date;

/**
 *
 * @author BirAdam
 */
public abstract class  Model {
    
    private long r;
    private long id;
    private long active;
    private Date data_date;

    public long getR() {
        return r;
    }

    public void setR(long r) {
        this.r = r;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getActive() {
        return active;
    }

    public void setActive(long active) {
        this.active = active;
    }

    public Date getData_date() {
        return data_date;
    }

    public void setData_date(Date data_date) {
        this.data_date = data_date;
    }
    
    
}
