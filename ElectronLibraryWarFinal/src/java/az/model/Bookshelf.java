/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.model;

import java.util.Date;

/**
 *
 * @author BirAdam
 */
public class Bookshelf extends Model{
    
    private String name;
    private String hardcover;
    private long publisher_id;
    private long language_id;
    private String isbn_10;
    private String isbn_13;
    private long categories_id;
    private String dimensions;
    private Date   publication_date;
    private long publication_city_country_id;
    private long author_id;
    
    private String photo_path;
    private String pdf_path;
    
    private Author author;
    private Categories categories;
    private Language language;
    private PublicationCityCountry publicationCityCountry;
    private Publisher publisher;

    public String getPhoto_path() {
        return photo_path;
    }

    public void setPhoto_path(String photo_path) {
        this.photo_path = photo_path;
    }

    public String getPdf_path() {
        return pdf_path;
    }

    public void setPdf_path(String pdf_path) {
        this.pdf_path = pdf_path;
    }    
    
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public PublicationCityCountry getPublicationCityCountry() {
        return publicationCityCountry;
    }

    public void setPublicationCityCountry(PublicationCityCountry publicationCityCountry) {
        this.publicationCityCountry = publicationCityCountry;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHardcover() {
        return hardcover;
    }

    public void setHardcover(String hardcover) {
        this.hardcover = hardcover;
    }

    public long getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(long publisher_id) {
        this.publisher_id = publisher_id;
    }

    public long getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(long language_id) {
        this.language_id = language_id;
    }

    public String getIsbn_10() {
        return isbn_10;
    }

    public void setIsbn_10(String isbn_10) {
        this.isbn_10 = isbn_10;
    }

    public String getIsbn_13() {
        return isbn_13;
    }

    public void setIsbn_13(String isbn_13) {
        this.isbn_13 = isbn_13;
    }

    public long getCategories_id() {
        return categories_id;
    }

    public void setCategories_id(long categories_id) {
        this.categories_id = categories_id;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public Date getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(Date publication_date) {
        this.publication_date = publication_date;
    }

    public long getPublication_city_country_id() {
        return publication_city_country_id;
    }

    public void setPublication_city_country_id(long publication_city_country_id) {
        this.publication_city_country_id = publication_city_country_id;
    }

    public long getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(long author_id) {
        this.author_id = author_id;
    }

   
    
}
