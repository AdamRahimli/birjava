<%-- 
    Document   : login
    Created on : 26.05.2018, 1:04:53
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        
        <script type="text/javascript">
            history.pushState(null,null,'login.jsp');
            window.addEventListener('popstate',function (event){
               history.pushState(null,null,'login.jsp'); 
            });
        </script>
    </head>
    <body>
        <form action="ls?action=login" method="post">
            <input type="text" placeholder="Username" name="username"> <br>
            <input type="password" placeholder="Password" name="password"> <br/>
            <input type="submit" value="Login"/> <br>
            ${invalid}
        </form>
    </body>
</html>
