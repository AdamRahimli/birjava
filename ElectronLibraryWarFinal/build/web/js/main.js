var globalBtnId;
var globalBookshelfId;
var globalAuthorId;
var globalCategoriesId;
var globalLanguageId;
var globalPublicationCityCountryId;
var globalPublisherId;

function downloadPdf(path){
    $.ajax({
        url: 'cs?action=downloadPdf',
        type:'POST',
        data: 'path='+path,
        dataType: 'text',
        success: function (data) {
            if(data=='success'){
                alert('Download...');
            }else{
                alert('Error!!!');
            }
        }
    });
}

function getBookshelfList() {
    $.ajax({
        url: 'cs?action=getBookshelfList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $(".ui-layout-center").html(data);
        }
    });
}

function getAuthorList() {
    $.ajax({
        url: 'cs?action=getAuthorList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function getCategoriesList() {
    $.ajax({
        url: 'cs?action=getCategoriesList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function getLanguageList() {
    $.ajax({
        url: 'cs?action=getLanguageList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function getPublicationCityCountryList() {
    $.ajax({
        url: 'cs?action=getPublicationCityCountryList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function getPublisherList() {
    $.ajax({
        url: 'cs?action=getPublisherList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function addBookshelf() {
    var photo = $('#bPhotoId').val();
    var name = $('#bNameId').val();
    var hardcover = $('#bHardcoverId').val();
    var publisher = $('#bPublisherId').val();
    var language = $('#bLanguageId').val();
    var categories = $('#bCategoriesId').val();
    var dimensions = $('#bDimensionsId').val();
    var publication_date = $('#bPublicationDateId').val();
    var publication_city_county = $('#bPublicationCityCountyId').val();
    var author = $('#bAuthorId').val();
    var isbn_10 = $('#bIsbn10Id').val();
    var isbn_13 = $('#bIsbn13Id').val();
    var pdf = $('#bPdfId').val();

    var data = {
        photo: photo,
        name: name,
        hardcover: hardcover,
        publisher: publisher,
        language: language,
        categories: categories,
        dimensions: dimensions,
        publication_date: publication_date,
        publication_city_county: publication_city_county,
        author: author,
        isbn_10: isbn_10,
        isbn_13: isbn_13,
        pdf: pdf
    };

    $.ajax({
        url: 'cs?action=addBookshelf',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getBookshelfList();
                $('#newBookshelfDialogId').dialog('close');
                alert('Bookshelf data has been successfully added!');
            } else {
                alert('Problem!!! Bookshelf data has not been successfully added!');
            }
        }
    });
}

function addAuthor() {
    var name = $('#aNameId').val();
    var surname = $('#aSurnameId').val();
    var dob = $('#aDobId').val();

    var data = {
        name: name,
        surname: surname,
        dob: dob
    };
    $.ajax({
        url: 'cs?action=addAuthor',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getAuthorList();
                $('#newAuthorDialogId').dialog('close');
                alert('Aurhor data has been successfully added!');
            } else {
                alert('Problem!!! Author data has not been successfully added!');
            }
        }
    });
}

function addCategories() {
    var name = $('#cNameId').val();

    $.ajax({
        url: 'cs?action=addCategories',
        type: 'POST',
        data: 'name=' + name,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getCategoriesList();
                $('#newCategoriesDialogId').dialog('close');
                alert('Categories data has been successfully added!');
            } else {
                alert('Problems!!! Categoories data has not been successfully added!');
            }
        }
    });
}

function addLanguage() {
    var name = $('#lNameId').val();

    $.ajax({
        url: 'cs?action=addLanguage',
        type: 'POST',
        data: 'name=' + name,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getLanguageList();
                $('#newLanguageDialogId').dialog('close');
                alert('Language data has been successfully added!');
            } else {
                alert('Problems!!! Language data has not been successfully added!');
            }
        }
    });
}

function addPublicationCityCountry() {
    var name = $('#pcNameId').val();

    $.ajax({
        url: 'cs?action=addPublicationCityCountry',
        type: 'POST',
        data: 'name=' + name,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getPublicationCityCountryList();
                $('#newPublicationCityCountryDialogId').dialog('close');
                alert('Publication City Country data has been succesfully added!');
            } else {
                alert('Problems!!! Publication City Country data has not been successfully added!');
            }
        }
    });
}

function addPublisher() {
    var name = $('#pNameId').val();

    $.ajax({
        url: 'cs?action=addPublisher',
        type: 'POST',
        data: 'name=' + name,
        dataType: 'text',
        success: function (data) {
            if (data = 'success') {
                getPublisherList();
                $('#newPublisherDialogId').dialog('close');
                alert('Publisher data has been successfully added!');
            } else {
                alert('Problems!!! Publisher data has not been successfully added!');
            }
        }
    });
}

function editBookshelfData(bookshelfId) {
    globalBookshelfId = bookshelfId;
    $.ajax({
        url: 'cs?action=editBookshelfData',
        type: 'GET',
        dataType: 'html',
        data: 'bookshelfId=' + bookshelfId,
        success: function (data) {
            $('#editBookshelfDialogId').html(data);
            $('#editBookshelfDialogId').dialog('open');
        }
    });
}

function editAuthorData(authorId) {
    globalAuthorId = authorId;
    $.ajax({
        url: 'cs?action=editAuthorData',
        type: 'GET',
        dataType: 'html',
        data: 'authorId=' + authorId,
        success: function (data) {
            $('#editAuthorDialogId').html(data);
            $('#editAuthorDialogId').dialog('open');
        }
    });
}

function editCategories(categoriesId) {
    globalCategoriesId = categoriesId;
    $.ajax({
        url: 'cs?action=editCategoriesData',
        type: 'GET',
        dataType: 'html',
        data: 'categoriesId=' + categoriesId,
        success: function (data) {
            $('#editCategoriesDialogId').html(data);
            $('#editCategoriesDialogId').dialog('open');
        }
    });
}

function editLanguage(languageId) {
    globalLanguageId = languageId;
    $.ajax({
        url: 'cs?action=editLanguageData',
        type: 'GET',
        dataType: 'html',
        data: 'languageId=' + languageId,
        success: function (data) {
            $('#editLanguageDialogId').html(data);
            $('#editLanguageDialogId').dialog('open');
        }
    });
}

function editPublicationCityCountry(publicationCityCountryId) {
    globalPublicationCityCountryId = publicationCityCountryId;
    $.ajax({
        url: 'cs?action=editPublicationCityCoountryData',
        type: 'GET',
        dataType: 'html',
        data: 'publicationCityCountryId=' + publicationCityCountryId,
        success: function (data) {
            $('#editPublicationCityCountryDialogId').html(data);
            $('#editPublicationCityCountryDialogId').dialog('open');
        }
    });
}

function editPublisher(publisherId) {
    globalPublisherId = publisherId;
    $.ajax({
        url: 'cs?action=editPublisherData',
        type: 'GET',
        dataType: 'html',
        data: 'publisherId=' + publisherId,
        success: function (data) {
            $('#editPublisherDialogId').html(data);
            $('#editPublisherDialogId').dialog('open');
        }
    });
}

function updateBookshelf() {
    var name = $('#bNameIdU').val();
    var hardcover = $('#bHardcoverIdU').val();
    var publisher = $('#bPublisherIdU').val();
    var language = $('#bLanguageIdU').val();
    var categories = $('#bCategoriesIdU').val();
    var dimensions = $('#bDimensionsIdU').val();
    var publication_date = $('#bPublicationDateIdU').val();
    var publication_city_county = $('#bPublicationCityCountyIdU').val();
    var author = $('#bAuthorIdU').val();
    var isbn_10 = $('#bIsbn10IdU').val();
    var isbn_13 = $('#bIsbn13IdU').val();
    var photo_path = $('#bPhotoIdU').val();
    var pdf_path = $('#bPdfIdU').val();

    var data = {
        name: name,
        hardcover: hardcover,
        publisher: publisher,
        language: language,
        categories: categories,
        dimensions: dimensions,
        publication_date: publication_date,
        publication_city_county: publication_city_county,
        author: author,
        isbn_10: isbn_10,
        isbn_13: isbn_13,
        photo_path: photo_path,
        pdf_path: pdf_path,
        bookshelfId: globalBookshelfId
    };

    $.ajax({
        url: 'cs?action=updateBookshelf',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getBookshelfList();
                $('#editBookshelfDialogId').dialog('close');
                alert('Bookshelf data has been successfully Updated!');
            } else {
                alert('Problem!!! Bookshelf data has not been successfully Updated!');
            }
        }
    });
}

function updateAuthor() {
    var name = $('#aNameIdU').val();
    var surname = $('#aSurnameIdU').val();
    var dob = $('#aDobIdU').val();

    var data = {
        name: name,
        surname: surname,
        dob: dob,
        authorId: globalAuthorId
    };

    $.ajax({
        url: 'cs?action=updateAuthor',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getAuthorList();
                $('#editAuthorDialogId').dialog('close');
                alert('Author data has been successfully Updated!');
            } else {
                alert('Problems!!! Author data has not been successfully Updated!');
            }
        }
    });
}

function updateCategories() {
    var name = $('#cNameId').val();
    var data = {
        name: name,
        categoriesId: globalCategoriesId
    };

    $.ajax({
        url: 'cs?action=updateCategories',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getCategoriesList();
                $('#editCategoriesDialogId').dialog('close');
                alert('Categories data has been successfully Updated!');
            } else {
                alert('Problems!!! Categories data has not been successfully Updated!');
            }
        }
    });
}

function updateLanguage() {
    var name = $('#lNameId').val();
    var data = {
        name: name,
        languageId: globalLanguageId
    };

    $.ajax({
        url: 'cs?action=updateLanguage',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getLanguageList();
                $('#editLanguageDialogId').dialog('close');
                alert('Language data has been successfully Updated!');
            } else {
                alert('Problems!!! Language data has not been successfully Updated!');
            }
        }
    });
}

function updatePiblicationCityCountry() {
    var name = $('#pcNameId').val();
    var data = {
        name: name,
        publicationCityCountryId: globalPublicationCityCountryId
    };

    $.ajax({
        url: 'cs?action=updatePublicationCityCountry',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getPublicationCityCountryList();
                $('#editPublicationCityCountryDialogId').dialog('close');
                alert('Publication City Country data has been successfully Updated!');
            } else {
                alert('Problems!!! Publication City Country data has not been successfully Updated!');
            }
        }
    });
}

function updatePublisher() {
    var name = $('#pNameId').val();
    var data = {
        name: name,
        publisherId: globalPublisherId
    };

    $.ajax({
        url: 'cs?action=updatePublisher',
        type: 'POST',
        data: data,
        dataType: 'text',
        success: function (data) {
            if (data == 'success') {
                getPublisherList();
                $('#editPublisherDialogId').dialog('close');
                alert('Publisher data has been successfully Updated!');
            } else {
                alert('Problems!!! Publisher data has not been successfully Updated!');
            }
        }
    });
}

function changeActiveBookshelf(bookshelfId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deleteBookshelf',
            type: 'POST',
            data: 'bookshelfId=' + bookshelfId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getBookshelfList();
                    alert('Bookshelf data has been successfully Deleted!');
                } else {
                    alert('Problem!!! Bookshelf data has not been successfully Deleted!');
                }
            }
        });
    }
}

function changeActiveAuthor(authorId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deleteAuthor',
            type: 'POST',
            data: 'authorId=' + authorId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getAuthorList();
                    alert('Author data has been successfullt Deleted!');
                } else {
                    alert('Problems!!! Author data has not been successfully Deleted!');
                }
            }
        });
    }
}

function changeActiveLanguage(languageId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deleteLanguage',
            type: 'POST',
            data: 'languageId=' + languageId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getLanguageList();
                    alert('Language data has been successfully Deleted!');
                } else {
                    alert('Problems!!! Language data has not been successfully Deleted!');
                }
            }
        });
    }
}

function changeActiveCategories(categoriesId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deleteCategories',
            type: 'POST',
            data: 'categoriesId=' + categoriesId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getCategoriesList();
                    alert('Categories data has been successfully Deleted!');
                } else {
                    alert('Problems!!! Categories data has not been successfully Deleted!');
                }
            }
        });
    }
}

function changeActivePublicationCityCountryTable(publicationCityCountryTableId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deletePublicationCityCountryTable',
            type: 'POST',
            data: 'publicationCityCountryTableId=' + publicationCityCountryTableId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getPublicationCityCountryList();
                    alert('Publication City Country data has been successfully Deleted!');
                } else {
                    alert('Problems!!! Publication City Country data has not been successfully Deleted!');
                }
            }
        });
    }
}

function changeActivePublisher(publisherId) {
    var isDelete = confirm("Are you sure?");
    if (isDelete) {
        $.ajax({
            url: 'cs?action=deletePublisher',
            type: 'POST',
            data: 'publisherId=' + publisherId,
            dataType: 'text',
            success: function (data) {
                if (data == 'success') {
                    getPublisherList();
                    alert('Publisher data has been successfully Delete!');
                } else {
                    alert('Problems!!! Publisher data has not been successfully Delete!');
                }
            }
        });
    }
}

function simpleSearch(keyWord) {
    switch (globalBtnId) {
        case 'bookshelftBtnId':
            searchBookshelfData(keyWord);
            break;
        case 'authorBtnId':
            searchAuthorData(keyWord);
            break;
        case 'categoreisBtnId':
            searchCategoriesData(keyWord);
            break;
        case 'languageBtnId':
            searchLanguageData(keyWord);
            break;
        case 'publicationCityCountryBtnId':
            searchPublicationCityCountry(keyWord);
            break;
        case 'publisherBtnId':
            searchPublisher(keyWord);
            break;
        default:
            alert('Please select menu!!!');
            break;
    }
}

function searchAuthorData(keyWord) {
    $.ajax({
        url: 'cs?action=searchAuthorData',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function searchBookshelfData(keyWord) {
    $.ajax({
        url: 'cs?action=searchBookshelfData',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function searchCategoriesData(keyWord) {
    $.ajax({
        url: 'cs?action=searchCategoriesData',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function searchLanguageData(keyWord) {
    $.ajax({
        url: 'cs?action=searchLanguageData',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function searchPublicationCityCountry(keyWord) {
    $.ajax({
        url: 'cs?action=searchPublicationCityCountry',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function searchPublisher(keyWord) {
    $.ajax({
        url: 'cs?action=searchPublisher',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.ui-layout-center').html(data);
        }
    });
}

function advanceSearchBookshelf(){
    var authorId = $('#advAuthorComboId').val();
    var categoriesId = $('#advCategoriesComboId').val();
    var languageId = $('#advLanguageComboId').val();
    var publisherId = $('#advPublisherComboId').val();
    var publication_city_country_id = $('#advPublicationCityCountryComboId').val();
    var beginDate = $('#advBeginDateId').val();
    var endDate = $('#advEndDateId').val();
    
    var data = {
        authorId:authorId,
        categoriesId:categoriesId,
        languageId:languageId,
        publisherId:publisherId,
        publication_city_country_id:publication_city_country_id,
        beginDate:beginDate,
        endDate:endDate
    };
    
    $.ajax({
        url:'cs?action=advancedSearchBookshelf',
        type: 'GET',
        data: data,
        dataType: 'html',
        success: function (data) {
            $('#bookshelfDivId').html(data);
        }
    });
}

$(function () {
    $('body').layout({applyDemoStyles: true});

    $("#bookshelftBtnId").click(function () {
        getBookshelfList();
    });

    $("#authorBtnId").click(function () {
        getAuthorList();
    });

    $("#categoreisBtnId").click(function () {
        getCategoriesList();
    });

    $("#languageBtnId").click(function () {
        getLanguageList();
    });

    $("#publicationCityCountryBtnId").click(function () {
        getPublicationCityCountryList();
    });

    $("#publisherBtnId").click(function () {
        getPublisherList();
    });

    $("#newBtnId").click(function () {
        switch (globalBtnId) {
            case 'bookshelftBtnId':
                $("#newBookshelfDialogId").load('cs?action=newBookshelf', function () {
                    $(this).dialog('open');
                });
                break;
            case 'authorBtnId':
                $('#newAuthorDialogId').load('views/newAuthor.jsp', function () {
                    $(this).dialog('open');
                });
                break;
            case 'categoreisBtnId':
                $('#newCategoriesDialogId').load('views/newCategories.jsp', function () {
                    $(this).dialog('open');
                });
                break;
            case 'languageBtnId':
                $('#newLanguageDialogId').load('views/newLanguage.jsp', function () {
                    $(this).dialog('open');
                });
                break;
            case 'publicationCityCountryBtnId':
                $('#newPublicationCityCountryDialogId').load('views/newPublicationCityCountry.jsp', function () {
                    $(this).dialog('open');
                });
                break;
            case 'publisherBtnId':
                $('#newPublisherDialogId').load('views/newPublisher.jsp', function () {
                    $(this).dialog('open');
                });
                break;
            default:
                alert('Please select menu!!!');
                break;
        }
    });

    $('#keyWordId').keyup(function () {
        var keyWord = $('#keyWordId').val();
        simpleSearch(keyWord);
    });

    $('.btnDesign').click(function () {
        globalBtnId = $(this).attr('id');
    });



    $("#newBookshelfDialogId").dialog({
        height: 400,
        width: 400,
        title: 'New Bookshelf',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addBookshelf();
            },
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });

    $('#newAuthorDialogId').dialog({
        height: 300,
        width: 400,
        title: 'New Author',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addAuthor();
            },
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });

    $('#newCategoriesDialogId').dialog({
        height: 200,
        width: 400,
        title: 'New Categories',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addCategories();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#newLanguageDialogId').dialog({
        height: 200,
        width: 400,
        title: 'New Language',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addLanguage();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#newPublisherDialogId').dialog({
        height: 200,
        weight: 400,
        title: 'New Publisher',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addPublisher();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#newPublicationCityCountryDialogId').dialog({
        height: 200,
        width: 400,
        title: 'New Publication City Country',
        autoOpen: false,
        buttons: {
            "Save": function () {
                addPublicationCityCountry();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $("#editBookshelfDialogId").dialog({
        height: 400,
        width: 400,
        title: 'Update Bookshelf',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updateBookshelf();
            },
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });

    $('#editAuthorDialogId').dialog({
        height: 300,
        width: 400,
        title: 'Update Author',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updateAuthor();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#editCategoriesDialogId').dialog({
        heigth: 200,
        width: 400,
        title: 'Update Categories',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updateCategories();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#editLanguageDialogId').dialog({
        height: 200,
        width: 400,
        title: 'Update Language',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updateLanguage();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#editPublicationCityCountryDialogId').dialog({
        height: 200,
        width: 400,
        title: 'Update Publication City Country',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updatePiblicationCityCountry();
            },
            "Close": function () {
                $(this).dialog('close');
            }
        }
    });

    $('#editPublisherDialogId').dialog({
        height: 200,
        width: 400,
        title: 'Update Publisher',
        autoOpen: false,
        buttons: {
            "Update": function () {
                updatePublisher();
            },
            "Close": function () {
                $(this).dialog('this');
            }
        }
    });
});