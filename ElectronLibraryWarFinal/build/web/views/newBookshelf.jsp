<%-- 
    Document   : newBookshelf
    Created on : 07.05.2018, 17:06:01
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<link rel="stylesheet" type="text/css" href="css/main.css">

<script type="text/javascript">
    $(function () {
        $('#bPublicationDateId').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>
<form action="cs?action=addBookshelf" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <th><label for="bPhotoId">Photo Path</label></th>
            <td><input type="file" id="bPhotoId" class="txtFieldDesign" name="photoB"></td>
        </tr>
        <tr>
            <th><label for="bNameId">Name</label></th>
            <td><input type="text" id="bNameId" class="txtFieldDesign" name="nameB"></td>
        </tr>
        <tr>
            <th><label for="bHardcoverId">Hardcover</label></th>
            <td><input type="text" id="bHardcoverId" class="txtFieldDesign" name="hardcoverB"></td>
        </tr>
        <tr>
            <th><label for="bPublisherId">Publisher</label></th>
            <td>
                <select id="bPublisherId" class="txtFieldDesign" name="publisherB">
                    <option value="0" selected="true" disabled="disabled">Select Publisher</option>
                    <c:forEach items="${publisherList}" var="publisher">
                        <option value="${publisher.id}">${publisher.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="bLanguageId">Language</label></th>
            <td>
                <select id="bLanguageId" class="txtFieldDesign" name="languageB">
                    <option value="0" selected="true" disabled="disabled">Select Language</option>
                    <c:forEach items="${languageList}" var="language">
                        <option value="${language.id}">${language.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="bCategoriesId">Categories</label></th>
            <td>
                <select id="bCategoriesId" class="txtFieldDesign" name="categoriesB">
                    <option value="0" selected="true" disabled="disabled">Select Categories</option>
                    <c:forEach items="${categoriesList}" var="categories">
                        <option value="${categories.id}">${categories.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="bDimensionsId">Dimensions</label></th>
            <td><input type="text" id="bDimensionsId" class="txtFieldDesign" name="dimensionsB"></td>
        </tr>
        <tr>
            <th><label for="bPublicationDateId">Publication Date</label></th>
            <td><input type="text" id="bPublicationDateId" class="txtFieldDesign" name="publication_dateB"></td>
        </tr>
        <tr>
            <th><label for="bPublicationCityCountyId">Publication City(County)</label></th>
            <td>
                <select id="bPublicationCityCountyId" class="txtFieldDesign" name="publication_city_countryB">
                    <option value="0" selected="true" disabled="disabled">Select City(Country)</option>
                    <c:forEach items="${publicationCityCountryList}" var="publicationCityCountry">
                        <option value="${publicationCityCountry.id}">${publicationCityCountry.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="bAuthorId">Author</label></th>
            <td>
                <select id="bAuthorId" class="txtFieldDesign" name="authorB">
                    <option value="0" selected="true" disabled="disabled">Select Author</option>
                    <c:forEach items="${authorList}" var="author">
                        <option value="${author.id}"><font size="5">${author.name} ${author.surname} ${author.dob}</font></option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="bIsbn10Id">ISBN 10</label></th>
            <td><input type="text" id="bIsbn10Id" class="txtFieldDesign" name="isbn_10B"></td>
        </tr>
        <tr>
            <th><label for="bIsbn13Id">ISBN 13</label></th>
            <td><input type="text" id="bIsbn13Id" class="txtFieldDesign" name="isbn_13B"></td>
        </tr>
        <tr>
            <th><label for="bPdfId">Pdf Path</label></th>
            <td><input type="file" id="bPdfId" class="txtFieldDesign" name="pdfB"></td>
        </tr>
        <tr>
        <input type="submit" value="Send">
        </tr>
    </table>
</form>