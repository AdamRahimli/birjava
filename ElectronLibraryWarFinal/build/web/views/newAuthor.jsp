<%-- 
    Document   : newAuthor
    Created on : 16.05.2018, 10:27:58
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<link rel="stylesheet" type="text/css" href="css/main.css">

<script type="text/javascript">
    $(function (){
        $('#aDobId').datepicker({
            changeMonth:true,
            changeYear:true
        });
    });
</script>

<table>
    <tr>
        <th><label for="aNameId">Name</label></th>
        <td><input type="text" id="aNameId" class="txtFieldDesign"></td>
    </tr>
    <tr>
        <th><label for="aSurnameId">Surname</label></th>
        <td><input type="text" id="aSurnameId" class="txtFieldDesign"></td>
    </tr>
    <tr>
        <th><label for="aDobId">DOB</label></th>
        <td><input type="text" id="aDobId" class="txtFieldDesign"></td>
    </tr>
</table>
