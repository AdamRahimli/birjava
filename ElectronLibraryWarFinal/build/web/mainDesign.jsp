<%-- 
    Document   : mainDesign
    Created on : 28.05.2018, 17:14:32
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main Design</title>

        <script type="text/javascript" src="jquery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="jquery/jquery-latest.js"></script>  
        <script type="text/javascript" src="jquery/jquery-ui.js"></script>
        <script type="text/javascript" src="jquery/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="mainDesign/js/all.js"></script>
        
        <script type="text/javascript" src="mainDesign/js/mainDesign.js"></script>

        <link rel="stylesheet" type="text/css" href="mainDesign/css/all.css"/>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="mainDesign/css/mainDesign.css">

    </head>
    <body>
        <nav class="menu">
            <a href="#p1" class="down">MAIN</a>
            <a href="#p2" class="down">SEARCH BOOK</a>
            <a href="#p3" class="down">BOOKSHELF</a>
            <a href="#p4" class="down">CONTACT US</a>
            <a href="#p5" class="down">NEWS</a>
            <a href="#p6" class="down">ABOUT US</a>
        </nav>

        <div id="p1" class="page">
            <div class="filter"></div>
            <div class="text">
                <h1>Electron Library</h1>
                <p>Welcome Bookshelf</p>
            </div>
        </div>
        <div id="p2" class="banner">
            <div class="advancedSearch"></div>
        </div>
        <div id="p3" class="page">
            <div class="filter"></div>
            <div class="bookshelfWall"></div>
            <div class="bookshelf"></div>
            <input type="text" placeholder="Search  Book" class="searchField" id="searchFieldId">
        </div>
        <div id="p4" class="banner"></div>
        <div id="p5" class="page">
            <div class="filter"></div>
        </div>
        <div id="p6" class="banner">
            <div id="iconsId">
                <ul>
                    <li><a href="https://www.facebook.com/fire.buqa" target="_blank"><i class="fab fa-facebook-square" style="color: #3b5998"></i></a></li>
                    <li><a href="https://www.youtube.com/user/AdemRehimov" target="_blank"><i class="fab fa-youtube" style="color: #bb0000"></i></a></li>
                    <li><a href="" target="_blank"><i class="fas fa-phone-volume" style="color: #5e87c7"></i></a></li>
                    <li><a href="https://www.mail.ru" target="_blank"><i class="fas fa-envelope" style="color: white"></i></a></li>
                </ul>
            </div>
        </div>
    </body>
</html>
