<%-- 
    Document   : mainDesign
    Created on : 28.05.2018, 17:14:32
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <script type="text/javascript" src="js/jquery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="js/mainDesign.js"></script>
        
        <link rel="stylesheet" type="text/css" href="css/mainDesign.css">
        
    </head>
    <body>
        <nav class="menu">
            <a href="#" class="down">MAIN</a>
            <a href="#p2" class="down">SEARCH BOOK</a>
            <a href="#p3" class="down">BOOKSHELF</a>
            <a href="#p4" class="down">CONTACT US</a>
            <a href="#p5" class="down">NEWS</a>
            <a href="#p6" class="down">ABOUT US</a>
        </nav>


        <div id="p1" class="page">
            <div class="filter"></div>
            <div class="text">
                <h1>Electron Library</h1>
                <p>Welcome Bookshelf</p>
            </div>
        </div>
        <div id="p2" class="banner"></div>
        <div id="p3" class="page">
            <div class="filter"></div>
        </div>
        <div id="p4" class="banner"></div>
        <div id="p5" class="page">
            <div class="filter"></div>
        </div>
        <div id="p6" class="banner"></div>
    </body>
</html>
