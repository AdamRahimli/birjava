<%-- 
    Document   : bookshelfData
    Created on : 24.05.2018, 22:36:25
    Author     : Orxan
--%>

<%@page import="java.util.List"%>
<%@page import="java.io.File"%>
<%@page import="az.model.Bookshelf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(function () {
        $('#bookshelftListId').DataTable();
    });
</script>

<table id="bookshelftListId" class="display" style="width:100%">
    <thead>
        <tr>
            <th>Photo</th>
            <th>№</th>
            <th>Name</th>
            <th>Hardcover</th>
            <th>Publisher</th>
            <th>Language</th>
            <th>Categories</th>
            <th>Dimensions</th>
            <th>Publication Date</th>
            <th>Publication City Country</th>
            <th>Author</th>
            <th>ISBN_10</th>
            <th>ISBN_13</th>
            <th>Download</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <% List<Bookshelf> bookshelfList = (List<Bookshelf>) request.getAttribute("bookshelfList"); %>
        <c:forEach items="${bookshelfList}" var="bookshelf">
            <tr>
                <%  for (Bookshelf b : bookshelfList) {
                        String photo_path_real = b.getPhoto_path();
                        String[] photo_paht_array = photo_path_real.split("\\" + File.separator);
                        String photo_path = photo_paht_array[photo_paht_array.length - 3] + File.separator + photo_paht_array[photo_paht_array.length - 2] + File.separator + photo_paht_array[photo_paht_array.length - 1];
                %>
                <td><img src="<%=photo_path%>" alt="Photo" width="50px" height="50px"></td> <% }%>
                <td>${bookshelf.r}</td>
                <td>${bookshelf.name}</td>
                <td>${bookshelf.hardcover}</td>
                <td>${bookshelf.publisher.name}</td>
                <td>${bookshelf.language.name}</td>
                <td>${bookshelf.categories.name}</td>
                <td>${bookshelf.dimensions}</td>
                <td>${bookshelf.publication_date}</td>
                <td>${bookshelf.publicationCityCountry.name}</td>
                <td>${bookshelf.author.name} ${bookshelf.author.surname}</td>
                <td>${bookshelf.isbn_10}</td>
                <td>${bookshelf.isbn_13}</td>
                <td><a href="javascript: downloadPdf('${bookshelf.pdf_path}');">Download</a></td>
                <td><a href="javascript: editBookshelfData('${bookshelf.id}');">Edit</a></td>
                <td><a href="javascript: changeActiveBookshelf('${bookshelf.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>Photo</th>
            <th>№</th>
            <th>Name</th>
            <th>Hardcover</th>
            <th>Publisher</th>
            <th>Language</th>
            <th>Categories</th>
            <th>Dimensions</th>
            <th>Publication Date</th>
            <th>Publication City Country</th>
            <th>Author</th>
            <th>ISBN_10</th>
            <th>ISBN_13</th>
            <th>Download</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>    
