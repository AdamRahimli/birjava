<%-- 
    Document   : languageList
    Created on : 15.05.2018, 0:15:15
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function () {
        $('#languageTableId').DataTable();
    });
</script>

<table id="languageTableId" class="display" style="width: 100%">
    <thead>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${languageList}" var="language">
            <tr>
                <td>${language.r}</td>
                <td>${language.id}</td>
                <td>${language.name}</td>
                <td>${language.active}</td>
                <td>${language.data_date}</td>
                <td><a href="javascript: editLanguage('${language.id}');">Edit</a></td>
                <td><a href="javascript: changeActiveLanguage('${language.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>
