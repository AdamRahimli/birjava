<%-- 
    Document   : publisherList
    Created on : 15.05.2018, 1:13:26
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function () {
        $('#publisherTableId').DataTable();
    });
</script>

<table id="publisherTableId" class="display" style="width: 100%">
    <thead>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${publisherList}" var="publisher">
            <tr>
                <td>${publisher.r}</td>
                <td>${publisher.id}</td>
                <td>${publisher.name}</td>
                <td>${publisher.active}</td>
                <td>${publisher.data_date}</td>
                <td><a href="javascript: editPublisher('${publisher.id}');">Edit</a></td>
                <td><a href="javascript: changeActivePublisher('${publisher.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>
