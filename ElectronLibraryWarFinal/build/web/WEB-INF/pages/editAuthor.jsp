<%-- 
    Document   : editAuthor
    Created on : 16.05.2018, 20:25:13
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<link rel="stylesheet" type="text/css" href="css/main.css">

<script type="text/javascript">
    $(function (){
        $('#aDobId').datepicker({
            changeMonth:true,
            changeYear:true
        });
    });
</script>

<table>
    <tr>
        <th><label for="aNameIdU">Name</label></th>
        <td><input type="text" id="aNameIdU" class="txtFieldDesign" value="${author.name}"></td>
    </tr>
    <tr>
        <th><label for="aSurnameIdU">Surname</label></th>
        <td><input type="text" id="aSurnameIdU" class="txtFieldDesign" value="${author.surname}"></td>
    </tr>
    <tr>
        <th><label for="aDobIdU">DOB</label></th>
        <td><input type="text" id="aDobIdU" class="txtFieldDesign" value="${author.dob}"></td>
    </tr>
</table>