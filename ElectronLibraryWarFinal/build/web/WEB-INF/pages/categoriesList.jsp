<%-- 
    Document   : categoriesList
    Created on : 15.05.2018, 0:05:02
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function () {
        $('#categoriesTableId').DataTable();
    });
</script>

<table id="categoriesTableId" class="display" style="width: 100%">
    <thead>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${categoriesList}" var="categories">
            <tr>
                <td>${categories.r}</td>
                <td>${categories.id}</td>
                <td>${categories.name}</td>
                <td>${categories.active}</td>
                <td>${categories.data_date}</td>
                <td><a href="javascript: editCategories('${categories.id}');">Edit</a></td>
                <td><a href="javascript: changeActiveCategories('${categories.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>

