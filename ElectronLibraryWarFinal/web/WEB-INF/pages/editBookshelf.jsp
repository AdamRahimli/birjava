<%-- 
    Document   : editBookshelf
    Created on : 11.05.2018, 0:21:09
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function () {
        $('#bPublicationDateIdU').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<table>
    <tr>
        <th><label for="bPhotoIdU">Photo Path</label></th>
        <td><input type="file" id="bPhotoIdU" class="txtFieldDesign" value="${bookshelf.photo_path}"></td>
    </tr>
    <tr>
        <th><label for="bNameIdU">Name</label></th>
        <td><input type="text" id="bNameIdU" value="${bookshelf.name}"></td>
    </tr>
    <tr>
        <th><label for="bHardcoverIdU">Hardcover</label></th>
        <td><input type="text" id="bHardcoverIdU" value="${bookshelf.hardcover}"></td>
    </tr>
    <tr>
        <th><label for="bPublisherIdU">Publisher</label></th>
        <td>
            <select id="bPublisherIdU" class="txtFieldDesign">
                <option value="0" selected="true" disabled="disabled">Select Publisher</option>
                <c:forEach items="${publisherList}" var="publisher">
                    <c:choose>
                        <c:when test="${publisher.id eq bookshelf.publisher_id}">
                            <option selected="true" value="${publisher.id}">${publisher.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${publisher.id}">${publisher.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <th><label for="bLanguageIdU">Language</label></th>
        <td>
            <select id="bLanguageIdU" class="txtFieldDesign">
                <option value="0" selected="true" disabled="disabled">Select Language</option>
                <c:forEach items="${languageList}" var="language">
                    <c:choose>
                        <c:when test="${language.id eq bookshelf.language_id}">
                            <option selected="true" value="${language.id}">${language.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${language.id}">${language.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <th><label for="bCategoriesIdU">Categories</label></th>
        <td>
            <select id="bCategoriesIdU" class="txtFieldDesign">
                <option value="0" selected="true" disabled="disabled">Select Categories</option>
                <c:forEach items="${categoriesList}" var="categories">
                    <c:choose>
                        <c:when test="${categories.id eq bookshelf.categories_id}">
                            <option value="${categories.id}" selected="true">${categories.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${categories.id}">${categories.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <th><label for="bDimensionsIdU">Dimensions</label></th>
        <td><input type="text" id="bDimensionsIdU" value="${bookshelf.dimensions}"></td>
    </tr>
    <tr>
        <th><label for="bPublicationDateIdU">Publication Date</label></th>
        <td><input type="text" id="bPublicationDateIdU" value="${bookshelf.publication_date}"></td>
    </tr>
    <tr>
        <th><label for="bPublicationCityCountyIdU">Publication City(County)</label></th>
        <td>
            <select id="bPublicationCityCountyIdU" class="txtFieldDesign">
                <option value="0" selected="true" disabled="disabled">Select City(Country)</option>
                <c:forEach items="${publicationCityCountryList}" var="publicationCityCountry">
                    <c:choose>
                        <c:when test="${publicationCityCountry.id eq bookshelf.publication_city_country_id}">
                            <option selected="true" value="${publicationCityCountry.id}">${publicationCityCountry.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${publicationCityCountry.id}">${publicationCityCountry.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <th><label for="bAuthorIdU">Author</label></th>
        <td>
            <select id="bAuthorIdU" class="txtFieldDesign">
                <option value="0" selected="true" disabled="disabled">Select Author</option>
                <c:forEach items="${authorList}" var="author">
                    <c:choose>
                        <c:when test="${author.id eq bookshelf.author_id}">
                            <option selected="true" value="${author.id}"><font size="5">${author.name} ${author.surname} ${author.dob}</font></option>
                        </c:when>
                        <c:otherwise>
                            <option value="${author.id}"><font size="5">${author.name} ${author.surname} ${author.dob}</font></option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <th><label for="bIsbn10IdU">ISBN 10</label></th>
        <td><input type="text" id="bIsbn10IdU" value="${bookshelf.isbn_10}"></td>
    </tr>
    <tr>
        <th><label for="bIsbn13IdU">ISBN 13</label></th>
        <td><input type="text" id="bIsbn13IdU" value="${bookshelf.isbn_13}"></td>
    </tr>
    <tr>
        <th><label for="bPdfIdU">Pdf Path</label></th>
        <td><input type="file" id="bPdfIdU" class="txtFieldDesign" value="${bookshelf.pdf_path}"></td>
    </tr>
</table>