<%-- 
    Document   : authorList
    Created on : 14.05.2018, 23:48:43
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script  type="text/javascript">
    $(function () {
        $('#authorListId').DataTable();
    });
</script>

<table id="authorListId" class="display" style="width: 100%">
    <thead>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Surname</th>
            <th>DOB</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${authorList}" var="author">
            <tr>
                <td>${author.r}</td>
                <td>${author.id}</td>
                <td>${author.name}</td>
                <td>${author.surname}</td>
                <td>${author.dob}</td>
                <td>${author.active}</td>
                <td>${author.data_date}</td>
                <td><a href="javascript: editAuthorData('${author.id}');">Edit</a></td>
                <td><a href="javascript: changeActiveAuthor('${author.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Surname</th>
            <th>DOB</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>