<%-- 
    Document   : bookshelfList
    Created on : 27.04.2018, 11:08:02
    Author     : Orxan
--%>

<%@page import="java.lang.String"%>
<%@page import="java.io.File"%>
<%@page import="az.model.Bookshelf"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(function () {
        $("#accordion").accordion({
            collapsible: true
        });
        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true
        });
        $('#bookshelftListId').DataTable();
    });
</script>

<link rel="stylesheet" type="text/css" href="../../css/main.css">

<div id="accordion">
    <h3>Advanced Search</h3>
    <div>
        <select id="advAuthorComboId" class="advTextFieldDesign">
            <option value="0">Select Author</option>
            <c:forEach items="${authorList}" var="author">
                <option value="${author.id}"><font size="5">${author.name} ${author.surname} ${author.dob}</font></option>
            </c:forEach>
        </select> <br/>
        <select id="advCategoriesComboId" class="advTextFieldDesign">
            <option value="0">Select Categories</option>
            <c:forEach items="${categoriesList}" var="categories">
                <option value="${categories.id}">${categories.name}</option>
            </c:forEach>
        </select> <br/>
        <select id="advLanguageComboId" class="advTextFieldDesign">
            <option value="0">Select Language</option>
            <c:forEach items="${languageList}" var="language">
                <option value="${language.id}">${language.name}</option>
            </c:forEach>
        </select> <br/>
        <select id="advPublisherComboId" class="advTextFieldDesign">
            <option value="0">Select Publisher</option>
            <c:forEach items="${publisherList}" var="publisher">
                <option value="${publisher.id}">${publisher.name}</option>
            </c:forEach>
        </select> <br/>
        <select id="advPublicationCityCountryComboId" class="advTextFieldDesign">
            <option value="0">Select City(Country)</option>
            <c:forEach items="${publicationCityCountryList}" var="publicationCityCountry">
                <option value="${publicationCityCountry.id}">${publicationCityCountry.name}</option>
            </c:forEach>
        </select> <br/>
        <input type="text" id="advBeginDateId" placeholder="Begin Date" class="datepicker" class="advTextFieldDesign" style="margin-left: 5px"> &nbsp;
        <input type="text" id="advEndDateId" placeholder="End Date" class="datepicker" class="advTextFieldDesign" style="margin-left: 5px"> <br/>
        <input type="button" id="advSearchBtnId" value="Search" style="margin: 5px" onclick="advanceSearchBookshelf();">
    </div>
</div>

<div id="bookshelfDivId">
    <table id="bookshelftListId" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Photo</th>
                <th>№</th>
                <th>Name</th>
                <th>Hardcover</th>
                <th>Publisher</th>
                <th>Language</th>
                <th>Categories</th>
                <th>Dimensions</th>
                <th>Publication Date</th>
                <th>Publication City Country</th>
                <th>Author</th>
                <th>ISBN_10</th>
                <th>ISBN_13</th>
                <th>Download</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <% List<Bookshelf> bookshelfList = (List<Bookshelf>) request.getAttribute("bookshelfList"); %>
            <c:forEach items="${bookshelfList}" var="bookshelf">
                <tr>
                    <%  for (Bookshelf b : bookshelfList) {
                            String photo_path_real = b.getPhoto_path();
                            String[] photo_paht_array = photo_path_real.split("\\" + File.separator);
                            String photo_path = photo_paht_array[photo_paht_array.length - 3] + File.separator + photo_paht_array[photo_paht_array.length - 2] + File.separator + photo_paht_array[photo_paht_array.length - 1];
                    %>
                    <td><img src="<%=photo_path%>" alt="Photo" width="50px" height="50px"></td> <% }%>
                    <td>${bookshelf.r}</td>
                    <td>${bookshelf.name}</td>
                    <td>${bookshelf.hardcover}</td>
                    <td>${bookshelf.publisher.name}</td>
                    <td>${bookshelf.language.name}</td>
                    <td>${bookshelf.categories.name}</td>
                    <td>${bookshelf.dimensions}</td>
                    <td>${bookshelf.publication_date}</td>
                    <td>${bookshelf.publicationCityCountry.name}</td>
                    <td>${bookshelf.author.name} ${bookshelf.author.surname}</td>
                    <td>${bookshelf.isbn_10}</td>
                    <td>${bookshelf.isbn_13}</td>
                    <td><a href="javascript: downloadPdf('${bookshelf.pdf_path}');">Download</a></td>
                    <td><a href="javascript: editBookshelfData('${bookshelf.id}');">Edit</a></td>
                    <td><a href="javascript: changeActiveBookshelf('${bookshelf.id}');">Delete</a></td>
                </tr>

            </c:forEach>
        </tbody>
        <tfoot>
            <tr>
                <th>Photo</th>
                <th>№</th>
                <th>Name</th>
                <th>Hardcover</th>
                <th>Publisher</th>
                <th>Language</th>
                <th>Categories</th>
                <th>Dimensions</th>
                <th>Publication Date</th>
                <th>Publication City Country</th>
                <th>Author</th>
                <th>ISBN_10</th>
                <th>ISBN_13</th>
                <th>Download</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
    </table>    
</div>
