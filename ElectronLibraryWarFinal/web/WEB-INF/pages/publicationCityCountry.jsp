<%-- 
    Document   : publicationCityCountry
    Created on : 15.05.2018, 0:25:52
    Author     : Orxan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function () {
        $('#publicationCityCountryTableId').DataTable();
    });
</script>

<table id="publicationCityCountryTableId" class="display" style="width: 100%">
    <thead>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${publicationCityCountryList}" var="publicationCityCountry">
            <tr>
                <td>${publicationCityCountry.r}</td>
                <td>${publicationCityCountry.id}</td>
                <td>${publicationCityCountry.name}</td>
                <td>${publicationCityCountry.active}</td>
                <td>${publicationCityCountry.data_date}</td>
                <td><a href="javascript: editPublicationCityCountry('${publicationCityCountry.id}');">Edit</a></td>
                <td><a href="javascript: changeActivePublicationCityCountryTable('${publicationCityCountry.id}');">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>№</th>
            <th>Id</th>
            <th>Name</th>
            <th>Active</th>
            <th>Data Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </tfoot>
</table>
