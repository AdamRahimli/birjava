<%-- 
    Document   : mainAdvancedSearch
    Created on : 28.05.2018, 19:15:34
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(function () {
        $("#accordion").accordion({
            collapsible: true
        });
        $('.mainAdvTextFieldDesign').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<link rel="stylesheet" type="text/css" href="../../css/main.css"/>

<div id="accordion">
    <h3 style="border-radius: 10px 10px 0 0">Advanced Search</h3>
    <div style="border-radius: 0 0 10px 10px">
        <select id="mainAdvAuthorComboId" class="mainAdvCmbDesign">
            <option value="0">Select Author</option>
            <c:forEach items="${authorList}" var="author">
                <option value="${author.id}"><font size="5">${author.name} ${author.surname} ${author.dob}</font></option>
            </c:forEach>
        </select> &nbsp;
        <select id="mainAdvCategoriesComboId" class="mainAdvCmbDesign">
            <option value="0">Select Categories</option>
            <c:forEach items="${categoriesList}" var="categories">
                <option value="${categories.id}">${categories.name}</option>
            </c:forEach>
        </select> &nbsp;
        <select id="mainAdvLanguageComboId" class="mainAdvCmbDesign">
            <option value="0">Select Language</option>
            <c:forEach items="${languageList}" var="language">
                <option value="${language.id}">${language.name}</option>
            </c:forEach>
        </select> &nbsp;
        <select id="mainAdvPublisherComboId" class="mainAdvCmbDesign">
            <option value="0">Select Publisher</option>
            <c:forEach items="${publisherList}" var="publisher">
                <option value="${publisher.id}">${publisher.name}</option>
            </c:forEach>
        </select> &nbsp;
        <select id="mainAdvPublicationCityCountryComboId" class="mainAdvCmbDesign">
            <option value="0">Select City(Country)</option>
            <c:forEach items="${publicationCityCountryList}" var="publicationCityCountry">
                <option value="${publicationCityCountry.id}">${publicationCityCountry.name}</option>
            </c:forEach>
        </select> <br/> <br/>
        <input type="text" id="mainAdvBeginDateId" placeholder="Begin Date" class="mainAdvTextFieldDesign" style="margin-left: 5px"> <br>
        <input type="text" id="mainAdvEndDateId" placeholder="End Date" class="mainAdvTextFieldDesign" style="margin-left: 5px"> <br/>
        <input type="button" id="mainAdvSearchBtnId" value="Search" style="margin: 5px 5px 5px 1150px; width: 80px; height: 30px" onclick="mainAdvanceSearchBookshelf();">
    </div>
</div>