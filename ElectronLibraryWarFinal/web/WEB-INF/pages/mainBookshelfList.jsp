<%-- 
    Document   : mainBookshelfList
    Created on : 28.05.2018, 18:26:01
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(function () {
        $('#mainBookshelftListId').DataTable({
            "searching": false,
            "lengthMenu": [[4]]
        });
    });
</script>

<table id="mainBookshelftListId" class="display" style="width:100%">
    <thead>
        <tr>
            <th>Photo</th>
            <th>№</th>
            <th>Name</th>
            <th>Hardcover</th>
            <th>Publisher</th>
            <th>Language</th>
            <th>Categories</th>
            <th>Dimensions</th>
            <th>Publication Date</th>
            <th>Publication City Country</th>
            <th>Author</th>
            <th>ISBN_10</th>
            <th>ISBN_13</th>
            <th>Download PDF</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${bookshelfList}" var="bookshelf">
            <tr>
                <td><img src="${bookshelf.photo_path}" alt="Photo" width="50px" height="70px"></td>
                <td>${bookshelf.r}</td>
                <td>${bookshelf.name}</td>
                <td>${bookshelf.hardcover}</td>
                <td>${bookshelf.publisher.name}</td>
                <td>${bookshelf.language.name}</td>
                <td>${bookshelf.categories.name}</td>
                <td>${bookshelf.dimensions}</td>
                <td>${bookshelf.publication_date}</td>
                <td>${bookshelf.publicationCityCountry.name}</td>
                <td>${bookshelf.author.name} ${bookshelf.author.surname}</td>
                <td>${bookshelf.isbn_10}</td>
                <td>${bookshelf.isbn_13}</td>
                <td><a href="javascript: downloadPdf('${bookshelf.pdf_path}');">Download</a></td>
            </tr>
        </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <th>Photo</th>
            <th>№</th>
            <th>Name</th>
            <th>Hardcover</th>
            <th>Publisher</th>
            <th>Language</th>
            <th>Categories</th>
            <th>Dimensions</th>
            <th>Publication Date</th>
            <th>Publication City Country</th>
            <th>Author</th>
            <th>ISBN_10</th>
            <th>ISBN_13</th>
            <th>Download PDF</th>
        </tr>
    </tfoot>
</table>    
