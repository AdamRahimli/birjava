$(document).ready(function () {
    $('.down').click(function (e) {
        var linkHref = $(this).attr('href');
        $('html,body').animate({
            scrollTop: $(linkHref).offset().top
        }, 1000);
    });
});

$(function () {
    getMainPageDataList();
    $('#searchFieldId').keyup(function () {
        var keyWord = $('#searchFieldId').val();
        mainSearchBookshelfData(keyWord);
    });
});

function mainSearchBookshelfData(keyWord) {
    $.ajax({
        url: 'mds?action=searchBookshelfData',
        type: 'GET',
        data: 'keyWord=' + keyWord,
        dataType: 'html',
        success: function (data) {
            $('.bookshelf').html(data);
        }
    });
}

function getMainPageDataList() {
    $.ajax({
        url: 'mds?action=getBookshelfList',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $(".bookshelf").html(data);
        }
    });
    $.ajax({
        url: 'mds?action=getAdvancedSearch',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $(".advancedSearch").html(data);
        }
    });
}
function mainAdvanceSearchBookshelf() {
    var authorId = $('#mainAdvAuthorComboId').val();
    var categoriesId = $('#mainAdvCategoriesComboId').val();
    var languageId = $('#mainAdvLanguageComboId').val();
    var publisherId = $('#mainAdvPublisherComboId').val();
    var publication_city_country_id = $('#mainAdvPublicationCityCountryComboId').val();
    var beginDate = $('#mainAdvBeginDateId').val();
    var endDate = $('#mainAdvEndDateId').val();

    var data = {
        authorId: authorId,
        categoriesId: categoriesId,
        languageId: languageId,
        publisherId: publisherId,
        publication_city_country_id: publication_city_country_id,
        beginDate: beginDate,
        endDate: endDate
    };

    $.ajax({
        url: 'mds?action=advancedSearchBookshelf',
        type: 'GET',
        data: data,
        dataType: 'html',
        success: function (data) {
            $('.bookshelf').html(data);
        }
    });
}