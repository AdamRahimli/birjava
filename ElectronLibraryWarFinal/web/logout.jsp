<%-- 
    Document   : logout
    Created on : 27.05.2018, 1:55:06
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    session.removeAttribute("user");
    session.invalidate();
    response.sendRedirect("login.jsp");
%>