<%-- 
    Document   : main
    Created on : 22.04.2018, 2:41:27
    Author     : Orxan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Main Page</title>

        <script type="text/javascript" src="jquery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="jquery/jquery-latest.js"></script>  
        <script type="text/javascript" src="jquery/jquery.layout-latest.js"></script>
        <script type="text/javascript" src="jquery/jquery-ui.js"></script>
        <script type="text/javascript" src="jquery/jquery.dataTables.min.js"></script>
        
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript">
            history.pushState(null,null,'main.jsp');
            window.addEventListener('popstate',function (event){
               history.pushState(null,null,'main.jsp'); 
            });
        </script>
        
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">

    </head>
    <body>
        <div class="ui-layout-center">
            Center
        </div>
        <div class="ui-layout-north">
            ${user.name} ${user.surname} <br/>
            <a href="logout.jsp">Logout</a>
        </div>
        <div class="ui-layout-south">
            South
        </div>
        <div class="ui-layout-east">
            <input type="button" value="New" id="newBtnId" class="newBtnDesign"> <br>
            <input type="text" placeholder="Search" id="keyWordId" class="txtFieldDesign"> <br>
        </div>
        <div class="ui-layout-west">
            <input type="button" value="Bookshelf List" id="bookshelftBtnId" class="btnDesign"> <br>
            <input type="button" value="Author List" id="authorBtnId" class="btnDesign"> <br>
            <input type="button" value="Categories List" id="categoreisBtnId" class="btnDesign"> <br>
            <input type="button" value="Language List" id="languageBtnId" class="btnDesign"> <br>
            <input type="button" value="Publication City Country List" id="publicationCityCountryBtnId" class="btnDesign"> <br>
            <input type="button" value="Publisher List" id="publisherBtnId" class="btnDesign"> <br>
        </div>
        <div id="newBookshelfDialogId"></div>
        <div id="newAuthorDialogId"></div>
        <div id="newCategoriesDialogId"></div>
        <div id="newLanguageDialogId"></div>
        <div id="newPublicationCityCountryDialogId"></div>
        <div id="newPublisherDialogId"></div>
        
        <div id="editBookshelfDialogId"></div>
        <div id="editAuthorDialogId"></div>
        <div id="editCategoriesDialogId"></div>
        <div id="editLanguageDialogId"></div>
        <div id="editPublicationCityCountryDialogId"></div>
        <div id="editPublisherDialogId"></div>
    </body>
</html>
